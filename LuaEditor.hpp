#ifndef _LUAEDITOR_HPP_INCLUDED
#define _LUAEDITOR_HPP_INCLUDED

#include <wx/stc/stc.h>

#include <string>
#include <vector>
#include <ios>

struct lua_State;

namespace masosi {

class LuaEditorPrefs
{
public:

    wxString words1;
    wxString words2;
    wxString words3;

    wxFont defaultFont;

    class Style
    {
    public:
        int index;
        wxColour foreground;
        wxColour background;
        wxFont font;

        Style();

        Style(
            int index_,
            const wxColour &fg_,
            const wxColour &bg_,
            const wxFont &font_);
    };

    std::vector<Style> styles;

    int wrapMode;
    int lineNumWidth;
    wxColour lineNumForeground;
    wxColour lineNumBackground;
    wxFont lineNumFont;
    bool indentationGuides;
    wxColour indentationGuidesColor;
    int tabWidth;
    int indentWidth;
    bool tabIndents;
    bool useTabs;
    int edgeMode;
    wxColour edgeColor;
    int edgeColumn;
    bool autoIndent;

    /**
     * Default preferences
     */
    LuaEditorPrefs();

    /**
     * Load from script
     */
    LuaEditorPrefs(const std::string &script);

    /**
     * Load from script
     */
    void LoadScript(const std::string &script);

    /**
     * Default preferences
     */
    void Defaults();

    /**
     * Register this class to Lua interface.
     * 
     * Requires wxFont and wxColour registered.
     */
    static void RegisterToLua(lua_State *L);

};

class LuaEditor : public wxStyledTextCtrl
{
public:

    LuaEditor();

    LuaEditor(
        wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const LuaEditorPrefs &prefs = LuaEditorPrefs(),
        const wxString &name = "LuaEditor");

    bool Create(
        wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const LuaEditorPrefs &prefs = LuaEditorPrefs(),
        const wxString &name = "LuaEditor");

    void SetPrefs(const LuaEditorPrefs &prefs);
    LuaEditorPrefs GetPrefs() const;

    virtual ~LuaEditor();

private:

    LuaEditorPrefs last_prefs_;
};

}

#endif // _LUAEDITOR_HPP_INCLUDED
