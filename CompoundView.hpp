#ifndef _COMPOUNDVIEW_HPP_INCLUDED
#define _COMPOUNDVIEW_HPP_INCLUDED

// 2016-08-21

#include <memory>
#include <vector>
#include <wx/panel.h>
#include <wx/textctrl.h>

#include "Simulator.hpp"
#include "LuaEditor.hpp"
#include "SimulationPanel.hpp"

struct lua_State;

namespace masosi {

extern const wxString CompoundViewName;

class CompoundViewPrefs
{
public:

    CompoundViewPrefs();

    CompoundViewPrefs(const std::string &script);

    void Defaults();

    LuaEditorPrefs luaEditorPrefs;

    struct {
        wxColour background;
        wxColour foreground;
        wxFont font;
    } outputPanelPrefs;

    // Look at the documentation of wxSplitterWindow::Split{Vertical,Horizontal}
    // sashPosition parameter
    int verticalSashPos;
    int horizontalSashPos;

    double verticalSashGrav;
    double horizontalSashGrav;

    // TODO add script support for the following?
    bool showStartStopButton;
    bool showPauseResumeButton;
    bool showStatusText;
    bool showSaveButton;
    bool showRestoreButton;
    bool showOutputPanel;
    bool showLuaEditors;
    /* NOTE I won't support them for now
    bool showRightPanel;
    bool showSimulationPanel;
    */
    void LoadScript(const std::string &script);

    /**
     * Requires LuaEditorPrefs and wxColour to be registered
     */
    static void RegisterToLua(lua_State *L);

};

class CompoundView : public wxPanel
{

public:

    CompoundView();

    CompoundView(
        wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const wxPoint &pos = wxDefaultPosition,
        const wxSize &size = wxDefaultSize,
        long style = wxTAB_TRAVERSAL | wxNO_BORDER,
        const wxString &name = CompoundViewName);

    bool Create(
        wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const wxPoint &pos = wxDefaultPosition,
        const wxSize &size = wxDefaultSize,
        long style = wxTAB_TRAVERSAL | wxNO_BORDER,
        const wxString &name = CompoundViewName);

    virtual ~CompoundView();

    Simulator *GetSimulator();
    LuaEditor *GetLuaEditor(Simulator::EScriptType type);
    SimulationPanel *GetSimulationPanel();
    wxTextCtrl *GetOutputControl();

    CompoundViewPrefs GetPrefs() const;
    void SetPrefs(const CompoundViewPrefs &prefs);

    wxSize DoGetBestSize() const override;

    Simulator::EScriptType GetCurrentScript() const;

    void ReloadScripts();

    bool IsAnyScriptModified() const;
    bool IsScriptModified(Simulator::EScriptType script) const;

    // scripts must be of size (Simulator::SCRIPT_COUNT + 1)
    void GetModifiedScripts(Simulator::EScriptType *scripts) const;

    //BEGIN Event Handling Stuff

    class EventHandler
    {
    public:

        virtual bool OnStopped();
        virtual bool OnStarted();
        virtual bool OnPaused();
        virtual bool OnResumed();

        virtual bool OnCurrentScriptChanged(
            Simulator::EScriptType old_script,
            Simulator::EScriptType new_script
        );

        virtual bool OnScriptModified(Simulator::EScriptType script);
        virtual bool OnScriptSavePointLeft(Simulator::EScriptType script);
        virtual bool OnScriptSavePointReached(Simulator::EScriptType script);
        virtual bool OnScriptRestored(Simulator::EScriptType script);
        virtual bool OnScriptSaved(Simulator::EScriptType script);
        virtual bool OnStatusChanged(const std::string &status);

        virtual bool OnOutput(const std::string &output);
        virtual bool OnInputRequested(
            const std::string &ir_type,
            const std::list<std::string> &ir_strings
        );

        virtual ~EventHandler();
    };

    void AddEventHandler(EventHandler *eventHandler);
    EventHandler *DetachEventHandler(EventHandler *eventHandler);

    // the event handlers array will be NULL-terminated
    void DetachAllEventHandlers(EventHandler ***eventHandlersArray);

    void DeleteAllEventHandlers();

    // returns a NULL-terminated list of event handlers.
    EventHandler * const * GetEventHandlers() const;

    //END


    //BEGIN Actions Stuff

    class ActionsHelper
    {
    public:
        enum EActionFlags
        {
            AF_NONE                         = 0,
            AF_ASK_USER                     = 1,
            AF_INVOKE_EVENT_HANDLERS        = 2,
            AF_DEFAULT =
                AF_ASK_USER |
                AF_INVOKE_EVENT_HANDLERS
        };

        typedef int EActionResult;

        static constexpr EActionResult AR_DONE                  = 0;
        static constexpr EActionResult AR_FAIL                  = 1;
        static constexpr EActionResult AR_USER_CANCELLED        = 2;
        static constexpr EActionResult AR_ALREADY_DONE          = 3;
        static constexpr EActionResult AR_INVALID_STATE         = 4;

        /**
         * Saves the currently selected script.
         * 
         * Its default user interaction is showing a messagebox asking whether
         * the user wants to save the script and asking if the user wants
         * to restart the simulation if it's running. The default behaviour
         * is not to restart the simulation.
         * 
         * If AF_INVOKE_EVENT_HANDLERS is not ORed, then it will not ask for
         * restarting the simulation. (since it's implemented by the default
         * event handler)
         * 
         * It has no prerequisites.
         * 
         * \return AR_USER_CANCELLED if the user cancelled, AR_DONE otherwise.
         * 
         */
        virtual EActionResult SaveCurrentScript(
            EActionFlags flags = AF_INVOKE_EVENT_HANDLERS) = 0;

        /**
         * Restores the currently selected script.
         * 
         * It resets the text shown in the corresponding lua editor to its
         * latest saved state.
         * 
         * Its default user interaction is showing a messagebox asking if the
         * user wants to restore the script.
         * 
         * It has no prerequisites.
         * 
         * \return AR_USER_CANCELLED if the user cancelled, AR_DONE otherwise.
         */
        virtual EActionResult RestoreCurrentScript(
            EActionFlags flags = AF_INVOKE_EVENT_HANDLERS) = 0;

        /**
         * Clears the currently selected script.
         * 
         * It only clears the text shown in the corresponding lua editor. It
         * does not save it.
         * 
         * Its default user interaction is showing a messagebox asking if the
         * user wants to clear the script.
         * 
         * It has no prerequisites.
         * 
         * \return AR_USER_CANCELLED if the user cancelled, AR_DONE otherwise.
         */
        virtual EActionResult ClearCurrentScript(
            EActionFlags flags = AF_INVOKE_EVENT_HANDLERS) = 0;

        /**
         * Saves the given script.
         * 
         * Its default user interaction is showing a messagebox asking whether
         * the user wants to save the script and asking if the user wants
         * to restart the simulation if it's running. The default behaviour
         * is not to restart the simulation.
         * 
         * If AF_INVOKE_EVENT_HANDLERS is not ORed, then it will not ask for
         * restarting the simulation. (since it's implemented by the default
         * event handler)
         * 
         * It has no prerequisites.
         * 
         * \return AR_USER_CANCELLED if the user cancelled, AR_DONE otherwise.
         * 
         */
        virtual EActionResult SaveScript(
            Simulator::EScriptType script,
            EActionFlags flags = AF_INVOKE_EVENT_HANDLERS) = 0;

        /**
         * Restores the given script.
         * 
         * It resets the text shown in the corresponding lua editor to its
         * latest saved state.
         * 
         * Its default user interaction is showing a messagebox asking if the
         * user wants to restore the script.
         * 
         * It has no prerequisites.
         * 
         * \return AR_USER_CANCELLED if the user cancelled, AR_DONE otherwise.
         */
        virtual EActionResult RestoreScript(
            Simulator::EScriptType script,
            EActionFlags flags = AF_INVOKE_EVENT_HANDLERS) = 0;

        /**
         * Clears the given script.
         * 
         * It only clears the text shown in the corresponding lua editor. It
         * does not save it.
         * 
         * Its default user interaction is showing a messagebox asking if the
         * user wants to clear the script.
         * 
         * It has no prerequisites.
         * 
         * \return AR_USER_CANCELLED if the user cancelled, AR_DONE otherwise.
         */
        virtual EActionResult ClearScript(
            Simulator::EScriptType script,
            EActionFlags flags = AF_INVOKE_EVENT_HANDLERS) = 0;

        /**
         * Stops the ongoing simulation.
         * 
         * It requires the simulation to be running.
         * 
         * The default user interaction is asking if the user wants to stop
         * the simulation.
         * 
         * \return AR_USER_CANCELLED if the user cancelled, AR_ALREADY_DONE if
         * the simulation is not running, AR_DONE otherwise.
         */
        virtual EActionResult Stop(
            EActionFlags flags = AF_INVOKE_EVENT_HANDLERS) = 0;

        /**
         * Starts the simulation.
         * 
         * It requires the simulation to be not running.
         * 
         * Its default user interaction is none. But SaveAllScripts is called
         * with flags, so it may cause some interaction.
         * 
         * \return AR_ALREADY_DONE if the simulation is running, AR_DONE
         * otherwise.
         */
        virtual EActionResult Start(
            bool saveAllScriptsBefore = true,
            EActionFlags flags = AF_DEFAULT) = 0;

        /**
         * Pauses the ongoing simulation.
         * 
         * It requires the simulation to be non-paused.
         * 
         * The default user interaction is none.
         * 
         * \return AR_ALREADY_DONE if the simulation is already paused,
         * AR_INVALID_STATE if it's not running, AR_DONE otherwise.
         */
        virtual EActionResult Pause(
            EActionFlags flags = AF_DEFAULT) = 0;

        /**
         * Resumes the ongoing and paused simulation.
         * 
         * It requires simulation to be paused.
         * 
         * The default user interaction is none.
         * 
         * \return AR_ALREADY_DONE if the simulation is not paused yet,
         * AR_INVALID_STATE if it's not running, AR_DONE otherwise.
         */
        virtual EActionResult Resume(
            EActionFlags flags = AF_DEFAULT) = 0;

        /**
         * Saves all of the scripts.
         * 
         * The default user interaction is asking for each script to save, if
         * modified.
         * 
         * \return AR_USER_CANCELLED if user cancelled the operation, AR_DONE
         * otherwise.
         */
        virtual EActionResult SaveAllScripts(
            EActionFlags flags = AF_DEFAULT) = 0;

        /**
         * Resets the given lua state.
         * 
         * It requires the simulation to be not running. If it's running, then
         * it fails.
         * 
         * The default user interaction is asking if user wants to reset the
         * given lua state.
         * 
         * \return AR_FAIL if the simulation is running, AR_DONE otherwise.
         */
        virtual EActionResult ResetLua(
            Simulator::ELuaState,
            EActionFlags flags = AF_DEFAULT) = 0;

        virtual ~ActionsHelper();

    };

    /**
     * Actions are not thread safe, they are supposed to be run from the main
     * thread.
     */
    ActionsHelper *Actions();

#define ACTIONSHELPER_INHERIT(x) static constexpr auto x = ActionsHelper:: x ;
    ACTIONSHELPER_INHERIT(AF_NONE)
    ACTIONSHELPER_INHERIT(AF_ASK_USER)
    ACTIONSHELPER_INHERIT(AF_INVOKE_EVENT_HANDLERS)
    ACTIONSHELPER_INHERIT(AF_DEFAULT)

    ACTIONSHELPER_INHERIT(AR_DONE)
    ACTIONSHELPER_INHERIT(AR_FAIL)
    ACTIONSHELPER_INHERIT(AR_USER_CANCELLED)
    ACTIONSHELPER_INHERIT(AR_ALREADY_DONE)
    ACTIONSHELPER_INHERIT(AR_INVALID_STATE)
#undef ACTIONSHELPER_INHERIT

    //END

private:

    struct Impl;
    std::unique_ptr<Impl> impl_;

};

}

#endif // _COMPOUNDVIEW_HPP_INCLUDED
