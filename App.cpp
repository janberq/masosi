#include "App.hpp"

#include "MainFrame.hpp"

#ifdef PRSS_DEBUG

#include "Misc/lua_prss.h"
#include <thread>

#endif

#include <locale>
#include <boost/locale.hpp>
#include <boost/filesystem.hpp>

#include <wx/image.h> // for wxInitAllImageHandlers()

namespace masosi {

bool App::OnInit()
{

#ifdef PRSS_DEBUG
    PRSS_StartTimer();
    PRSS_Status("Main Thread ID: ", std::this_thread::get_id());
#endif

    // let's have utf-8 filesystem paths
    std::locale::global(boost::locale::generator().generate(""));
    boost::filesystem::path::imbue(std::locale());

    wxInitAllImageHandlers();

    MainFrame *frm = new MainFrame(nullptr, wxID_ANY);
    frm->Show();
    return true;
}

}

wxIMPLEMENT_APP(masosi::App);
