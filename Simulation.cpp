#include "Simulation.hpp"

#include <sstream>
#include <locale>
#include <tuple>
#include <utility>
#include <algorithm>
#include <iterator>

#include <cmath>
#include <cassert>

#include <sol.hpp>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/segment.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <boost/geometry/geometries/register/ring.hpp>

//BEGIN Boost.Geometry Registrations

namespace boost { namespace geometry { namespace traits {

    BOOST_GEOMETRY_DETAIL_SPECIALIZE_POINT_TRAITS(
        masosi::Vector *,
        2,
        double,
        boost::geometry::cs::cartesian)

    template <> struct access<masosi::Vector *, 0> {
        static inline double get(masosi::Vector * const & p)
        { return p->x; }
        static inline void set(masosi::Vector * & p, double const &v)
        { p->x = v; };
    };

    template <> struct access<masosi::Vector *, 1> {
        static inline double get(masosi::Vector * const & p)
        { return p->y; }
        static inline void set(masosi::Vector * & p, double const &v)
        { p->y = v; };
    };
}}}

// register Vector to boost.geometry

BOOST_GEOMETRY_REGISTER_POINT_2D(
    masosi::Vector, double, boost::geometry::cs::cartesian, x, y)

// register ring of this type to boost.geometry

BOOST_GEOMETRY_REGISTER_RING(std::vector<masosi::Vector *>)
                                // IMPORTANT NOTE ring needs random access

BOOST_GEOMETRY_REGISTER_RING(std::vector<masosi::Vector>)

//END

namespace masosi
{

/**
 * Vector
 */

double Vector::norm() const {
    return std::sqrt(x*x + y*y);
}

Vector Vector::unit() const {
    return *this / norm();
}

Vector &Vector::normalize() {
    auto nrm = norm();
    x /= nrm;
    y /= nrm;
    return *this;
}

double Vector::cross(const Vector &p) const {
    return x * p.y - y * p.x;
}

Vector &Vector::rotate(double a) {
    double oldx = x, oldy = y;
    x = oldx * std::cos(a) - oldy * std::sin(a);
    y = oldx * std::sin(a) + oldy * std::cos(a);
    return *this;
}

Vector Vector::rotated(double a) const {
    return Vector{*this}.rotate(a);
}

Vector Vector::normal() const {
    return Vector{ -y, x };
}

Vector Vector::unit_normal() const {
    return normal().unit();
}

std::string Vector::repr() const {
    std::stringstream ss;
    ss.imbue(std::locale("C"));
    ss << "(" << x << ", " << y << ")";
    return ss.str();
}

void RegisterVector(lua_State *L, const std::string &name) {
    sol::state_view lua{L};
    lua.new_usertype<Vector>(
        name,
        sol::constructors<
            sol::types<>,
            sol::types<const Vector&>,
            sol::types<double, double>>(),
        "new_unsafe", sol::overload(
            []() -> Vector* { return new Vector(); },
            [](const Vector &other) -> Vector * { return new Vector(other); },
            [](double x, double y) -> Vector * { return new Vector(x, y); }),
        "x", &Vector::x,
        "y", &Vector::y,
        sol::meta_function::addition,
            [](const Vector &a, const Vector &b) -> Vector
            { return a + b; }, // since they have unary overloads
        sol::meta_function::subtraction,
            [](const Vector &a, const Vector &b) -> Vector { return a - b; },
        sol::meta_function::multiplication, sol::overload(
            [](double a, const Vector &b) {
                return a * b;
            },
            [](const Vector &a, double b) {
                return a * b;
            },
            [](const Vector &a, const Vector &b) {
                return a * b;
            }),
        sol::meta_function::division, &Vector::operator/,
        sol::meta_function::unary_minus,
            [](const Vector &a) -> Vector { return -a; },
        "equals", &Vector::operator==,
        "norm", &Vector::norm,
        "unit", &Vector::unit,
        "normalize", &Vector::normalize,
        "cross", &Vector::cross,
        "rotate", &Vector::rotate,
        "rotated", &Vector::rotated,
        "normal", &Vector::normal,
        "unit_normal", &Vector::unit_normal,
        "copy_from", &Vector::copy_from,
        "set", &Vector::set,
        "repr", &Vector::repr,
        "delete", [](Vector *p) { delete p; }
    );
}

/**
 * Color
 */

void RegisterColor(lua_State *L, const std::string& name) {
    sol::state_view lua{L};
    lua.new_usertype<Color>(
        name,
        sol::constructors<
            sol::types<>,
            sol::types<int, int, int>,
            sol::types<int, int, int, int>,
            sol::types<const Color &>>(),
        "new_unsafe", sol::overload(
            []() -> Color* { return new Color(); },
            [](const Color &other) -> Color * { return new Color(other); },
            [](int r, int g, int b) -> Color * { return new Color(r, g, b); },
            [](int r, int g, int b, int a) -> Color *
                { return new Color(r, g, b, a); }),
        "r", &Color::r,
        "g", &Color::g,
        "b", &Color::b,
        "a", &Color::a,
        "copy_from", &Color::copy_from,
        "set", &Color::set,
        "delete", [](Color *obj) { delete obj; }
    );
}

/**
 * Polygon
 */

Polygon::Polygon()
{
}

Polygon::Polygon(const Polygon &other)
{
    copy_from(other);
}

Polygon::Polygon(Polygon &&other)
{
    move_from(other);
}

Polygon &Polygon::operator=(const Polygon &other)
{
    copy_from(other);
    return *this;
}

Polygon &Polygon::operator=(Polygon &&other)
{
    move_from(other);
    return *this;
}

void Polygon::copy_from(const Polygon& other)
{
    if (this == &other) return ;

    ClearVectors();

    position = other.position;
    rotation = other.rotation;
    color = other.color;
    border_color = other.border_color;
    border = other.border;
    enabled = other.enabled;
    shown = other.shown;

    is_in_block_ = false;
    is_in_iteration_ = false;

    for (auto &o: other.pts_)
    {
        pts_.push_back(new Vector(*o));
    }

    RefreshVector();
}

void Polygon::move_from(Polygon &other)
{
    if (this == &other) return ;

    std::swap(position, other.position);
    std::swap(rotation, other.rotation);
    std::swap(color, other.color);
    std::swap(border_color, other.border_color);
    std::swap(border, other.border);
    std::swap(enabled, other.enabled);
    std::swap(shown, other.shown);

    is_in_block_ = false;
    is_in_iteration_ = false;

    std::swap(pts_, other.pts_);

    RefreshVector();
    other.RefreshVector();
}

void Polygon::BeginVectors() {
    is_in_block_ = true;
}

void Polygon::EndVectors() {
    RefreshVector();
    is_in_block_ = false;
}

Vector * Polygon::AddVector(const Vector& p) {
    Vector *res;
    pts_.push_back(res = new Vector{p});
    if (!is_in_block_) RefreshVector();
    return res;
}

Vector *Polygon::AddVector(double x, double y) {
    Vector *res;
    pts_.push_back(res = new Vector{x, y});
    if (!is_in_block_) RefreshVector();
    return res;
}

Vector *Polygon::OwnVector(Vector *p)
{
    pts_.push_back(p);
    if (!is_in_block_) RefreshVector();
    return p;
}

void Polygon::IterateVectors(std::function<bool (Vector *)> f) {
    assert(f);
    is_in_iteration_ = true;
    auto old_size = pts_.size();
    for (auto &p : pts_)
    {
        if (!f(p)) break;
    }
    if (old_size != pts_.size())
    {
        RefreshVector();
    }
    is_in_iteration_ = false;
}

void Polygon::IterateVectors(std::function<bool (const Vector *)> f) const {
    assert(f);
    auto this_new = const_cast<Polygon *>(this);
    this_new->is_in_iteration_ = true;
    auto old_size = pts_.size();
    for (auto &p : pts_)
    {
        if (!f(p)) break;
    }
    if (old_size != pts_.size())
    {
        this_new->RefreshVector();
    }
    this_new->is_in_iteration_ = false;
}

void Polygon::DeleteVector(Vector* p) {
    delete p;
    pts_.remove(p);
    if (!is_in_iteration_) RefreshVector();
}

Vector * Polygon::DetachVector(Vector* p)
{
    pts_.remove(p);
    if (!is_in_iteration_) RefreshVector();
    return p;
}

void Polygon::ClearVectors() {
    for (auto &p : pts_) {
        delete p;
    }
    pts_.clear();
    RefreshVector();
}

std::size_t Polygon::GetVectorCount() const
{
    return pts_.size();
}

namespace {

inline bool DoIsInside(const std::vector<Vector> &pts, const Vector &pt)
{
    return boost::geometry::covered_by(pt, pts);
}

inline bool DoIsInside(const std::vector<Vector *> &pts, const Vector &pt)
{
    return boost::geometry::covered_by(pt, pts);
}

}

bool Polygon::IsInside(const Vector& p) const {
    assert(!is_in_iteration_);
    return DoIsInside(pts_vect_, (p-position).rotated(-rotation));
}

bool Polygon::IsInside(double x, double y) const {
    return IsInside(Vector {x, y});
}

bool Polygon::FastIsInside(const masosi::Vector& p) const
{
    return DoIsInside(fast_pts_, p);
}

bool Polygon::FastIsInside(double x, double y) const
{
    return FastIsInside(Vector {x, y});
}

void Polygon::PrepareFast()
{
    DoPreparePoints(fast_pts_);
}

namespace { // better than static inline STH { ... }

inline bool DoIntersects(
    const std::vector<Vector> &poly1,
    const std::vector<Vector> &poly2)
{
    return boost::geometry::intersects(poly1, poly2);
}

}

bool Polygon::FastIntersects(const Polygon& polygon) const
{
    return DoIntersects(fast_pts_, polygon.fast_pts_);
}

bool Polygon::Intersects(const Polygon& polygon) const
{
    std::vector<Vector> this_polygon;
    std::vector<Vector> other_polygon;
    DoPreparePoints(this_polygon);
    polygon.DoPreparePoints(other_polygon);
    return DoIntersects(this_polygon, other_polygon);
}

namespace {

inline std::vector<Vector> DoIntersectionsWithSegment(
    const std::vector<Vector>& poly,
    const Vector& a,
    const Vector& b)
{
    if (poly.size() == 0) return {};
    std::vector<Vector> result;
    result.reserve(20);
    for (auto it = poly.begin(); it != poly.end() - 1; ++it)
    {
        std::vector<Vector> subresult;
        subresult.reserve(5);
        boost::geometry::model::segment<Vector> segment1(*it, *(it+1));
        boost::geometry::model::segment<Vector> segment2(a, b);
        boost::geometry::intersection(segment1, segment2, subresult);
        std::copy(
            subresult.begin(),
            subresult.end(),
            std::back_inserter(result));
    }
    return result;
}

}

std::vector<Vector> Polygon::FastIntersectionsWithSegment(
    const Vector& a,
    const Vector& b)
{
    return DoIntersectionsWithSegment(fast_pts_, a, b);
}

std::vector<Vector> Polygon::IntersectionsWithSegment(
    const Vector& a,
    const Vector& b)
{
    std::vector<Vector> this_polygon;
    DoPreparePoints(this_polygon);
    return DoIntersectionsWithSegment(this_polygon, a, b);
}

Polygon::~Polygon() {
    ClearVectors();
}

void Polygon::DoPreparePoints(std::vector<Vector>& output) const
{
    output.reserve(pts_.size() + 1);
    for (auto &o: pts_)
        output.push_back(o->rotated(rotation) + position);
    output.push_back(output.front());
}

void Polygon::RefreshVector() {
    pts_vect_.clear();
    pts_vect_.reserve(pts_.size() + 1);
    for (auto &p : pts_)
    {
        pts_vect_.push_back(p);
    }
    pts_vect_.push_back(pts_.front());
}

void RegisterPolygon(lua_State *L, const std::string& name) {
    sol::state_view lua{L};
    lua.new_usertype<Polygon>(
        name,
        sol::constructors<
            sol::types<>,
            sol::types<const Polygon &>
        >(),
        "new_unsafe", sol::overload(
            []() -> Polygon * { return new Polygon; },
            [](const Polygon &other) -> Polygon *
            { return new Polygon(other); }),
        "copy_from", &Polygon::copy_from,
        "begin_points", &Polygon::BeginVectors,
        "end_points", &Polygon::EndVectors,
        "add_point", sol::overload(
            [](Polygon &o, const Vector &p)
            {
                return o.AddVector(p);
            },
            [](Polygon &o, double x, double y)
            {
                return o.AddVector(x, y);
            }),
        "own_point", &Polygon::OwnVector,
        "iterate_points",
            [](Polygon *obj, std::function<bool (Vector *)> f)
            { obj->IterateVectors(f); },
        "delete_point", &Polygon::DeleteVector,
        "detach_point", &Polygon::DetachVector,
        "clear_points", &Polygon::ClearVectors,
        "get_point_count", &Polygon::GetVectorCount,
        "is_inside", sol::overload(
            (bool (Polygon:: *)(const Vector &) const) &Polygon::IsInside,
            (bool (Polygon:: *)(double, double) const) &Polygon::IsInside),
        "fast_is_inside", sol::overload(
            (bool (Polygon:: *)(const Vector &) const) &Polygon::FastIsInside,
            (bool (Polygon:: *)(double, double) const) &Polygon::FastIsInside),
        "prepare_fast", &Polygon::PrepareFast,
        "fast_intersects", &Polygon::FastIntersects,
        "intersects", &Polygon::Intersects,
        "fast_intersections_with_segment",
            &Polygon::FastIntersectionsWithSegment,
        "intersections_with_segment", &Polygon::IntersectionsWithSegment,
        "color", &Polygon::color,
        "border_color", &Polygon::border_color,
        "position", &Polygon::position,
        "rotation", &Polygon::rotation,
        "border", &Polygon::border,
        "enabled", &Polygon::enabled,
        "shown", &Polygon::shown,
        "name", &Polygon::name,
        "type", &Polygon::type,
        "z_order", &Polygon::z_order,
        "delete", [](Polygon *obj) { delete obj; }
    );
}

/**
 * Simulation
 */

Simulation::Simulation()
{
}

Polygon * Simulation::NewPolygon()
{
    auto polygon = new Polygon;
    polygons_.push_back(polygon);
    return polygon;
}

Polygon *Simulation::AddPolygon(const Polygon& other)
{
    auto polygon = new Polygon(other);
    polygons_.push_back(polygon);
    return polygon;
}

Polygon * Simulation::OwnPolygon(Polygon* polygon)
{
    polygons_.push_back(polygon);
    return polygon;
}

void Simulation::IteratePolygons(std::function<bool (Polygon *)> f)
{
    assert(f);
    for (auto &polygon: polygons_)
    {
        if (!f(polygon)) break;
    }
}

void Simulation::IteratePolygons(std::function<bool (const Polygon *)> f) const
{
    assert(f);
    for (auto &polygon: polygons_)
    {
        if (!f(polygon)) break;
    }
}

void Simulation::DeletePolygon(Polygon* polygon)
{
    delete polygon;
    polygons_.remove(polygon);
}

Polygon *Simulation::DetachPolygon(Polygon* polygon)
{
    polygons_.remove(polygon);
    return polygon;
}

void Simulation::ClearPolygons()
{
    for (auto &polygon: polygons_)
    {
        delete polygon;
    }
    polygons_.clear();
}

std::size_t Simulation::GetPolygonCount() const
{
    return polygons_.size();
}

#define COLLISION_CHECK_CODE_WITH_POLYGON \
    std::vector<Vector> this_polygon; \
    this_polygon.reserve(p.pts_.size() + 1); \
    for (auto &o: p.pts_) \
        this_polygon.push_back(o->rotated(p.rotation) + p.position); \
    this_polygon.push_back(this_polygon.front()); \
 \
    for (auto &o: polygons_) \
    { \
        std::vector<Vector> that_polygon; \
        that_polygon.reserve(o->pts_.size() + 1); \
        for (auto &pt: o->pts_) \
            that_polygon.push_back(pt->rotated(o->rotation) + o->position); \
        that_polygon.push_back(that_polygon.front()); \
        if (o->enabled && boost::geometry::intersects( \
            this_polygon, that_polygon)) \
            if (!func(o)) return ; \
    }

void Simulation::CollisionCheck(const Polygon &p, std::function<bool (Polygon *)> func)
{
    COLLISION_CHECK_CODE_WITH_POLYGON
}

void Simulation::CollisionCheck(const Polygon &p, std::function<bool (const Polygon *)> func) const
{
    COLLISION_CHECK_CODE_WITH_POLYGON
}

#undef COLLISION_CHECK_CODE_WITH_POLYGON

#define COLLISION_CHECK_CODE_WITH_VECTOR \
    assert(func); \
    for (auto &o: polygons_) \
    { \
        if (o->IsInside(p)) \
        { \
            if (!func(o)) break; \
        } \
    }

void Simulation::CollisionCheck(const Vector &p, std::function<bool (Polygon *)> func)
{
    COLLISION_CHECK_CODE_WITH_VECTOR
}

void Simulation::CollisionCheck(const Vector &p, std::function<bool (const Polygon *)> func) const
{
    COLLISION_CHECK_CODE_WITH_VECTOR
}

#undef COLLISION_CHECK_CODE_WITH_VECTOR

Simulation::~Simulation()
{
    ClearPolygons();
}

void RegisterSimulation(lua_State *L, const std::string& name)
{
    sol::state_view lua{L};
    lua.new_usertype<Simulation>(
        name,
        "new_unsafe", []() { return new Simulation; },
        "new_polygon", &Simulation::NewPolygon,
        "add_polygon", &Simulation::AddPolygon,
        "own_polygon", &Simulation::OwnPolygon,
        "iterate_polygons",
            [](Simulation * obj, std::function<bool (Polygon *)> f)
            { obj->IteratePolygons(f); },
        "delete_polygon", &Simulation::DeletePolygon,
        "detach_polygon", &Simulation::DetachPolygon,
        "clear_polygons", &Simulation::ClearPolygons,
        "get_polygon_count", &Simulation::GetPolygonCount,
        "collision_check", sol::overload(
            [](
                Simulation *sim,
               const Polygon &p,
               std::function<bool (Polygon *)> func) {
                   sim->CollisionCheck(p, func);
            },
            [](
                Simulation *sim,
               const Vector &p,
               std::function<bool (Polygon *)> func) {
                   sim->CollisionCheck(p, func);
            }),
        "delete", [](Simulation *obj) { delete obj; }
    );
}

}
