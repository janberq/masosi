#include "CompoundView.hpp"

#include "RegisterWxWidgets.hpp"

#include <mutex>
#include <utility>
#include <algorithm>
#include <cassert>

#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/menu.h>
#include <wx/msgdlg.h>
#include <wx/splitter.h>
#include <wx/notebook.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/radiobut.h>

#include <sol.hpp>

#define UNUSED(x)

// Event Types
wxDEFINE_EVENT(COMPVIEW_EVT_ERROR,          wxThreadEvent);
wxDEFINE_EVENT(COMPVIEW_EVT_THREAD_EXIT,    wxThreadEvent);
wxDEFINE_EVENT(COMPVIEW_EVT_PUSH_OUTPUT,    wxThreadEvent);
wxDEFINE_EVENT(COMPVIEW_EVT_REFRESH_REQUEST,wxThreadEvent);
wxDEFINE_EVENT(COMPVIEW_EVT_REFRESH_POLYGON_REQUEST,    wxThreadEvent);
wxDEFINE_EVENT(COMPVIEW_EVT_INPUT_REQUEST,  wxThreadEvent);

namespace masosi {

static constexpr int defaultTimeout = 500 /* ms */;

//BEGIN CompoundViewPrefs

CompoundViewPrefs::CompoundViewPrefs()
{
    Defaults();
}

CompoundViewPrefs::CompoundViewPrefs(const std::string& script)
{
    LoadScript(script);
}

void CompoundViewPrefs::Defaults()
{
    luaEditorPrefs.Defaults();
    outputPanelPrefs.background = *wxWHITE;
    outputPanelPrefs.foreground = *wxBLACK;
    outputPanelPrefs.font = wxFont(
        12,
        wxFONTFAMILY_TELETYPE,
        wxFONTSTYLE_NORMAL,
        wxFONTWEIGHT_NORMAL);
    verticalSashPos = -400;
    verticalSashGrav = 0.7;
    horizontalSashPos = -200;
    horizontalSashGrav = 0.7;

    showStartStopButton = true;
    showPauseResumeButton = true;
    showStatusText = true;
    showSaveButton = true;
    showRestoreButton = true;
    showOutputPanel = true;
    showLuaEditors = true;
    /*
     * TODO Uncomment here if I support them
    showRightPanel = true;
    showSimulationPanel = true;
    */
}

void CompoundViewPrefs::LoadScript(const std::string& script)
{
    sol::state lua;
    lua.open_libraries(
        sol::lib::base,
        sol::lib::io,
        sol::lib::math,
        sol::lib::string,
        sol::lib::table);

    RegisterWxFont(lua);
    RegisterWxColour(lua);
    LuaEditorPrefs::RegisterToLua(lua);
    CompoundViewPrefs::RegisterToLua(lua);

    lua["compoundViewPrefs"] = this;

    lua.script(script);
}

void CompoundViewPrefs::RegisterToLua(lua_State *L)
{
    sol::state_view lua{L};

#define REGISTER_FUNC(class, name) #name , & class :: name

    typedef decltype(CompoundViewPrefs::outputPanelPrefs) opp_t;
    lua.new_usertype<opp_t>(
        "OutputPanelPrefs",
        REGISTER_FUNC(opp_t, background),
        REGISTER_FUNC(opp_t, foreground),
        REGISTER_FUNC(opp_t, font)
    );

    lua.new_usertype<CompoundViewPrefs>(
        "CompoundViewPrefs",
        REGISTER_FUNC(CompoundViewPrefs, luaEditorPrefs),
        REGISTER_FUNC(CompoundViewPrefs, outputPanelPrefs),
        REGISTER_FUNC(CompoundViewPrefs, verticalSashPos),
        REGISTER_FUNC(CompoundViewPrefs, horizontalSashPos),
        REGISTER_FUNC(CompoundViewPrefs, verticalSashGrav),
        REGISTER_FUNC(CompoundViewPrefs, horizontalSashGrav)
    );
#undef REGISTER_FUNC

}

//END

// just as a helper
static inline wxString toWxString(const std::string &str) {
    return wxString::FromUTF8(str.c_str(), str.size());
}

static inline std::string toStdString(const wxString &str) {
    return str.mb_str(wxConvUTF8).data();
}

static wxString g_page_names [Simulator::SCRIPT_COUNT] = {
    _("Drawing"),
    _("Prelude"),
    _("Simulation"), // DEFAULT
    _("Postlude")
};

const wxString CompoundViewName = "compound_view";

struct CompoundView::Impl :
    public CompoundView::ActionsHelper,
    public CompoundView::EventHandler
{
    Impl(CompoundView *obj);

    bool Create(
        wxWindow *parent,
        wxWindowID id,
        const wxPoint &pos = wxDefaultPosition,
        const wxSize &size = wxDefaultSize,
        long style = wxTAB_TRAVERSAL | wxNO_BORDER,
        const wxString &name = CompoundViewName);

    Simulator *GetSimulator();
    LuaEditor *GetLuaEditor(Simulator::EScriptType type);
    SimulationPanel *GetSimulationPanel();
    wxTextCtrl *GetOutputControl();

    CompoundViewPrefs GetPrefs() const;
    void SetPrefs(const CompoundViewPrefs &prefs);

    wxSize DoGetBestSize() const;

    Simulator::EScriptType GetCurrentScript() const;

    void ReloadScripts();

    bool IsAnyScriptModified() const;
    bool IsScriptModified(Simulator::EScriptType script) const;
    void GetModifiedScripts(Simulator::EScriptType *scripts) const;

    void AddEventHandler(EventHandler *eventHandler);
    EventHandler *DetachEventHandler(EventHandler *eventHandler);
    void DetachAllEventHandlers(EventHandler ***eventHandlersArray);
    void DeleteAllEventHandlers();
    EventHandler * const * GetEventHandlers() const;
    ActionsHelper *Actions();

    EActionResult SaveScript(
        Simulator::EScriptType script,
        CompoundView::ActionsHelper::EActionFlags flags
            = AF_INVOKE_EVENT_HANDLERS) override;

    EActionResult RestoreScript(
        Simulator::EScriptType script,
        CompoundView::ActionsHelper::EActionFlags flags
            = AF_INVOKE_EVENT_HANDLERS) override;

    EActionResult ClearScript(
        Simulator::EScriptType script,
        CompoundView::ActionsHelper::EActionFlags flags) override;

    EActionResult
    SaveCurrentScript(
        CompoundView::ActionsHelper::EActionFlags flags
            = AF_INVOKE_EVENT_HANDLERS) override;

    EActionResult
    RestoreCurrentScript(
        CompoundView::ActionsHelper::EActionFlags flags
            = AF_INVOKE_EVENT_HANDLERS) override;

    EActionResult ClearCurrentScript(
        CompoundView::ActionsHelper::EActionFlags flags) override;

    EActionResult
    Stop(
        CompoundView::ActionsHelper::EActionFlags flags = AF_DEFAULT) override;
    
    EActionResult
    Start(
        bool saveAllScriptsBefore = true,
        CompoundView::ActionsHelper::EActionFlags flags = AF_DEFAULT) override;
    
    EActionResult
    Pause(
        CompoundView::ActionsHelper::EActionFlags flags = AF_DEFAULT) override;
    
    EActionResult
    Resume(
        CompoundView::ActionsHelper::EActionFlags flags = AF_DEFAULT) override;
    
    EActionResult
    SaveAllScripts(
        CompoundView::ActionsHelper::EActionFlags flags = AF_DEFAULT) override;

    EActionResult ResetLua(
        Simulator::ELuaState state,
        CompoundView::ActionsHelper::EActionFlags flags = AF_DEFAULT) override;

    bool OnStopped() override;
    bool OnStarted() override;
    bool OnPaused() override;
    bool OnResumed() override;
    bool OnCurrentScriptChanged(
        Simulator::EScriptType old_script,
        Simulator::EScriptType new_script) override;
    bool OnScriptModified(Simulator::EScriptType script) override;
    bool OnScriptSavePointLeft(Simulator::EScriptType script) override;
    bool OnScriptSavePointReached(Simulator::EScriptType script) override;
    bool OnScriptRestored(Simulator::EScriptType script) override;
    bool OnScriptSaved(Simulator::EScriptType script) override;
    bool OnStatusChanged(const std::string & status) override;
    bool OnInputRequested(
        const std::string & ir_type,
        const std::list<std::string> & ir_strings) override;
    bool OnOutput(const std::string & output) override;

    void SetStatusText(const wxString &text, bool invokeEvents = true);

    ~Impl();

private:

    CompoundView *compview_ {nullptr};

    enum EInputPanels {
        INPUT_RESUME = 0,
        INPUT_TEXT_ENTRY,
        INPUT_RADIOBUTTONS,
        INPUT_COUNT,
        INPUT_NONE = -1
    };

    EInputPanels current_input_type_ {INPUT_NONE};

    class InputRequest
    {
    private:
        std::mutex mtx_;
    public:

        /* lock functionality */
        void lock() { mtx_.lock(); }
        void unlock() { mtx_.unlock(); }
        bool try_lock() { return mtx_.try_lock(); }

        std::string input_type;
        std::list<std::string> request_strings;

        void clear()
        {
            input_type = "";
            request_strings.clear();
        }
    } input_request_;

    class InputRequestResult
    {
    private:
        std::mutex mtx_;
        std::list<std::string>::const_iterator it_;
    public:

        /* lock functionality */
        void lock() { mtx_.lock(); }
        void unlock() { mtx_.unlock(); }
        bool try_lock() { return mtx_.try_lock(); }

        std::list<std::string> result_strings;

        void reset()
        {
            it_ = result_strings.begin();
        }

        bool next(std::string &out)
        {
            if (it_ == result_strings.end())
                return false;
            out = *it_;
            ++it_;
            return true;
        }

        void clear()
        {
            result_strings.clear();
        }

    } input_request_result_;

    void ProcessInputRequest(const InputRequest &request);

    wxWindow *input_panel_ {nullptr};

    wxWindow *CreateResumeInput(
        wxWindow *parent,
        const InputRequest &request);
    wxWindow *CreateTextEntryInput(
        wxWindow *parent,
        const InputRequest &request);
    wxWindow *CreateRadioButtonsInput(
        wxWindow *parent,
        const InputRequest &request);

    wxPanel *toppanel_ {nullptr};
    wxBoxSizer *toppanelsizer_ {nullptr};

    Simulator simulator_;

    wxWindow *CreateEditorPage(wxWindow *parent, Simulator::EScriptType type);

    LuaEditor *lua_editors_[Simulator::SCRIPT_COUNT];
    wxButton *save_buttons_[Simulator::SCRIPT_COUNT];
    wxButton *restore_buttons_[Simulator::SCRIPT_COUNT];
    Simulator::EScriptType current_script_;

    wxTextCtrl *text_output_ {nullptr};
    wxTextCtrl *text_status_ {nullptr};
    SimulationPanel *sim_panel_{nullptr};

    CompoundViewPrefs prefs_;

    wxSplitterWindow *splitter1_, *splitter2_;

    wxButton *button_start_stop_, *button_pause_resume_;

    wxNotebook *lua_editors_notebook_;

    std::vector<CompoundView::EventHandler *> event_handlers_;

    void ApplyPrefs();

    void DoResetLua(bool reset_drawing = true, bool reset_secondary = true);

    void AskForRestartingSimulationAfterChangesMade();

    bool on_script_saved_do_interaction_ {true};

    class DefaultEventHandler : public CompoundView::EventHandler
    {
    private:
        Impl *impl_;
    public:

        DefaultEventHandler(Impl *impl) : impl_{impl} {  }
    };

    friend class DefaultEventHandler;

    DefaultEventHandler default_event_handler_ {this};

    template <typename F, typename ...A>
    bool InvokeEventHandlers(F &&f, A && ...args)
    {
        if (!(this->*f)(std::forward<A>(args)...)) // default
            return false;
        for (   auto it = event_handlers_.begin();
                it != event_handlers_.end() - 1 /* exclude nullptr */;
                ++it)
        {
            if (!(((*it)->*f)(std::forward<A>(args)...)))
                return false;
        }
        return true;
    }

#define COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(event)                       \
    template <typename ...A>                                                \
    bool Invoke ## event ## EventHandlers (A && ...args) {                  \
        return InvokeEventHandlers(&CompoundView::EventHandler:: event ,    \
            std::forward<A>(args)...);                                      \
    }

COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnStopped)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnStarted)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnPaused)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnResumed)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnCurrentScriptChanged)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnScriptModified)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnScriptSavePointLeft)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnScriptSavePointReached)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnScriptRestored)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnScriptSaved)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnStatusChanged)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnOutput)
COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER(OnInputRequested)

#undef COMPOUNDVIEW_IMPL_DEFINE_EVENT_INVOKER

};

CompoundView::Impl::Impl(CompoundView* obj) : compview_ {obj}
{
}

#if 0
#include <iostream>
#endif

bool CompoundView::Impl::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxPoint& pos,
    const wxSize& size,
    long style,
    const wxString& name)
{
    if (!
        (((wxPanel *) compview_)->Create(parent, id, pos, size, style, name)))
        return false;

    event_handlers_.push_back(nullptr);

    wxBoxSizer *root_sizer = new wxBoxSizer(wxVERTICAL);

    splitter1_ = new wxSplitterWindow(
        compview_,
        wxID_ANY,
        wxDefaultPosition,
        wxDefaultSize,
        0 /* no 3D sash*/);
    splitter2_ = new wxSplitterWindow(
        splitter1_,
        wxID_ANY,
        wxDefaultPosition,
        wxDefaultSize,
        0 /* no 3D sash*/);
    auto rightPanel = new wxPanel(splitter1_);
    toppanel_ = new wxPanel(splitter2_);
    auto bottomPanel = new wxPanel(splitter2_);

    splitter1_->SplitVertically(splitter2_, rightPanel, -100);
    splitter2_->SplitHorizontally(toppanel_, bottomPanel, -50);

    splitter1_->SetMinimumPaneSize(1);
    splitter2_->SetMinimumPaneSize(1);

    // right panel initialization
    {
        wxBoxSizer *sizer1 = new wxBoxSizer(wxVERTICAL);
        wxBoxSizer *sizer2 = new wxBoxSizer(wxHORIZONTAL);

        button_start_stop_ = new wxButton(rightPanel, wxID_ANY, _("Start"));
        button_pause_resume_ = new wxButton(rightPanel, wxID_ANY, _("Pause"));
        button_pause_resume_->Enable(false);

        button_start_stop_->Bind(wxEVT_BUTTON,
            [this](wxCommandEvent &UNUSED(evt)){
            // start/stop button

            if (simulator_.IsRunning()) // then stop
            {
                Stop();
            }
            else // then start
            {
                Start();
            }
        });
        button_pause_resume_->Bind(wxEVT_BUTTON,
            [this](wxCommandEvent &UNUSED(evt)) {
            // pause/resume button
            // since this button is enabled only when the simulation is running
            // no need to check it.
            if (simulator_.IsPaused())
            {
                Resume();
            }
            else
            {
                Pause();
            }
        });

        sizer2->AddStretchSpacer(1);
        sizer2->Add(button_start_stop_, wxSizerFlags(0).Expand().Border(
            wxTOP|wxRIGHT));
        sizer2->Add(button_pause_resume_, wxSizerFlags(0).Expand().Border(
            wxTOP|wxRIGHT));

        sizer1->Add(sizer2, wxSizerFlags(0).Expand().Border(0));

        lua_editors_notebook_ = new wxNotebook(rightPanel, wxID_ANY);
        for (int i = 0; i < Simulator::SCRIPT_COUNT; ++i)
        {
            auto page = CreateEditorPage(
                lua_editors_notebook_, (Simulator::EScriptType) i);
            lua_editors_notebook_->AddPage(page, g_page_names[i]);
        }
        lua_editors_notebook_->ChangeSelection(2); // simulator panel

        sizer1->Add(lua_editors_notebook_, wxSizerFlags(1).Expand().Border(wxALL));

        lua_editors_notebook_->Bind(wxEVT_NOTEBOOK_PAGE_CHANGED,
            [this](wxBookCtrlEvent &evt) {
#if 0
            std::cout << "In notebook page changed event handler" << std::endl;
            std::cout << "new selection: " << evt.GetSelection() << std::endl;
            std::cout << "old selection: " << evt.GetOldSelection()
                << std::endl;
#endif
            if (evt.GetOldSelection() > 0)
            {
                // if we do not take this block in conditional
                // it's tried to be executed after the "this" is destroyed
                // I think this is a bug of wxWidgets
                // the cheapest way to avoid this is using the condition above
                // after destroyed, before the crash would happen,
                // we have old selection == -1
                // TODO find a better solution!
                InvokeOnCurrentScriptChangedEventHandlers(
                    (Simulator::EScriptType) evt.GetOldSelection(),
                    /* i don't think it's possible to get wxNOT_FOUND here */
                    (Simulator::EScriptType) evt.GetSelection()
                );
            }
        });

        rightPanel->SetSizer(sizer1);
    }

    // top panel initialization
    {
        // top panel has input panels

        // NOTE input panels should provide no borders for their
        // interior members, it's also better if they draw a border

        toppanelsizer_ = new wxBoxSizer(wxVERTICAL);

        sim_panel_ = new SimulationPanel(toppanel_);
        sim_panel_->SetSimulator(&simulator_);

        class MyOnPaintHook1 : public SimulationPanel::OnPaintHook {
            /* ^^^ for drawing script */

            Simulator *simulator;
        public:
            MyOnPaintHook1(Simulator *p_simulator) : simulator{p_simulator} {
                z_order = 1;
            }
            void OnPaint(wxDC &dc) override {
                sol::state_view lua{
                    simulator->GetLua(Simulator::STATE_DRAWING)};
                lua["_DC"] = &dc;
                lua["_Z"] = 1;
                simulator->ExecuteDrawingScript();
            }
        };

        class MyOnPaintHook2 : public SimulationPanel::OnPaintHook {
            Simulator *simulator;
        public:
            MyOnPaintHook2(Simulator *p_simulator) : simulator{p_simulator} {
                z_order = -1;
            }
            void OnPaint(wxDC &dc) override {
                sol::state_view lua{
                    simulator->GetLua(Simulator::STATE_DRAWING)};
                lua["_DC"] = &dc;
                lua["_Z"] = -1;
                simulator->ExecuteDrawingScript();
            }
        };

        auto myonpainthook1 = new MyOnPaintHook1(&simulator_);
        auto myonpainthook2 = new MyOnPaintHook2(&simulator_);

        sim_panel_->AddOnPaintHook(myonpainthook2);
        sim_panel_->AddOnPaintHook(myonpainthook1);

        toppanelsizer_->Add(sim_panel_, wxSizerFlags(1).Expand().Border(
            wxALL));

        toppanel_->SetSizer(toppanelsizer_);
    }

    // bottom panel initialization
    {
        wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

        text_output_ = new wxTextCtrl(
            bottomPanel,
            wxID_ANY,
            wxEmptyString,
            wxDefaultPosition,
            wxDefaultSize,
            wxTE_MULTILINE |
            wxTE_READONLY |
            wxHSCROLL |
            wxVSCROLL);

        text_status_ = new wxTextCtrl(
            bottomPanel,
            wxID_ANY,
            wxEmptyString,
            wxDefaultPosition,
            wxDefaultSize,
            wxTE_READONLY);

        sizer->Add(text_output_, wxSizerFlags(1).Expand().Border(
            wxTOP|wxLEFT|wxRIGHT|wxBOTTOM));
        sizer->Add(text_status_, wxSizerFlags(0).Expand().Border(
            wxLEFT|wxRIGHT|wxBOTTOM));

        bottomPanel->SetSizer(sizer);
    }

    root_sizer->Add(splitter1_, wxSizerFlags(1).Expand().Border(0));
    compview_->SetSizer(root_sizer);

    ApplyPrefs();

    simulator_.SetExitHandler([this]() {
        wxThreadEvent evt;
        evt.SetEventType(COMPVIEW_EVT_THREAD_EXIT);
        wxQueueEvent(compview_, evt.Clone());
    });
    simulator_.SetExecutionErrorHandler([this](const std::exception &ex) {
        wxString str(ex.what(), wxConvUTF8);
        wxThreadEvent evt;
        evt.SetEventType(COMPVIEW_EVT_ERROR);
        evt.SetString(str);
        wxQueueEvent(compview_, evt.Clone());
    });

    DoResetLua();

    compview_->Bind(COMPVIEW_EVT_ERROR, [this](wxThreadEvent &evt) {
        *text_output_ << "Error: " << evt.GetString() << "\n";
    });

    compview_->Bind(COMPVIEW_EVT_THREAD_EXIT, [this](wxThreadEvent &) {
        InvokeOnStoppedEventHandlers();
    });

    compview_->Bind(COMPVIEW_EVT_PUSH_OUTPUT, [this](wxThreadEvent &evt) {
        *text_output_ << evt.GetString() << "\n";
        InvokeOnOutputEventHandlers(toStdString(evt.GetString()));
    });

    compview_->Bind(COMPVIEW_EVT_REFRESH_REQUEST, [this](wxThreadEvent &) {
        // i do not want multiple refresh events to accumulate
        wxEventBlocker blocker(compview_, COMPVIEW_EVT_REFRESH_REQUEST);
        sim_panel_->RefreshAll(); // maybe I need some kind of other stuff?
    });

    compview_->Bind(
        COMPVIEW_EVT_REFRESH_POLYGON_REQUEST, [this](wxThreadEvent &evt) {
        sim_panel_->RefreshPolygon(evt.GetPayload<Polygon*>());
        sim_panel_->Refresh();
    });

    compview_->Bind(COMPVIEW_EVT_INPUT_REQUEST, [this](wxThreadEvent &) {
        ProcessInputRequest(input_request_);
    });

    return true;
}

Simulator * CompoundView::Impl::GetSimulator()
{
    return &simulator_;
}

LuaEditor * CompoundView::Impl::GetLuaEditor(Simulator::EScriptType type)
{
    return lua_editors_[type];
}

SimulationPanel * CompoundView::Impl::GetSimulationPanel()
{
    return sim_panel_;
}

wxTextCtrl * CompoundView::Impl::GetOutputControl()
{
    return text_output_;
}

CompoundViewPrefs CompoundView::Impl::GetPrefs() const
{
    return prefs_;
}

void CompoundView::Impl::SetPrefs(const CompoundViewPrefs& prefs)
{
    prefs_ = prefs;
    ApplyPrefs();
}

void CompoundView::Impl::ProcessInputRequest(
    const CompoundView::Impl::InputRequest& request)
{
    if (current_input_type_ != INPUT_NONE) return ;
    {
        std::unique_lock<InputRequestResult> lock{input_request_result_};
        input_request_result_.clear();
    }
    input_request_.lock();
    simulator_.WaitForWaiting(100);
    if (simulator_.IsWaiting())
    {
        InvokeOnInputRequestedEventHandlers(
            input_request_.input_type,
            input_request_.request_strings
        );
        input_panel_ = nullptr;
        if (request.input_type == "INPUT_RESUME")
        {
            input_panel_ = CreateResumeInput(toppanel_, request);
        }
        else if (request.input_type == "INPUT_TEXT_ENTRY")
        {
            input_panel_ = CreateTextEntryInput(toppanel_, request);
        }
        else if (request.input_type == "INPUT_RADIOBUTTONS")
        {
            input_panel_ = CreateRadioButtonsInput(toppanel_, request);
        }
        else
        {
            // do nothing
        }

        if (input_panel_)
        {
            toppanelsizer_->Insert(0, input_panel_,
                wxSizerFlags(0).Expand().Border(wxTOP|wxLEFT|wxRIGHT));
            toppanel_->SendSizeEvent();
        }
        else
        {
            simulator_.WakeUpWait();
        }
        input_request_.clear();
    }
    else
    {
        // strange situation :/
        // but whatever, it's better than being freeze
        wxMessageBox(_(
            "An input request was made, but it seems like "
            "the script is not waiting for the result. It may be "
            "due the bad coding of the script.\n"
            "If the script isn't working fine, "
            "you can just terminate it."),
            _("Input Request Error"),
            wxICON_ERROR | wxOK | wxCENTRE,
            compview_
        );
    }
}

wxWindow * CompoundView::Impl::CreateResumeInput(
    wxWindow *parent,
    const CompoundView::Impl::InputRequest &request)
{
    // some assertions
    // TODO convert them to less harmful messages
    assert(request.input_type == "INPUT_RESUME");
    assert(request.request_strings.size() >= 1);

    current_input_type_ = INPUT_RESUME; // just for informative purposes

    auto it = request.request_strings.begin();
    auto end = request.request_strings.end();
    wxString strDescription = toWxString(*it); ++it;
    wxString strResumeText;
    if (it != end)  strResumeText = toWxString(*it);
    else            strResumeText = _("Resume");

    // resume output is made of several things:
    //   Some informative text message
    //   Resume button
    auto panel = new wxPanel(parent);
    auto descriptive_text = new wxStaticText(panel, wxID_ANY, strDescription);
    auto resume_button = new wxButton(panel, wxID_ANY, strResumeText);

    resume_button->SetFocus();
    resume_button->SetDefault();

    auto sizer = new wxBoxSizer(wxHORIZONTAL);

    sizer->Add(descriptive_text, wxSizerFlags(1).Expand().Border(wxRIGHT));
    sizer->Add(resume_button, wxSizerFlags(0).Border(0).Align(wxTOP));

    resume_button->Bind(wxEVT_BUTTON,
        [this, panel](wxCommandEvent &UNUSED(evt)) {
        if (simulator_.IsPaused()) return ; // paused may fuck up everything
        // it generates no output
        current_input_type_ = INPUT_NONE;
        input_request_.unlock();
        simulator_.WakeUpWait(); // wake it up
        panel->Hide();
        toppanel_->SendSizeEvent();
        input_panel_ = nullptr;
        panel->Destroy(); // get rid off this control
    });

    panel->SetSizer(sizer);

    return panel;
}

wxWindow * CompoundView::Impl::CreateTextEntryInput(
    wxWindow *parent,
    const CompoundView::Impl::InputRequest &request)
{
    // some assertions
    // TODO convert them to less harmful messages
    assert(request.input_type == "INPUT_TEXT_ENTRY");
    assert(request.request_strings.size() >= 1);

    current_input_type_ = INPUT_TEXT_ENTRY;

    wxString strPrompt;
    wxString strButton;

    {
        auto it     = request.request_strings.begin();
        auto end    = request.request_strings.end();
        strPrompt = toWxString(*it); ++it;
        if (it != end)  { strButton = toWxString(*it); ++it; }
        else            { strButton = _("Enter"); }
    }

    auto panel = new wxPanel(parent);
    auto labelPrompt = new wxStaticText(
        panel,
        wxID_ANY,
        strPrompt);
    auto txtInput = new wxTextCtrl(panel, wxID_ANY);
    auto btnEnter = new wxButton(panel, wxID_ANY, strButton);
    btnEnter->SetDefault();
    txtInput->SetFocus();

    auto sizer1 = new wxBoxSizer(wxHORIZONTAL);
    auto sizer2 = new wxBoxSizer(wxVERTICAL);

    sizer2->Add(labelPrompt, wxSizerFlags(0).Expand().Border(wxBOTTOM));
    sizer2->Add(txtInput, wxSizerFlags(0).Expand().Border(0));

    sizer1->Add(sizer2, wxSizerFlags(1).Expand().Border(wxRIGHT));
    sizer1->Add(btnEnter, wxSizerFlags(0).Expand().Border(0));

    panel->SetSizer(sizer1);

    btnEnter->Bind(wxEVT_BUTTON,
        [this, panel, txtInput](wxCommandEvent &UNUSED(evt)) {
        if (simulator_.IsPaused()) return ;
        // push output
        {
            std::unique_lock<InputRequestResult> lock{input_request_result_};
            input_request_result_.result_strings.push_back(
                toStdString(txtInput->GetValue()));
            input_request_result_.reset();
                // needed for iterators
        }
        current_input_type_ = INPUT_NONE;
        input_request_.unlock();
        simulator_.WakeUpWait();
        panel->Hide();
        toppanel_->SendSizeEvent();
        input_panel_ = nullptr;
        panel->Destroy();
    });

    return panel;
}

// TODO why not just having void CreateSTH() since almost
// everything are already defined?
wxWindow * CompoundView::Impl::CreateRadioButtonsInput(
    wxWindow *parent,
    const CompoundView::Impl::InputRequest &request)
{
    // some assertions
    // TODO convert them to less harmful messages
    assert(request.input_type == "INPUT_RADIOBUTTONS");
    assert(request.request_strings.size() >= 2);

    current_input_type_ = INPUT_RADIOBUTTONS;

    auto it     = request.request_strings.begin();
    auto end    = request.request_strings.end();

    auto panel = new wxPanel(parent);
    auto labelPrompt = new wxStaticText(
        panel,
        wxID_ANY,
        toWxString(*(it++)));
    auto btnOK = new wxButton(panel, wxID_ANY, _("OK"));
    btnOK->SetDefault();
    btnOK->SetFocus();

    auto sizer1 = new wxBoxSizer(wxHORIZONTAL);
    auto sizer2 = new wxBoxSizer(wxVERTICAL);

    sizer2->Add(labelPrompt, wxSizerFlags(0).Expand().Border(0));

    // just to make event handlers work proper
    {
        std::unique_lock<InputRequestResult> lock{input_request_result_};
        input_request_result_.result_strings.push_back("0");
        input_request_result_.reset(); // needs to be called, unfortunately
    }

    auto new_radio = [this, panel](
        std::size_t i,
        const wxString &str,
        long style = 0) {
        auto radio = new wxRadioButton(
            panel,
            wxID_ANY,
            str,
            wxDefaultPosition,
            wxDefaultSize,
            style);
        radio->Bind(wxEVT_RADIOBUTTON,
            [i, this](wxCommandEvent &UNUSED(evt)) {
            std::unique_lock<InputRequestResult> lock{input_request_result_};
            input_request_result_.result_strings.front() = std::to_string(i);
        });
        return radio;
    };

    std::size_t i = 0;

    sizer2->Add(    // add the first radio button
        new_radio(i++, toWxString(*(it++)), wxRB_GROUP),
        wxSizerFlags(0).Expand().Border(wxUP)
    ); // it != end, this is guaranteed by the second assertion above

    for (; it != end; ++it) // add the other radio buttons if they exist
        sizer2->Add(
            new_radio(i++, toWxString(*it)),
            wxSizerFlags(0).Expand().Border(wxUP)
        );

    sizer1->Add(sizer2, wxSizerFlags(1).Expand().Border(wxRIGHT));
    sizer1->Add(btnOK, wxSizerFlags(0).Align(wxTOP).Border(0));

    panel->SetSizer(sizer1);

    btnOK->Bind(wxEVT_BUTTON,
        [this, panel](wxCommandEvent &UNUSED(evt)) {
        if (simulator_.IsPaused()) return ; // paused may fuck up everything
        // it generates no output, auto gen by radio button evthandlers
        current_input_type_ = INPUT_NONE;
        input_request_.unlock();
        simulator_.WakeUpWait(); // wake it up
        panel->Hide();
        toppanel_->SendSizeEvent();
        input_panel_ = nullptr;
        panel->Destroy(); // get rid off this control
    });

    return panel;
}

wxWindow * CompoundView::Impl::CreateEditorPage(
    wxWindow* parent,
    Simulator::EScriptType type)
{
    wxPanel *page = new wxPanel(parent, wxID_ANY);
    LuaEditor *editor = new LuaEditor(page, wxID_ANY);
    wxButton *button_save = new wxButton(page, wxID_ANY, _("Save"));
    wxButton *button_restore = new wxButton(page, wxID_ANY, _("Restore"));

    wxBoxSizer *sizer1 = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *sizer2 = new wxBoxSizer(wxHORIZONTAL);

    sizer1->Add(editor, wxSizerFlags(1).Expand().Border(wxALL));
    sizer1->Add(sizer2, wxSizerFlags(0).Expand().Border(0));

    sizer2->AddStretchSpacer(1);
    sizer2->Add(
        button_restore,
        wxSizerFlags(0).
        Align(wxALIGN_CENTER).
        Border(wxRIGHT|wxBOTTOM));
    sizer2->Add(
        button_save,
        wxSizerFlags(0).
        Align(wxALIGN_CENTER).
        Border(wxRIGHT|wxBOTTOM));

    editor->Bind(wxEVT_STC_MODIFIED,
        [=](wxStyledTextEvent &UNUSED(evt)) {
        // QUESTION should I give more info in this event handlers?
        InvokeOnScriptModifiedEventHandlers(type);
    });

    editor->Bind(
        wxEVT_STC_SAVEPOINTLEFT,
        [=](wxStyledTextEvent &UNUSED(evt)) {
        InvokeOnScriptSavePointLeftEventHandlers(type);
    });

    editor->Bind(
        wxEVT_STC_SAVEPOINTREACHED,
        [=](wxStyledTextEvent &UNUSED(evt)) {
        InvokeOnScriptSavePointReachedEventHandlers(type);
    });

    page->SetSizer(sizer1);

    button_save->Bind(wxEVT_BUTTON, [this](wxCommandEvent &){
        // SAVE
        SaveCurrentScript();
    });
    button_restore->Bind(wxEVT_BUTTON,
        [this, button_restore](wxCommandEvent &) {
        auto ID_PROMPT = wxID_HIGHEST + 1;
        auto ID_YES = wxID_HIGHEST + 2;
        auto ID_NO  = wxID_HIGHEST + 3;
        wxMenu popup;
        popup.Append(ID_PROMPT, _("Restore?"));
        popup.Append(ID_YES, _("Yes"));
        popup.Append(ID_NO, _("No"));
        popup.Enable(ID_PROMPT, false);
        popup.Bind(wxEVT_MENU,
            [this](wxCommandEvent &UNUSED(evt)) {
            // RESTORE
            RestoreCurrentScript();
        }, ID_YES);
        popup.Bind(wxEVT_MENU,
            [this](wxCommandEvent &UNUSED(evt)) {
            // do nothing
        }, ID_NO);
        button_restore->PopupMenu(&popup);
    });
    lua_editors_[type] = editor;
    save_buttons_[type] = button_save;
    restore_buttons_[type] = button_restore;
    button_save->Enable(false);
    return page;
}

void CompoundView::Impl::ApplyPrefs()
{
    for (int i = 0; i < Simulator::SCRIPT_COUNT; ++i)
    {
        lua_editors_[i]->SetPrefs(prefs_.luaEditorPrefs);
    }

    text_output_->SetBackgroundColour(prefs_.outputPanelPrefs.foreground);
    text_output_->SetBackgroundColour(prefs_.outputPanelPrefs.background);
    text_output_->SetFont(prefs_.outputPanelPrefs.font);

    splitter1_->SetSashGravity(prefs_.verticalSashGrav);
    splitter1_->SetSashPosition(prefs_.verticalSashPos);

    splitter2_->SetSashGravity(prefs_.horizontalSashGrav);
    splitter2_->SetSashPosition(prefs_.horizontalSashPos);

    button_start_stop_->Show(prefs_.showStartStopButton);
    button_pause_resume_->Show(prefs_.showPauseResumeButton);
    text_status_->Show(prefs_.showStatusText);

    for (int i = 0; i < Simulator::SCRIPT_COUNT; ++i)
        save_buttons_[i]->Show(prefs_.showSaveButton);

    for (int i = 0; i < Simulator::SCRIPT_COUNT; ++i)
        restore_buttons_[i]->Show(prefs_.showSaveButton);

    text_output_->ShowPosition(prefs_.showOutputPanel);

    for (int i = 0; i < Simulator::SCRIPT_COUNT; ++i)
        lua_editors_[i]->Show(prefs_.showLuaEditors);

    compview_->SendSizeEvent();
}

void CompoundView::Impl::DoResetLua(
    bool reset_drawing,
    bool reset_secondary)
{
    auto raw_output = [this](const char *s) {
        wxString str(s, wxConvUTF8);
        wxThreadEvent evt;
        evt.SetEventType(COMPVIEW_EVT_PUSH_OUTPUT);
        evt.SetString(str);
        wxQueueEvent(compview_, evt.Clone());
    };
    // second thread stuff
    if (reset_secondary)
    {
        simulator_.ResetLua(Simulator::STATE_SECOND_THREAD);
        sol::state_view lua{simulator_.GetLua(Simulator::STATE_SECOND_THREAD)};
        lua["raw_output"] = raw_output;
        lua["refresh"] = [this]() {
            wxThreadEvent evt;
            evt.SetEventType(COMPVIEW_EVT_REFRESH_REQUEST);
            wxQueueEvent(compview_, evt.Clone());
        };
        lua["refresh_polygon"] = [this](Polygon *poly) {
            wxThreadEvent evt;
            evt.SetPayload(poly);
            evt.SetEventType(COMPVIEW_EVT_REFRESH_POLYGON_REQUEST);
            wxQueueEvent(compview_, evt.Clone());
        };
        lua.new_usertype<InputRequest>(
            "InputRequest",
            "new", sol::no_constructor,
            "lock", &InputRequest::lock,
            "unlock", &InputRequest::unlock,
            "try_lock", &InputRequest::try_lock,
            "set_type", [](InputRequest &ir, const std::string &type) {
                ir.input_type = type;
            },
            "add_str", [](InputRequest &ir, const std::string &str) {
                ir.request_strings.push_back(str);
            },
            "clear", &InputRequest::clear,
            "send", [this]() {
                wxThreadEvent evt;
                evt.SetEventType(COMPVIEW_EVT_INPUT_REQUEST);
                wxQueueEvent(compview_, evt.Clone());
            }
        );
        lua["_IR"] = &input_request_;

        lua.new_usertype<InputRequestResult>(
            "InputRequestResult",
            "new", sol::no_constructor,
            "lock", &InputRequestResult::lock,
            "unlock", &InputRequestResult::unlock,
            "try_lock", &InputRequestResult::try_lock,
            "clear", &InputRequestResult::clear,
            "reset", &InputRequestResult::reset,
            "next", [](InputRequestResult *obj, sol::this_state s) {
                sol::state_view lua{s};
                std::string str;
                if (obj->next(str))
                    return sol::make_object(lua, str);
                else
                    return sol::make_object(lua, sol::nil);
            }
        );
        lua["_IRRES"] = &input_request_result_;

        lua.script(
//BEGIN lua initial script
R"RAW(

function wait_for_input(message, resume_text)
    if not message then error("wait_for_input: message can't be nil") end
    _IR:lock()
        _IR:set_type("INPUT_RESUME")
        _IR:add_str(message)
        if resume_text ~= nil then
            _IR:add_str(resume_text)
        end
    _IR:unlock()
    _IR.send()
    wait()
end

function get_text(prompt, button_text)
    if not prompt then error("get_text: prompt can't be nil") end
    _IR:lock()
        _IR:set_type("INPUT_TEXT_ENTRY")
        _IR:add_str(prompt)
        if button_text ~= nil then
            _IR:add_str(button_text)
        end
    _IR:unlock()
    _IR.send()
    wait()
    local result = nil
    _IRRES:lock()
        _IRRES:reset() -- not needed, but makes no harm, just for safety
        result = _IRRES:next()
    _IRRES:unlock()
    return result
end

function ask_single_choice(description, radio1, ...)
    if not description then
        error("ask_single_choice: description can't be nil")
    end
    if not radio1 then error("ask_single_choice: radio1 can't be nil") end
    _IR:lock()
        _IR:set_type("INPUT_RADIOBUTTONS")
        _IR:add_str(description)
        _IR:add_str(radio1)
        for i,v in ipairs({...}) do
            _IR:add_str(v)
        end
    _IR:unlock()
    _IR.send()
    wait()
    local result = nil
    _IRRES:lock()
        _IRRES:reset() -- same as before
        result = _IRRES:next()
    _IRRES:unlock()
    return tonumber(result)
end

)RAW"
//END
        );
    }
    // drawing thread stuff
    if (reset_drawing)
    {
        simulator_.ResetLua(Simulator::STATE_DRAWING);
        sol::state_view lua{simulator_.GetLua(Simulator::STATE_DRAWING)};
        lua["raw_output"] = raw_output;

        class SimulationPanelWrapper
        {
            SimulationPanel *panel_;
        public:
            SimulationPanelWrapper(SimulationPanel *panel) : panel_{panel}
            {
            }
            decltype(auto) GetSize() const
            {
                assert(panel_ != nullptr);
                int w, h;
                panel_->GetSize(&w, &h);
                return std::tie(w, h);
            }
            decltype(auto) GetScaleFactors() const
            {
                assert(panel_ != nullptr);
                return std::make_tuple(
                    panel_->Prefs().scaleX, panel_->Prefs().scaleY);
            }
            decltype(auto) GetZoomFactors() const
            {
                assert(panel_ != nullptr);
                auto &prefs = panel_->Prefs();
                auto &prov = *(panel_->GetZoomScaleProvider());
                return std::make_tuple(
                    prov.GetZoomMultiplier(prefs.zoomFactorXindex),
                    prov.GetZoomMultiplier(prefs.zoomFactorYindex));
            }
            decltype(auto) GetScreenOrigin() const
            {
                assert(panel_ != nullptr);
                return panel_->Prefs().screenOrigin;
            }
            decltype(auto) GetScreenOriginOffset() const
            {
                assert(panel_ != nullptr);
                return panel_->Prefs().screenOriginOffset;
            }
            decltype(auto) GetOrigin() const
            {
                assert(panel_ != nullptr);
                return panel_->Prefs().origin;
            }
            decltype(auto) W2S(const Vector &v) const
            {
                assert(panel_ != nullptr);
                return panel_->TransformW2S(v);
            }
            decltype(auto) S2W(const Vector &v) const
            {
                assert(panel_ != nullptr);
                return panel_->TransformS2W(v);
            }
        };
        lua.new_enum(
            "EScreenOriginType",
            "center",           SimulationPanelPrefs::ORIGIN_CENTER,
            "top_left",         SimulationPanelPrefs::ORIGIN_TOP_LEFT,
            "top_center",       SimulationPanelPrefs::ORIGIN_TOP_CENTER,
            "top_right",        SimulationPanelPrefs::ORIGIN_TOP_RIGHT,
            "center_left",      SimulationPanelPrefs::ORIGIN_CENTER_LEFT,
            "center_right",     SimulationPanelPrefs::ORIGIN_CENTER_RIGHT,
            "bottom_left",      SimulationPanelPrefs::ORIGIN_BOTTOM_LEFT,
            "bottom_center",    SimulationPanelPrefs::ORIGIN_BOTTOM_CENTER,
            "bottom_right",     SimulationPanelPrefs::ORIGIN_BOTTOM_RIGHT
            );

#define SIMWRAPPER_REGISTER_FUNC(x) #x , & SimulationPanelWrapper :: x
        lua.new_usertype<SimulationPanelWrapper>(
            "SimulationPanelWrapper",
            "new", sol::no_constructor,
            SIMWRAPPER_REGISTER_FUNC(GetSize),
            SIMWRAPPER_REGISTER_FUNC(GetScaleFactors),
            SIMWRAPPER_REGISTER_FUNC(GetZoomFactors),
            SIMWRAPPER_REGISTER_FUNC(GetScreenOrigin),
            SIMWRAPPER_REGISTER_FUNC(GetScreenOriginOffset),
            SIMWRAPPER_REGISTER_FUNC(GetOrigin),
            SIMWRAPPER_REGISTER_FUNC(W2S),
            SIMWRAPPER_REGISTER_FUNC(S2W)
        );
#undef SIMWRAPPER_REGISTER_FUNC
        lua["_VIEW"] = SimulationPanelWrapper(sim_panel_);
    }
}

void CompoundView::Impl::AskForRestartingSimulationAfterChangesMade()
{
    if (simulator_.IsRunning())
    {
        auto res = wxMessageBox(_(
            "Simulation is ongoing. Do you want to restart it?\n"
            "Your changes will apply after you restart it."),
            _("Restart Simulation"),
            wxICON_QUESTION | wxYES_NO | wxYES_DEFAULT | wxCENTRE,
            compview_);
        if (res == wxID_YES)
        {
            Stop(); // stop the simulator
            simulator_.StopWait(defaultTimeout);
            if (!simulator_.IsRunning())
            {
                Start();
            }
            else
            {
                // timeout
                wxMessageBox(
                    _("Cannot restart the \
                    simulation (couldn't stop it)."),
                    _("Restart Simulation"),
                    wxICON_ERROR | wxOK | wxCENTRE,
                    compview_);
            }
        }
        else
        {
            // do nothing
        }
    }
}

wxSize CompoundView::Impl::DoGetBestSize() const
{
    return wxSize(800, 600);
}

Simulator::EScriptType CompoundView::Impl::GetCurrentScript() const
{
    auto sel = lua_editors_notebook_->GetSelection();
    if (sel != wxNOT_FOUND)
    {
        return (Simulator::EScriptType) sel;
    }
    else
    {
        return Simulator::SCRIPT_NONE;
    }
}

void CompoundView::Impl::ReloadScripts()
{
    simulator_.StopWait(defaultTimeout);
    if (simulator_.IsRunning())
    {
        if (wxMessageBox(_(
            "Cannot reload scripts as the simulation couldn't be stopped!\n"
            "Try again?"),
            _("Reload Scripts - Stop Simulation Error!"),
            wxICON_ERROR | wxYES_NO | wxYES_DEFAULT | wxCENTRE,
            compview_
        ) == wxYES)
        {
            ReloadScripts();
        }
    }
    else
    {
        for (int i = 0; i < Simulator::SCRIPT_COUNT; ++i)
        {
            RestoreScript((Simulator::EScriptType) i);
        }
    }
}

bool CompoundView::Impl::IsAnyScriptModified() const
{
    for (int i = 0; i < Simulator::SCRIPT_COUNT; ++i)
    {
        if (lua_editors_[i]->IsModified()) return true;
    }
    return false;
}

bool CompoundView::Impl::IsScriptModified(Simulator::EScriptType script) const
{
    assert(
        script < Simulator::SCRIPT_COUNT && script > Simulator::SCRIPT_NONE);
    return lua_editors_[script]->IsModified();
}

void CompoundView::Impl::GetModifiedScripts(
    Simulator::EScriptType* scripts) const
{
    assert(scripts != nullptr);
    for (
        int i = 0, j = 0;
        i < Simulator::SCRIPT_COUNT;
        ++i)
    {
        if (lua_editors_[i]->IsModified())
        {
            scripts[j] = (Simulator::EScriptType) i;
            ++j;
        }
    }
}

void CompoundView::Impl::AddEventHandler(
    CompoundView::EventHandler* eventHandler)
{
    assert(eventHandler != nullptr);
    event_handlers_.back() = eventHandler;
    event_handlers_.push_back(nullptr);
}

CompoundView::EventHandler * CompoundView::Impl::DetachEventHandler(
    CompoundView::EventHandler* eventHandler)
{
    assert(eventHandler != nullptr);
    auto it = std::find(
        event_handlers_.begin(),
        event_handlers_.end(),
        eventHandler);
    if (it != event_handlers_.end())
    {
        event_handlers_.erase(it);
    }
    return eventHandler;
}

void CompoundView::Impl::DetachAllEventHandlers(
    CompoundView::EventHandler *** eventHandlersArray)
{
    assert(eventHandlersArray);
    *eventHandlersArray =
        new CompoundView::EventHandler *[event_handlers_.size()];
    std::size_t i = 0;
    for (auto &o: event_handlers_)
    {
        (*eventHandlersArray)[i] = o;
        i++;
    }
    event_handlers_.clear();
    event_handlers_.push_back(nullptr); /* for null-terminated-ness */
}

void CompoundView::Impl::DeleteAllEventHandlers()
{
    for (auto &o: event_handlers_)
        delete o; // note: it's safe to delete nullptr
    event_handlers_.clear();
    event_handlers_.push_back(nullptr);
}

CompoundView::EventHandler *const *
CompoundView::Impl::GetEventHandlers() const
{
    return &event_handlers_[0];
}

CompoundView::ActionsHelper * CompoundView::Impl::Actions()
{
    return this;
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::SaveScript(
    Simulator::EScriptType script,
    CompoundView::ActionsHelper::EActionFlags flags)
{
    if (script == Simulator::SCRIPT_NONE)
        return AR_FAIL;
    auto do_save = [&] {
        auto editor = lua_editors_[script];
        editor->SetSavePoint();
        simulator_.SetScript(
            script,
            toStdString(editor->GetText())
        );
    };
    if (flags & AF_ASK_USER)
    {
        auto r = wxMessageBox(
            wxString::Format(_(
                "Do you want to save script %s?"
            ), g_page_names[script]),
            _("Save Script"),
            wxICON_QUESTION | wxYES | wxNO | wxYES_DEFAULT | wxCENTRE,
            compview_
        );
        if (r == wxYES)
        {
            do_save();
        }
        else
        {
            return AR_USER_CANCELLED;
        }
    }
    else
    {
        do_save();
    }
    if (flags & AF_INVOKE_EVENT_HANDLERS)
    {
        on_script_saved_do_interaction_ = flags & AF_ASK_USER;
        InvokeOnScriptSavedEventHandlers(script);
    }
    return AR_DONE;
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::RestoreScript(
    Simulator::EScriptType script,
    CompoundView::ActionsHelper::EActionFlags flags)
{
    if (script == Simulator::SCRIPT_NONE)
        return AR_FAIL;
    auto do_restore = [&] {
        auto editor = lua_editors_[script];
        editor->SetText(toWxString(simulator_.GetScript(script)));
        editor->SetSavePoint();
    };
    if (flags & AF_ASK_USER)
    {
        auto r = wxMessageBox(
            wxString::Format(_(
                "Do you want to restore script %s?"
            ), g_page_names[script]),
            _("Restore Script"),
            wxICON_QUESTION | wxYES | wxNO | wxYES_DEFAULT | wxCENTRE,
            compview_
        );
        if (r == wxYES)
        {
            do_restore();
        }
        else
        {
            return AR_USER_CANCELLED;
        }
    }
    else
    {
        do_restore();
    }
    if (flags & AF_INVOKE_EVENT_HANDLERS)
    {
        InvokeOnScriptRestoredEventHandlers(script);
    }
    return AR_DONE;
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::ClearScript(Simulator::EScriptType script, CompoundView::ActionsHelper::EActionFlags flags)
{
    if (script == Simulator::SCRIPT_NONE)
        return AR_FAIL;
    auto do_clear = [&] {
        auto editor = lua_editors_[script];
        editor->SetText("");
    };
    if (flags & AF_ASK_USER)
    {
        auto r = wxMessageBox(
            wxString::Format(_(
                "Do you want to clear script %s?"
            ), g_page_names[script]),
            _("Restore Script"),
            wxICON_QUESTION | wxYES | wxNO | wxYES_DEFAULT | wxCENTRE,
            compview_
        );
        if (r == wxYES)
        {
            do_clear();
        }
        else
        {
            return AR_USER_CANCELLED;
        }
    }
    else
    {
        do_clear();
    }
    if (flags & AF_INVOKE_EVENT_HANDLERS)
    {
        InvokeOnScriptRestoredEventHandlers(script);
    }
    return AR_DONE;
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::SaveCurrentScript(
    CompoundView::ActionsHelper::EActionFlags flags)
{
    return SaveScript(GetCurrentScript(), flags);
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::RestoreCurrentScript(
    CompoundView::ActionsHelper::EActionFlags flags)
{
    return RestoreScript(GetCurrentScript(), flags);
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::ClearCurrentScript(CompoundView::ActionsHelper::EActionFlags flags)
{
    return ClearScript(GetCurrentScript(), flags);
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::Stop(
    CompoundView::ActionsHelper::EActionFlags flags)
{
    if (!simulator_.IsRunning())
        return AR_ALREADY_DONE;
    auto do_stop = [this] {
        simulator_.Stop();
    };
    if (flags & AF_ASK_USER)
    {
        auto r = wxMessageBox(
            "Do you want to stop the simulation?",
            _("Restore Script"),
            wxICON_QUESTION | wxYES | wxNO | wxYES_DEFAULT | wxCENTRE,
            compview_
        );
        if (r == wxYES)
        {
            do_stop();
        }
        else
        {
            return AR_USER_CANCELLED;
        }
    }
    else
    {
        do_stop();
    }
    return CompoundView::ActionsHelper::AR_DONE;
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::Start(
    bool saveAllScriptsBefore, CompoundView::ActionsHelper::EActionFlags flags)
{
    if (simulator_.IsRunning())
        return AR_ALREADY_DONE;
    if (
        saveAllScriptsBefore ?
        (SaveAllScripts(flags) != AR_USER_CANCELLED) : true)
    {
        simulator_.Start();
        if (flags & AF_INVOKE_EVENT_HANDLERS)
        {
            InvokeOnStartedEventHandlers();
        }
        return AR_DONE;
    }
    else
    {
        return AR_USER_CANCELLED;
    }
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::Pause(CompoundView::ActionsHelper::EActionFlags flags)
{
    if (!simulator_.IsRunning())
        return AR_INVALID_STATE;
    if (simulator_.IsPaused())
        return AR_ALREADY_DONE;
    simulator_.PauseWait(defaultTimeout);
    if (simulator_.IsPaused())
    {
        if (flags & AF_INVOKE_EVENT_HANDLERS)
        {
            InvokeOnPausedEventHandlers();
        }
    }
    else
    {
        /* QUESTION should I do something else? */
        return AR_FAIL;
    }
    return AR_DONE;
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::Resume(CompoundView::ActionsHelper::EActionFlags flags)
{
    if (!simulator_.IsRunning())
        return AR_INVALID_STATE;
    if (!simulator_.IsPaused())
        return AR_ALREADY_DONE;
    simulator_.ResumeWait(defaultTimeout);
    if (!simulator_.IsPaused())
    {
        if (flags & AF_INVOKE_EVENT_HANDLERS)
        {
            InvokeOnResumedEventHandlers();
        }
    }
    else
    {
        /* QUESTION should I do something else? */
        return AR_FAIL;
    }
    return AR_DONE;
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::SaveAllScripts(
    CompoundView::ActionsHelper::EActionFlags flags)
{
    on_script_saved_do_interaction_ = false;
    bool any_script_changed = false;
    if (flags & AF_ASK_USER)
    {
        for (int i = 0; i < Simulator::SCRIPT_COUNT; ++i)
        {
            if (lua_editors_[i]->IsModified())
            {
                auto r = wxMessageBox(
                    wxString::Format(_(
                    "Script %s is changed, do you want to save it?"),
                    g_page_names[i]),
                    _("Save All Scripts"),
                    wxICON_QUESTION | wxYES | wxNO | wxCANCEL |
                    wxYES_DEFAULT | wxCENTRE,
                    compview_
                );
                if (r == wxYES)
                {
                    lua_editors_[i]->SetSavePoint();
                    simulator_.SetScript(
                        (Simulator::EScriptType) i,
                        lua_editors_[i]->GetText().mb_str(wxConvUTF8).data());
                    if (flags & AF_INVOKE_EVENT_HANDLERS)
                    {
                        InvokeOnScriptSavedEventHandlers(
                            (Simulator::EScriptType) i);
                    }
                    any_script_changed = true;
                }
                else if (r == wxCANCEL)
                {
                    return AR_USER_CANCELLED; // stop execution
                }
                else // close mbox, no etc.
                {
                    // do nothing
                }
            }
        }
    }
    else
    {
        for (int i = 0; i < Simulator::SCRIPT_COUNT; ++i)
        {
            if (lua_editors_[i]->IsModified())
            {
                lua_editors_[i]->SetSavePoint();
                simulator_.SetScript(
                    (Simulator::EScriptType) i,
                    lua_editors_[i]->GetText().mb_str(wxConvUTF8).data());
                if (flags & AF_INVOKE_EVENT_HANDLERS)
                {
                    InvokeOnScriptSavedEventHandlers(
                        (Simulator::EScriptType) i);
                }
                any_script_changed = true;
            }
        }
    }
    if (flags & AF_ASK_USER && any_script_changed)
    {
        AskForRestartingSimulationAfterChangesMade();
    }
    return CompoundView::ActionsHelper::AR_DONE;
}

CompoundView::ActionsHelper::EActionResult
CompoundView::Impl::ResetLua(
    Simulator::ELuaState state,
    CompoundView::ActionsHelper::EActionFlags UNUSED(flags))
{
    if (simulator_.IsRunning())
        return AR_INVALID_STATE;
    if (state == Simulator::STATE_DRAWING)
    {
        DoResetLua(true, false);
    }
    else if (state == Simulator::STATE_SECOND_THREAD)
    {
        DoResetLua(false, true);
    }
    return AR_DONE;
}

bool CompoundView::Impl::OnStopped()
{
    button_start_stop_->SetLabel(_("Start"));
    button_pause_resume_->Enable(false);
    SetStatusText(_("Stopped."));
    sim_panel_->RefreshAll();
    if (input_panel_)
    {
        input_panel_->Destroy();
        input_panel_ = nullptr;
        toppanel_->SendSizeEvent();
    }
    current_input_type_ = INPUT_NONE;
    input_request_.unlock();
    return true;
}

bool CompoundView::Impl::OnStarted()
{
    button_start_stop_->SetLabel(_("Stop"));
    SetStatusText(_("Started."));
    button_pause_resume_->Enable(true);
    button_pause_resume_->SetLabel(_("Pause"));
        // if it was stopped while paused before
    return true;
}

bool CompoundView::Impl::OnPaused()
{
    SetStatusText(_("Paused."));
    button_pause_resume_->SetLabel(_("Resume"));
    return true;
}

bool CompoundView::Impl::OnResumed()
{
    SetStatusText(_("Started."));
    button_pause_resume_->SetLabel(_("Pause"));
    return true;
}

bool CompoundView::Impl::OnCurrentScriptChanged(
    Simulator::EScriptType UNUSED(old_script),
    Simulator::EScriptType UNUSED(new_script))
{
    // nothing to do
    return true;
}

bool CompoundView::Impl::OnScriptModified(
    Simulator::EScriptType script)
{
    // nothing to do
    return true;
}

bool CompoundView::Impl::OnScriptSavePointLeft(Simulator::EScriptType script)
{
    save_buttons_[script]->Enable(true);
    lua_editors_notebook_->SetPageText(script, g_page_names[script] + " *");
    return true;
}

bool CompoundView::Impl::OnScriptSavePointReached(
    Simulator::EScriptType script)
{
    save_buttons_[script]->Enable(false);
    lua_editors_notebook_->SetPageText(script, g_page_names[script]);
    return true;
}

bool CompoundView::Impl::OnScriptRestored(
    Simulator::EScriptType script)
{
    save_buttons_[script]->Enable(false);
    lua_editors_notebook_->SetPageText(script, g_page_names[script]);
    return true;
}

bool CompoundView::Impl::OnScriptSaved(
    Simulator::EScriptType UNUSED(script))
{
    if (on_script_saved_do_interaction_)
    {
        AskForRestartingSimulationAfterChangesMade();
    }
    return true;
}

bool CompoundView::Impl::OnStatusChanged(
    const std::string& UNUSED(status))
{
    // nothing to do
    return true;
}

bool CompoundView::Impl::OnOutput(
    const std::string& UNUSED(output))
{
    // nothing to do
    return true;
}

bool CompoundView::Impl::OnInputRequested(
    const std::string& UNUSED(ir_type),
    const std::list<std::string>& UNUSED(ir_strings))
{
    // nothing to do
    return true;
}

void CompoundView::Impl::SetStatusText(const wxString& text, bool invokeEvents)
{
    text_status_->SetValue(text);
    if (invokeEvents)
    {
        InvokeOnStatusChangedEventHandlers(toStdString(text));
    }
}

CompoundView::Impl::~Impl()
{
    DeleteAllEventHandlers();
}

//BEGIN Pseudo-Implementation

CompoundView::CompoundView()
{
}

CompoundView::CompoundView(
    wxWindow* parent,
    wxWindowID id,
    const wxPoint& pos,
    const wxSize& size,
    long style,
    const wxString& name)
{
    CompoundView::Create(parent, id, pos, size, style, name);
}

bool CompoundView::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxPoint& pos,
    const wxSize& size,
    long style,
    const wxString& name)
{
    impl_ = std::make_unique<CompoundView::Impl>(this);
    return impl_->Create(parent, id, pos, size, style, name);
}

CompoundView::~CompoundView()
{
}

Simulator * CompoundView::GetSimulator()
{
    assert(impl_);
    return impl_->GetSimulator();
}

LuaEditor * CompoundView::GetLuaEditor(Simulator::EScriptType type)
{
    assert(impl_);
    return impl_->GetLuaEditor(type);
}

SimulationPanel * CompoundView::GetSimulationPanel()
{
    assert(impl_);
    return impl_->GetSimulationPanel();
}

wxTextCtrl * CompoundView::GetOutputControl()
{
    assert(impl_);
    return impl_->GetOutputControl();
}

CompoundViewPrefs CompoundView::GetPrefs() const
{
    assert(impl_);
    return impl_->GetPrefs();
}

void CompoundView::SetPrefs(const CompoundViewPrefs& prefs)
{
    assert(impl_);
    impl_->SetPrefs(prefs);
}

wxSize CompoundView::DoGetBestSize() const
{
    assert(impl_);
    return impl_->DoGetBestSize();
}

Simulator::EScriptType CompoundView::GetCurrentScript() const
{
    assert(impl_);
    return impl_->GetCurrentScript();
}

void CompoundView::ReloadScripts()
{
    assert(impl_);
    impl_->ReloadScripts();
}

bool CompoundView::IsAnyScriptModified() const
{
    return impl_->IsAnyScriptModified();
}

bool CompoundView::IsScriptModified(Simulator::EScriptType script) const
{
    return impl_->IsScriptModified(script);
}

void CompoundView::GetModifiedScripts(Simulator::EScriptType* scripts) const
{
    impl_->GetModifiedScripts(scripts);
}

void CompoundView::AddEventHandler(CompoundView::EventHandler* eventHandler)
{
    assert(impl_);
    impl_->AddEventHandler(eventHandler);
}

CompoundView::EventHandler * CompoundView::DetachEventHandler(
    CompoundView::EventHandler* eventHandler)
{
    assert(impl_);
    return impl_->DetachEventHandler(eventHandler);
}

void CompoundView::DetachAllEventHandlers(
    CompoundView::EventHandler *** eventHandlersArray)
{
    assert(impl_);
    impl_->DetachAllEventHandlers(eventHandlersArray);
}

void CompoundView::DeleteAllEventHandlers()
{
    assert(impl_);
    impl_->DeleteAllEventHandlers();
}

CompoundView::EventHandler *const * CompoundView::GetEventHandlers() const
{
    assert(impl_);
    return impl_->GetEventHandlers();
}

CompoundView::ActionsHelper * CompoundView::Actions()
{
    assert(impl_);
    return impl_->Actions();
}

//BEGIN CompoundView::EventHandler

bool CompoundView::EventHandler::OnStopped()
{
    return true;
}

bool CompoundView::EventHandler::OnStarted()
{
    return true;
}

bool CompoundView::EventHandler::OnPaused()
{
    return true;
}

bool CompoundView::EventHandler::OnResumed()
{
    return true;
}

bool CompoundView::EventHandler::OnCurrentScriptChanged(
    Simulator::EScriptType UNUSED(old_script),
    Simulator::EScriptType UNUSED(new_script))
{
    return true;
}

bool CompoundView::EventHandler::OnScriptModified(
    Simulator::EScriptType UNUSED(script))
{
    return true;
}

bool CompoundView::EventHandler::OnScriptSavePointLeft(
    Simulator::EScriptType UNUSED(script))
{
    return true;
}

bool CompoundView::EventHandler::OnScriptSavePointReached(
    Simulator::EScriptType UNUSED(script))
{
    return true;
}

bool CompoundView::EventHandler::OnScriptRestored(
    Simulator::EScriptType UNUSED(script))
{
    return true;
}

bool CompoundView::EventHandler::OnScriptSaved(
    Simulator::EScriptType UNUSED(script))
{
    return true;
}

bool CompoundView::EventHandler::OnStatusChanged(
    const std::string& UNUSED(status))
{
    return true;
}

bool CompoundView::EventHandler::OnOutput(
    const std::string& UNUSED(output))
{
    return true;
}

bool CompoundView::EventHandler::OnInputRequested(
    const std::string& UNUSED(ir_type),
    const std::list<std::string>& UNUSED(ir_strings))
{
    return true;
}

CompoundView::EventHandler::~EventHandler()
{
}

//END

//BEGIN Other stuff

CompoundView::ActionsHelper::~ActionsHelper()
{
}

//END

//END

}
