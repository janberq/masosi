#ifndef _APP_HPP_INCLUDED
#define _APP_HPP_INCLUDED

#include <wx/app.h>

namespace masosi
{

class App : public wxApp
{
public:
    bool OnInit() override;
};

}

wxDECLARE_APP(masosi::App);

#endif // _APP_HPP_INCLUDED
