#ifndef _SIMULATOR_HPP_INCLUDED
#define _SIMULATOR_HPP_INCLUDED

#include <cstddef>
#include <string>
#include <memory>
#include <functional>

struct lua_State;

namespace masosi {

class Simulation;

class Simulator
{

public:

    Simulator();

    enum EScriptType
    {
        SCRIPT_NONE = -1,
        SCRIPT_DRAWING = 0,
        SCRIPT_PRELUDE,
        SCRIPT_SIMULATION,
        SCRIPT_POSTLUDE,
        SCRIPT_COUNT
    };

    void SetScript(EScriptType type, const std::string &script);
    std::string GetScript(EScriptType type);

    void Start();

    bool IsRunning();
    bool IsPaused();
    bool IsWaiting();

    void Pause();
    void PauseWait();
    void PauseWait(std::uint32_t timeout_ms);

    void Resume();
    void ResumeWait();
    void ResumeWait(std::uint32_t timeout_ms);

    void WakeUp();
    void WakeUpWait();
    void WakeUpWait(std::uint32_t timeout_ms);

    void Stop();
    void StopWait();
    void StopWait(std::uint32_t timeout_ms);

    void WaitForWaiting();
    void WaitForWaiting(std::uint32_t timeout_ms);

    void ExecuteDrawingScript();

    enum ELuaState
    {
        STATE_DRAWING = 0,
        STATE_SECOND_THREAD,
        STATE_COUNT
    };

    lua_State *ResetLua(ELuaState state);
    lua_State *GetLua(ELuaState state);

    void ResetLuaAll();

    Simulation *GetSimulation();

    void LockSimulation();
    bool LockSimulation(std::size_t timeout); // solution to most problems?
    void UnlockSimulation();

    bool DisallowRunning();
    void AllowRunning();

    void SetExecutionErrorHandler(
        std::function<void (const std::exception &ex)> f);
    void SetExitHandler(std::function<void ()> f);

    virtual ~Simulator();

protected:

    struct Impl;
    std::unique_ptr<Impl> impl_;

};

}

#endif // _SIMULATOR_HPP_INCLUDED
