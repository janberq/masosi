#ifndef _SIMULATIONPANELTF_HPP_INCLUDED
#define _SIMULATIONPANELTF_HPP_INCLUDED

#include <wx/frame.h>
#include <memory>

namespace masosi {

class SimulationPanelTF : public wxFrame
{
public:

    SimulationPanelTF();

    SimulationPanelTF(wxWindow *parent, wxWindowID id = wxID_ANY);

    bool Create(wxWindow *parent, wxWindowID id = wxID_ANY);

    ~SimulationPanelTF();

protected:

    struct Impl;
    std::unique_ptr<Impl> impl_;
};

}

#endif // _SIMULATIONPANELTF_HPP_INCLUDED
