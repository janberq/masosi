// 2016-08-23

#ifndef _COMPOUNDVIEWTF_HPP_INCLUDED_
#define _COMPOUNDVIEWTF_HPP_INCLUDED_

#include <wx/frame.h>
#include <memory>

namespace masosi {

class CompoundViewTF : public wxFrame
{
public:

    CompoundViewTF();

    CompoundViewTF(wxWindow *parent, wxWindowID id = wxID_ANY);

    bool Create(wxWindow *parent, wxWindowID id = wxID_ANY);

    ~CompoundViewTF();

protected:

    struct Impl;
    std::unique_ptr<Impl> impl_;
};

}

#endif // _COMPOUNDVIEWTF_HPP_INCLUDED_
