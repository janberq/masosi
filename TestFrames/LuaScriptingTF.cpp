#include "TestFrames/LuaScriptingTF.hpp"
#include "Simulation.hpp"
#include "LuaEditor.hpp"

#include <string>
#include <sstream>
#include <fstream>

#include <sol.hpp>

#include <wx/menu.h>
#include <wx/stc/stc.h>
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/splitter.h>
#include <wx/panel.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/filedlg.h>
#include <wx/msgdlg.h>

namespace std {
    inline std::string to_string(const wxString &other) {
        /* THE FOLLOWING MAY NOT WORK somehow
        auto ret = other.mb_str(wxConvUTF8);
        return std::string { ret.data(), ret.length() };
        */
        return other.ToUTF8().data();
    }
}

namespace masosi {

/**
 * ID definitions
 */

static const long ID_SAVE_OUTPUT = ::wxNewId();

static const long ID_RESET_LUA = ::wxNewId();
static const long ID_CLEAR_OUTPUT = ::wxNewId();
static const long ID_EXECUTE = ::wxNewId();

struct LuaScriptingTF::Impl
{
public:

    LuaScriptingTF *frm {nullptr};

    LuaEditor *luaEditor {nullptr};
    wxTextCtrl *tcOutput {nullptr};

    wxString fpath {""};
    bool isCodeChanged {false};

    sol::state lua;

    Impl(LuaScriptingTF *_frm) : frm(_frm)
    {
    }

    void ResetLua()
    {
        lua = sol::state();
        lua.open_libraries(sol::lib::base);
        RegisterVector(lua);
        RegisterColor(lua);
        RegisterPolygon(lua);
        RegisterSimulation(lua); // TODO Add the new ones here
        lua["print"] = [this](const std::string &txt) {
            tcOutput->AppendText(txt + "\n");
        };
    }

    int SavePrompt()
    {
        return wxMessageBox(_("Script is changed, do you want to save it?"), _("Prompt"), wxCENTRE | wxYES_NO | wxCANCEL | wxYES_DEFAULT, frm);
    }

    void NewScript()
    {
        if (isCodeChanged && SavePrompt())
        {
            SaveScript();
        }
        fpath = "";
        frm->SetTitle("MaSoSi Test Frame [ :New File: ]");
        luaEditor->SetText("");
        luaEditor->SelectNone();
        isCodeChanged = false;
    }

    void OpenScript()
    {
        wxFileDialog fdlg(frm, _("Open Script"), "", "", _("Lua Files (*.lua)|*.lua|All files (*.*)|*.*"), wxFD_OPEN);
        fdlg.CenterOnParent();
        if (fdlg.ShowModal() == wxID_OK)
        {
            wxString file = fdlg.GetPath();
            if (isCodeChanged)
            {
                switch (SavePrompt())
                {
                    case wxYES:
                        SaveScript();
                        break;
                    case wxNO:
                        // do nothing
                        break;
                    default: // cancel and other
                        return ;
                        break;
                }
            }
            if (luaEditor->LoadFile(file))
            {
                fpath = file;
                frm->SetTitle("MaSoSi Test Frame [ " + fpath + " ]");
                luaEditor->SelectNone(); // as seen in the sample
                isCodeChanged = false;
            }
            else
            {
                // It shows a message box automatically
                // wxMessageBox(_("Unable to open file!"), _("Information"), wxICON_INFORMATION | wxOK | wxCENTER, frm);
            }
        }
    }

    bool SaveScript(const wxString &path)
    {
        std::ofstream ofs{path}; // automatically closed when destroyed
        if (ofs)
        {
            auto buf = luaEditor->GetText().mb_str(wxConvUTF8);
            ofs.write(buf.data(), buf.length());
        }
        return true;
    }

    void SaveScript()
    {
        if (!fpath.empty()) // overwrite
        {
            SaveScript(fpath);
        }
        else
        {
            wxFileDialog fdlg(frm, _("Save Script"), "", "", _("Lua Files (*.lua)|*.lua|All files (*.*)|*.*"), wxFD_SAVE);
            fdlg.CenterOnParent();
            if (fdlg.ShowModal() == wxID_OK)
            {
                auto path = fdlg.GetPath();
                if (SaveScript(path))
                {
                    fpath = path;
                    frm->SetTitle("MaSoSi Test Frame [ " + fpath + " ]");
                    isCodeChanged = false;
                }
            }
        }
    }

    void SaveScriptAs()
    {
        wxFileDialog fdlg(frm, _("Save Script As"), "", "", _("Lua Files (*.lua)|*.lua|All files (*.*)|*.*"), wxFD_SAVE);
        fdlg.CenterOnParent();
        if (fdlg.ShowModal() == wxID_OK)
        {
            if (SaveScript(fdlg.GetPath()))
            {
            }
        }
    }

    void ClearOutput()
    {
        tcOutput->Clear();
    }

    bool SaveOutput(const wxString &path)
    {
        tcOutput->SaveFile(path);
        return true;
    }

    void SaveOutput()
    {
        wxFileDialog fdlg(frm, _("Save Output"), "", "", _("All files (*.*)|*.*"));
        fdlg.CenterOnParent();
        if (fdlg.ShowModal() == wxID_OK)
        {
            if (SaveOutput(fdlg.GetPath()))
            {
                // do nothing
            }
        }
    }

    void Execute()
    {
        try
        {
            lua.script(std::to_string(luaEditor->GetText()));
        }
        catch (sol::error &err)
        {
            std::ostringstream oss;
            oss << ">> ERROR: " << err.what() << "\n";
            tcOutput->AppendText(oss.str());
        }
    }

    bool InitFrame()
    {
        /* initialize menu bar */
        {
            wxMenuBar *menuBar = new wxMenuBar;

            wxMenu *file = new wxMenu;
            file->Append(wxID_NEW, _("New Script\tCTRL+N"));
            file->Append(wxID_OPEN, _("Open Script\tCTRL+O"));
            file->Append(wxID_SAVE, _("Save Script\tCTRL+S"));
            file->Append(wxID_SAVEAS, _("Save Script As\tCTRL+SHIFT+S"));
            file->AppendSeparator();
            file->Append(ID_SAVE_OUTPUT, _("Save Output\tCTRL+V"));
            file->AppendSeparator();
            file->Append(ID_RESET_LUA, _("Reset Lua\tF3"));
            file->Append(ID_CLEAR_OUTPUT, _("Clear Output\tF2"));
            file->AppendSeparator();
            file->Append(wxID_EXIT, _("Quit\tALT+F4"));
            menuBar->Append(file, "&File");

            wxMenu *script = new wxMenu;
            script->Append(ID_EXECUTE, _("Execute\tF5"));
            menuBar->Append(script, "Script");

            menuBar->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                NewScript();
            }, wxID_NEW);

            menuBar->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                OpenScript();
            }, wxID_OPEN);

            menuBar->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                SaveScript();
            }, wxID_SAVE);

            menuBar->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                SaveScriptAs();
            }, wxID_SAVEAS);

            menuBar->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                SaveOutput();
            }, ID_SAVE_OUTPUT);

            menuBar->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                ResetLua();
            }, ID_RESET_LUA);
            menuBar->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                ClearOutput();
            }, ID_CLEAR_OUTPUT);
            menuBar->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                Execute();
            }, ID_EXECUTE);

            frm->SetMenuBar(menuBar);
        }

        /* initialize sizer */
        wxBoxSizer *bsMain = new wxBoxSizer(wxVERTICAL);

        /* initialize splitter */
        wxSplitterWindow *splitter = new wxSplitterWindow(frm,
            wxID_ANY,
            wxDefaultPosition,
            wxDefaultSize,
            0 /* no 3D sash*/);
        splitter->SetSashGravity(0.7);
        splitter->SetMinimumPaneSize(20);

        bsMain->Add(splitter, wxSizerFlags(1).Expand().Border(wxALL, 0));

        /* initialize styled text ctrl */
        luaEditor = new LuaEditor(splitter, wxID_ANY);

        InitializeLuaEditor();

        /* initialize output panel */
        tcOutput = new wxTextCtrl(
            splitter,
            wxID_ANY,
            "",
            wxDefaultPosition, wxDefaultSize,
            wxTE_MULTILINE | wxTE_READONLY | wxVSCROLL | wxHSCROLL);
        auto font = wxFont(
            12,
            wxFontFamily::wxFONTFAMILY_TELETYPE,
            wxFontStyle::wxFONTSTYLE_NORMAL,
            wxFontWeight::wxFONTWEIGHT_NORMAL);
        tcOutput->SetFont(font);

        /* split vertically */

        splitter->SplitHorizontally(luaEditor, tcOutput);

        frm->SetSizer(bsMain);

        return true;
    }

    void InitializeLuaEditor()
    {
       luaEditor->Bind(wxEVT_STC_MODIFIED, [this](wxStyledTextEvent &) {
            isCodeChanged = true;
        });
    }

};

LuaScriptingTF::LuaScriptingTF()
{
}

LuaScriptingTF::LuaScriptingTF(wxWindow* parent, wxWindowID id)
{
    Create(parent, id);
}

bool LuaScriptingTF::Create(wxWindow* parent, wxWindowID id)
{
    if (!wxFrame::Create(parent, id, _("MaSoSi Test Frame [ :New File: ]")))
        return false;

    data_ = std::make_unique<LuaScriptingTF::Impl>(this);

    /* initialize lua */
    data_->ResetLua();

    return data_->InitFrame();
}

LuaScriptingTF::~LuaScriptingTF()
{
}

}
