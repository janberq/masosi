#ifndef _LUASCRIPTINGTF_HPP_INCLUDED
#define _LUASCRIPTINGTF_HPP_INCLUDED

#include <wx/frame.h>
#include <memory>

namespace masosi {

class LuaScriptingTF : public wxFrame
{
public:

    LuaScriptingTF();

    LuaScriptingTF(wxWindow *parent, wxWindowID id);

    bool Create(wxWindow *parent, wxWindowID id);

    virtual ~LuaScriptingTF();

private:

    struct Impl;
    std::unique_ptr<Impl> data_;

};

}

#endif
