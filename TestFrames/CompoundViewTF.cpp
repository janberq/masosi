#include "CompoundViewTF.hpp"
#include "CompoundView.hpp"

#include <cassert>

#include <wx/sizer.h>

namespace masosi {

struct CompoundViewTF::Impl
{
    Impl(CompoundViewTF *frame);
    bool Create(wxWindow *parent, wxWindowID id);

private:

    CompoundViewTF *frame_;
    CompoundView *compview_;

};

CompoundViewTF::Impl::Impl(CompoundViewTF* frame)
{
    assert(frame != nullptr);
    frame_ = frame;
}

bool CompoundViewTF::Impl::Create(wxWindow* parent, wxWindowID id)
{
    if (!(frame_->wxFrame::Create)(parent, id, _("CompoundView Test Frame")))
        return false;

    wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
    compview_ = new CompoundView(frame_);
    sizer->Add(compview_, wxSizerFlags(1).Expand().Border(0));
    frame_->SetSizerAndFit(sizer);

    return true;
}

//BEGIN pseudo-impl

CompoundViewTF::CompoundViewTF()
{
}

CompoundViewTF::CompoundViewTF(wxWindow* parent, wxWindowID id)
{
    CompoundViewTF::Create(parent, id);
}

bool CompoundViewTF::Create(wxWindow* parent, wxWindowID id)
{
    impl_ = std::make_unique<Impl>(this);
    return impl_->Create(parent, id);
}

CompoundViewTF::~CompoundViewTF()
{
}

}

//END
