#include "TestFrames/SimulationPanelTF.hpp"

#include "LuaEditor.hpp"
#include "SimulationPanel.hpp"

#include <sol.hpp>

#include <wx/menu.h>
#include <wx/splitter.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>

#include <iostream>
#include <string>

namespace masosi {

// IDs

static const long ID_FILE_CLEAR_OUTPUT                  = ::wxNewId();
static const long ID_SIMULATION_EXECUTE_SCRIPT          = ::wxNewId();
static const long ID_SIMULATION_CLEAR_EVERYTHING        = ::wxNewId();
static const long ID_SIMULATION_RESET_LUA               = ::wxNewId();
static const long ID_SIMULATION_REFRESH_EVERYTHING      = ::wxNewId();
static const long ID_SIMULATION_RESET_VIEW              = ::wxNewId();

struct SimulationPanelTF::Impl
{
    Impl();
    Impl(SimulationPanelTF *frm);

    bool Create();
    void Destroy();

    ~Impl();

protected:

    SimulationPanelTF *frm_ { nullptr };
    SimulationPanel *panel_ { nullptr };
    Simulator simulator_;
    LuaEditor *lua_editor_ { nullptr };
    wxTextCtrl *output_panel_ { nullptr };
    sol::state lua_;

    void ClearOutput();
    void ExecuteScript();
    void ClearEverything();
    void ResetLua();
    void RefreshEverything();
    void ResetView();
};

SimulationPanelTF::Impl::Impl()
{
}

SimulationPanelTF::Impl::Impl(SimulationPanelTF* frm)
    : frm_ { frm }
{
}

bool SimulationPanelTF::Impl::Create()
{
    assert(frm_ != nullptr);

    // Splitters
    wxSplitterWindow *splitter1 = new wxSplitterWindow(frm_,
        wxID_ANY,
        wxDefaultPosition,
        wxDefaultSize,
        0 /* no 3D sash*/);
    wxSplitterWindow *splitter2 = new wxSplitterWindow(splitter1,
        wxID_ANY,
        wxDefaultPosition,
        wxDefaultSize,
        0 /* no 3D sash*/);

    // SimulationPanel

    panel_ = new SimulationPanel(splitter2, wxID_ANY);

    // Output panel

    output_panel_ = new wxTextCtrl(
        splitter2,
        wxID_ANY,
        "",
        wxDefaultPosition, wxDefaultSize,
        wxTE_MULTILINE | wxTE_READONLY | wxVSCROLL | wxHSCROLL);
    auto output_font = wxFont(
        12,
        wxFontFamily::wxFONTFAMILY_TELETYPE,
        wxFontStyle::wxFONTSTYLE_NORMAL,
        wxFontWeight::wxFONTWEIGHT_NORMAL);
    output_panel_->SetFont(output_font);

    splitter2->SetMinimumPaneSize(25);
    splitter2->SplitHorizontally(panel_, output_panel_);
    splitter2->SetSashGravity(0.8);

    // Lua Editor

    LuaEditorPrefs prefs;
    prefs.Defaults();
    lua_editor_ = new LuaEditor(splitter1, wxID_ANY, prefs);

    splitter1->SetMinimumPaneSize(50);
    splitter1->SplitVertically(splitter2, lua_editor_);
    splitter1->SetSashGravity(0.5);

    frm_->SetSize(640, 480);

    // main sizer
    wxBoxSizer *main_sizer = new wxBoxSizer(wxHORIZONTAL);
    main_sizer->Add(splitter1, wxSizerFlags(1).Expand().Border(wxALL, 0));
    frm_->SetSizer(main_sizer);

    // initialize menubar
    wxMenuBar *menu = new wxMenuBar;

    wxMenu *file = new wxMenu;
    file->Append(ID_FILE_CLEAR_OUTPUT, _("Clear Output\tCTRL+L"));
    file->AppendSeparator();
    file->Append(wxID_EXIT, _("Exit\tALT+F4"));

    wxMenu *simulation = new wxMenu;
    simulation->Append(ID_SIMULATION_EXECUTE_SCRIPT, _("Execute Script\tF11"));
    simulation->Append(ID_SIMULATION_CLEAR_EVERYTHING, _("Clear Everything (except output)\tCTRL+F7"));
    simulation->Append(ID_SIMULATION_RESET_LUA, _("Reset Lua\tCTRL+F6"));
    simulation->Append(ID_SIMULATION_REFRESH_EVERYTHING, _("Refresh Everything\tF5"));
    simulation->Append(ID_SIMULATION_RESET_VIEW, _("Reset View\tF3"));

    menu->Append(file, _("&File"));
    menu->Append(simulation, _("&Simulation"));

    // connect events
    menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
        frm_->Close();
    }, wxID_EXIT);
    menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
        ClearOutput();
    }, ID_FILE_CLEAR_OUTPUT);
    menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
        ExecuteScript();
    }, ID_SIMULATION_EXECUTE_SCRIPT);
    menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
        ClearEverything();
    }, ID_SIMULATION_CLEAR_EVERYTHING);
    menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
        ResetLua();
    }, ID_SIMULATION_RESET_LUA);    
    menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
        RefreshEverything();
    }, ID_SIMULATION_REFRESH_EVERYTHING);
    menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
        ResetView();
    }, ID_SIMULATION_RESET_VIEW);

    // set menu bar
    frm_->SetMenuBar(menu);

    // initialize SimulationPanel
    panel_->SetSimulator(&simulator_);

    // initialize lua
    ResetLua();

    return true;
}

void SimulationPanelTF::Impl::Destroy()
{
    simulator_.GetSimulation()->ClearPolygons();
    panel_->SetSimulator(nullptr);
}

SimulationPanelTF::Impl::~Impl()
{
    Destroy();
}

void SimulationPanelTF::Impl::ClearOutput()
{
    output_panel_->Clear();
}

void SimulationPanelTF::Impl::ExecuteScript()
{
    std::string input = lua_editor_->GetText().ToUTF8().data();
    try
    {
        lua_.script(input);
    }
    catch (sol::error &e)
    {
        *output_panel_ << "Lua Error: " << e.what() << "\n";
    }
    RefreshEverything();
}

void SimulationPanelTF::Impl::ClearEverything()
{
    simulator_.GetSimulation()->ClearPolygons();
    panel_->RefreshAll();
}

void SimulationPanelTF::Impl::ResetLua()
{
    lua_ = sol::state();
    lua_.open_libraries(
        sol::lib::base,
        sol::lib::math,
        sol::lib::bit32,
        sol::lib::coroutine,
        sol::lib::count,
        sol::lib::debug,
        sol::lib::ffi,
        sol::lib::io,
        sol::lib::jit,
        sol::lib::os,
        sol::lib::package,
        sol::lib::string,
        sol::lib::table);
    RegisterVector(lua_);
    RegisterColor(lua_);
    RegisterPolygon(lua_);
    RegisterSimulation(lua_); // TODO Add the new ones here
    lua_["_SIM"] = simulator_.GetSimulation();
    lua_["gui_print"] = [this](const std::string &in) {
        output_panel_->AppendText(in + "\n");
    };
}

void SimulationPanelTF::Impl::RefreshEverything()
{
    panel_->RefreshAll();
}

void SimulationPanelTF::Impl::ResetView()
{
    panel_->ResetPrefs();
    panel_->RefreshAll();
}

/**
 * SimulationPanelTF impl
 */

SimulationPanelTF::SimulationPanelTF()
{
}

SimulationPanelTF::SimulationPanelTF(wxWindow* parent, wxWindowID id)
{
    SimulationPanelTF::Create(parent, id);
}

bool SimulationPanelTF::Create(wxWindow* parent, wxWindowID id)
{
    if (!wxFrame::Create(parent, id, _("Simulation Panel Test Frame")))
        return false;

    impl_ = std::make_unique<Impl>(this);
    if (!impl_->Create())
        return false;

    return true;
}

SimulationPanelTF::~SimulationPanelTF()
{
}

}
