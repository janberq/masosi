/**
 * 
 * LuaBundle: Simple multithreaded data sharing between lua_State s.
 * 
 * Canberk Sönmez
 * 
 * Date: 2016-08-13
 * 
 */

#include "lua_bundle.h"

#include <stdlib.h>
#include <string.h>

#include <unordered_map>
#include <string>
#include <mutex>
#include <vector>
#include <algorithm>

#include <iostream>

struct LBNDL_Bundle
{
    LBNDL_Manager *manager;

    std::string name;

    struct Datum
    {
        char type;
        std::string tname;
        std::mutex mtx;
        union
        {
            lua_Integer integer;
            lua_Number number;
            int boolean;
            void *ludata;
        } value;
        std::vector<char> data;
    };

    std::unordered_map<std::string, Datum> data;
    std::mutex mtx;

};

struct LBNDL_Manager
{

    std::unordered_map<std::string, LBNDL_Bundle *> bundles;

    std::vector<lua_State *> lua_states;

    ~LBNDL_Manager()
    {
        for (auto &p : bundles)
        {
            delete p.second;
        }
        bundles.clear();
    }

};

// IMPLEMENT C FUNCTIONS HERE

extern "C"
{

#include <lauxlib.h>

LBNDL_Manager *LBNDL_CreateBundleManager(
    )
{
    return new LBNDL_Manager;
}

LBNDL_Result LBNDL_RegisterBundleManager(
    LBNDL_Manager *manager,
    lua_State *state,
    const char *name)
{
    if (manager == nullptr)
        return LBNDL_ERR_NULLMANAGER;
    if (state == nullptr)
        return LBNDL_ERR_NULLSTATE;
    if (name == nullptr)
        return LBNDL_ERR_NULLSTRING;

    /**
     * register Bundle metatable
     */

    luaL_newmetatable(state, "LBNDL.bundle_mt");

    lua_pushstring(state, "__index");
    lua_pushcfunction(state, [](lua_State *L) -> int {
        auto bundle = * (LBNDL_Bundle **) luaL_checkudata(L, 1, "LBNDL.bundle_mt");
        auto key = (std::string) luaL_checkstring(L, 2);

        if (key == "lock")
        {
            lua_pushcfunction(L, [](lua_State *L) -> int {
                auto bundle = * (LBNDL_Bundle **) luaL_checkudata(L, 1, "LBNDL.bundle_mt");
                if (lua_gettop(L) == 1) // lock bundle
                {
                    bundle->mtx.lock();
                }
                else if (lua_gettop(L) == 2) // lock object
                {
                    auto key = (std::string) luaL_checkstring(L, 2);
                    auto it = bundle->data.find(key);
                    if (it != bundle->data.end())
                    {
                        it->second.mtx.lock();
                    }
                    else
                    {
                        luaL_error(L, "LBNDL: Not found such a bundle!");
                    }
                }
                else
                {
                    luaL_error(L, "LBNDL: not correct number of args!");
                }
                return 0;
            });
            return 1;
        }
        else if (key == "try_lock")
        {
            lua_pushcfunction(L, [](lua_State *L) -> int {
                auto bundle = * (LBNDL_Bundle **) luaL_checkudata(L, 1, "LBNDL.bundle_mt");
                if (lua_gettop(L) == 1) // lock bundle
                {
                    bundle->mtx.lock();
                }
                else if (lua_gettop(L) == 2) // lock object
                {
                    auto key = (std::string) luaL_checkstring(L, 2);
                    auto it = bundle->data.find(key);
                    if (it != bundle->data.end())
                    {
                        lua_pushboolean(L, it->second.mtx.try_lock());
                        return 1;
                    }
                    else
                    {
                        luaL_error(L, "LBNDL: Not found such a bundle!");
                    }
                }
                else
                {
                    luaL_error(L, "LBNDL: not correct number of args!");
                }
                return 0;
            });
            return 1;
        }
        else if (key == "unlock")
        {
            lua_pushcfunction(L, [](lua_State *L) -> int {
                auto bundle = * (LBNDL_Bundle **) luaL_checkudata(L, 1, "LBNDL.bundle_mt");
                if (lua_gettop(L) == 1) // lock bundle
                {
                    bundle->mtx.unlock();
                }
                else if (lua_gettop(L) == 2) // lock object
                {
                    auto key = (std::string) luaL_checkstring(L, 2);
                    auto it = bundle->data.find(key);
                    if (it != bundle->data.end())
                    {
                        it->second.mtx.unlock();
                    }
                    else
                    {
                        luaL_error(L, "LBNDL: Not found such a bundle!");
                    }
                }
                else
                {
                    luaL_error(L, "LBNDL: not correct number of args!");
                }
                return 0;
            });
            return 1;
        }
        else
        {
            auto it = bundle->data.find(key);
            if (it != bundle->data.end())
            {
                switch (it->second.type)
                {
                case LBNDL_TYPE_INTEGER:
                    lua_pushinteger(L, it->second.value.integer);
                    break;
                case LBNDL_TYPE_NUMBER:
                    lua_pushnumber(L, it->second.value.number);
                    break;
                case LBNDL_TYPE_STRING:
                    lua_pushlstring(
                        L,
                        &it->second.data.front(),
                        it->second.data.size());
                    break;
                case LBNDL_TYPE_BOOLEAN:
                    lua_pushboolean(L, it->second.value.boolean);
                    break;
                case LBNDL_TYPE_LUDATA:
                    lua_pushlightuserdata(L, it->second.value.ludata);
                    luaL_setmetatable(L, it->second.tname.c_str());
                    break;
                case LBNDL_TYPE_HUDATA:
                {
                    size_t len = it->second.data.size();
                    void* p = lua_newuserdata(L, len);
                    memcpy(p, &it->second.data.front(), len);
                    luaL_setmetatable(L, it->second.tname.c_str());
                }
                    break;
                default:
                    luaL_error(L, "LBNDL: Invalid type.");
                }
                return 1;
            }
            else
            {
                lua_pushnil(L); // not found
                return 1;
            }
        }
        return 0;
    });
    lua_settable(state, -3);

    lua_pushstring(state, "__newindex");
    lua_pushcfunction(state, [](lua_State *L) -> int {
        auto bundle = * (LBNDL_Bundle **) luaL_checkudata(L, 1, "LBNDL.bundle_mt");
        auto key = (std::string) luaL_checkstring(L, 2);

        LBNDL_Bundle::Datum &datum = bundle->data[key];

        if (lua_isinteger(L, 3))
        {
            datum.type = LBNDL_TYPE_INTEGER;
            datum.value.integer = lua_tointeger(L, -1);
        }
        else if (lua_isnumber(L, 3))
        {
            datum.type = LBNDL_TYPE_NUMBER;
            datum.value.number = lua_tonumber(L, -1);
        }
        else if (lua_isstring(L, 3))
        {
            datum.type = LBNDL_TYPE_STRING;
            size_t len;
            const char *data = lua_tolstring(L, -1, &len);
            datum.data.resize(len);
            memcpy(&datum.data.front(), data, len);
        }
        else if (lua_isboolean(L, 3))
        {
            datum.type = LBNDL_TYPE_BOOLEAN;
            datum.value.boolean = lua_toboolean(L, -1);
        }
        else if (lua_isuserdata(L, 3))
        {
            // get the name of the table
            lua_getmetatable(L, -1);
            lua_pushstring(L, "__name");
            lua_rawget(L, -2);

            datum.tname = lua_tostring(L, -1);

            lua_pop(L, 2);

            if (lua_islightuserdata(L, 3))
            {
                datum.type = LBNDL_TYPE_LUDATA;
                datum.value.ludata = lua_touserdata(L, -1);
            }
            else // heavy user data
            {
                size_t len = lua_rawlen(L, -1);
                datum.type = LBNDL_TYPE_HUDATA;
                datum.data.resize(len);
                memcpy(
                    &datum.data.front(),
                    lua_touserdata(L, -1),
                    len);
            }
        }
        else if (lua_isnil(L, 3))
        {
            bundle->data.erase(bundle->data.find(key));
        }
        else
        {
            luaL_error(L, "LBDL: Cannot store this!");
        }

        return 0;
    });
    lua_settable(state, -3);

    lua_pop(state, 1);

    /**
     * register Manager metatable
     */

    luaL_newmetatable(state, "LBNDL.manager_mt");

    lua_pushstring(state, "__index");
    lua_pushcfunction(state, [](lua_State *L) -> int {
        auto manager = * (LBNDL_Manager **) luaL_checkudata(L, 1, "LBNDL.manager_mt");
        auto key = (std::string) luaL_checkstring(L, 2);

        if (key == "new_bundle")
        {
            lua_pushcfunction(L, [](lua_State *L) -> int {
                // new_bundle(manager, name)
                auto manager = * (LBNDL_Manager **) luaL_checkudata(L, 1, "LBNDL.manager_mt");
                auto name = (std::string) luaL_checkstring(L, 2);

                LBNDL_Bundle *bundle;
                auto r = LBNDL_CreateBundle(manager, name.c_str(), &bundle);

                if (r == LBNDL_ERR_NOERROR)
                {
                    auto bundlepp = (LBNDL_Bundle **) lua_newuserdata(L, sizeof(LBNDL_Bundle*));
                    *bundlepp = bundle;
                    luaL_setmetatable(L, "LBNDL.bundle_mt");
                    return 1;
                }
                else
                {
                    luaL_error(L, "LBNDL_CreateBundle error: %s", LBNDL_GetErrorMsg(r));
                }

                return 0;
            });
            return 1;
        }
        else
        {
            auto it = manager->bundles.find(key);
            if (it != manager->bundles.end())
            {
                auto bundlepp = (LBNDL_Bundle **) lua_newuserdata(L, sizeof(LBNDL_Bundle*));
                *bundlepp = it->second;
                luaL_setmetatable(L, "LBNDL.bundle_mt");
                return 1;
            }
            else
            {
                lua_pushnil(L);
                return 1;
            }
        }

        return 0;
    });
    lua_settable(state, -3);

    lua_pushstring(state, "__newindex");
    lua_pushcfunction(state, [](lua_State *L) -> int {
        auto manager = * (LBNDL_Manager **) luaL_checkudata(L, 1, "LBNDL.manager_mt");
        auto key = (std::string) luaL_checkstring(L, 2);

        if (lua_isnil(L, 3))
        {
            // delete the bundle
            auto it = manager->bundles.find(key);
            if (it != manager->bundles.end())
            {
                delete it->second;
                manager->bundles.erase(it);
            }
            else
            {
                luaL_error(L, "Not found such a bundle!");
            }
        }
        else
        {
            luaL_error(L, "__newindex can work only with `nil` (to delete a bundle).");
        }
        return 0;
    });
    lua_settable(state, -3);

    lua_pop(state, 1);

    manager->lua_states.push_back(state);

    auto managerpp = (LBNDL_Manager **) lua_newuserdata(state, sizeof(LBNDL_Manager*));
    luaL_setmetatable(state, "LBNDL.manager_mt");
    lua_setglobal(state, name);
    *managerpp = manager;

    return LBNDL_ERR_NOERROR;
}

LBNDL_Result LBNDL_UnassociateState(
    LBNDL_Manager *manager,
    lua_State *state
)
{
    auto &states = manager->lua_states;
    auto it = std::find(states.begin(), states.end(), state);
    if (it == manager->lua_states.end())
        return LBNDL_ERR_NOTFOUND;
    manager->lua_states.erase(it);
    return LBNDL_ERR_NOERROR;
}

LBNDL_Result LBNDL_DestroyBundleManager(
    LBNDL_Manager *manager)
{
    delete manager;
    return LBNDL_ERR_NOERROR;
}


LBNDL_Result LBNDL_GetBundle(
    LBNDL_Manager *manager,
    const char *bundle_name,
    LBNDL_Bundle **result)
{
    if (manager == nullptr)
        return LBNDL_ERR_NULLMANAGER;
    if (bundle_name == nullptr)
        return LBNDL_ERR_NULLSTRING;
    if (result == nullptr)
        return LBNDL_ERR_NULLPTR;

    auto it = manager->bundles.find(bundle_name);
    if (it != manager->bundles.end())
    {
        *result = it->second;
        return LBNDL_ERR_NOERROR;
    }
    return LBNDL_ERR_NOTFOUND;
}

LBNDL_Result LBNDL_CreateBundle(
    LBNDL_Manager *manager,
    const char *bundle_name,
    LBNDL_Bundle **result)
{
    if (manager == nullptr)
        return LBNDL_ERR_NULLMANAGER;
    if (bundle_name == nullptr)
        return LBNDL_ERR_NULLSTRING;

    auto it = manager->bundles.find(bundle_name);
    if (it == manager->bundles.end())
    {
        auto res = new LBNDL_Bundle;
        manager->bundles[bundle_name] = res;
        res->name = bundle_name;
        res->manager = manager;
        if (result != nullptr)
            *result = res;
        return LBNDL_ERR_NOERROR;
    }

    return LBNDL_ERR_EXISTS;
}

LBNDL_Result LBNDL_DestroyBundle(
    LBNDL_Bundle *bundle)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;

    auto it = bundle->manager->bundles.find(bundle->name);
    bundle->manager->bundles.erase(it);

    delete bundle;

    return LBNDL_ERR_NOERROR;
}


LBNDL_Result LBNDL_GetType(
    LBNDL_Bundle *bundle,
    const char *key,
    int *type,
    char **tname)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;
    if (key == nullptr)
        return LBNDL_ERR_NULLSTRING;
    if (type == nullptr)
        return LBNDL_ERR_NULLPTR;

    auto it = bundle->data.find(key);
    if (it != bundle->data.end())
    {
        *type = it->second.type;
        if (((*type == LBNDL_TYPE_HUDATA) ||
            (*type == LBNDL_TYPE_LUDATA)) &&
            tname != nullptr)
        {
            size_t len = it->second.tname.length() + 1;
            *tname = (char *) malloc(len);
            memcpy(*tname, it->second.tname.c_str(), len - 1);
            tname[len-1] = 0;
        }
        return LBNDL_ERR_NOERROR;
    }

    return LBNDL_ERR_NOTFOUND;
}

#define LBNDL_GETSTH_IMPL(a, b) \
    if (bundle == nullptr) \
        return LBNDL_ERR_NULLBUNDLE; \
    if (key == nullptr) \
        return LBNDL_ERR_NULLSTRING; \
    if (result == nullptr) \
        return LBNDL_ERR_NULLPTR; \
 \
    auto it = bundle->data.find(key); \
    if (it != bundle->data.end()) \
    { \
        if (it->second.type == a) \
        { \
            *result = it->second.value.b; \
            return LBNDL_ERR_NOERROR; \
        } \
        return LBNDL_ERR_INCORRECTTYPE; \
    } \
    return LBNDL_ERR_NOTFOUND;

#define LBNDL_SETSTH_IMPL(a, b) \
    if (bundle == nullptr) \
        return LBNDL_ERR_NULLBUNDLE; \
    if (key == nullptr) \
        return LBNDL_ERR_NULLSTRING; \
 \
    auto &ref = bundle->data[key]; \
    ref.value.b = value; \
    ref.type = a;

LBNDL_Result LBNDL_GetInteger(
    LBNDL_Bundle *bundle,
    const char *key,
    lua_Integer *result)
{
    LBNDL_GETSTH_IMPL(LBNDL_TYPE_INTEGER, integer)
}

LBNDL_Result LBNDL_SetInteger(
    LBNDL_Bundle *bundle,
    const char *key,
    lua_Integer value)
{
    LBNDL_SETSTH_IMPL(LBNDL_TYPE_INTEGER, integer)
    return LBNDL_ERR_NOERROR;
}


LBNDL_Result LBNDL_GetBoolean(
    LBNDL_Bundle *bundle,
    const char *key,
    int *result)
{
    LBNDL_GETSTH_IMPL(LBNDL_TYPE_BOOLEAN, boolean)
}

LBNDL_Result LBNDL_SetBoolean(
    LBNDL_Bundle *bundle,
    const char *key,
    int value)
{
    LBNDL_SETSTH_IMPL(LBNDL_TYPE_BOOLEAN, boolean)
    return LBNDL_ERR_NOERROR;
}


LBNDL_Result LBNDL_GetNumber(
    LBNDL_Bundle *bundle,
    const char *key,
    lua_Number *result)
{
    LBNDL_GETSTH_IMPL(LBNDL_TYPE_NUMBER, number)
}

LBNDL_Result LBNDL_SetNumber(
    LBNDL_Bundle *bundle,
    const char *key,
    lua_Number value)
{
    LBNDL_SETSTH_IMPL(LBNDL_TYPE_NUMBER, number)
    return LBNDL_ERR_NOERROR;
}


LBNDL_Result LBNDL_GetString(
    LBNDL_Bundle *bundle,
    const char *key,
    char **result,
    size_t *len)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;
    if (key == nullptr)
        return LBNDL_ERR_NULLSTRING;

    auto it = bundle->data.find(key);
    if (it != bundle->data.end())
    {
        if (it->second.type == LBNDL_TYPE_STRING)
        {
            size_t length = it->second.data.size();
            if (result != nullptr)
            {
                *result = (char *) malloc(length);
                memcpy(*result, &it->second.data[0], length);
            }
            if (len != nullptr)
            {
                *len = length;
            }
            return LBNDL_ERR_NOERROR;
        }
        return LBNDL_ERR_INCORRECTTYPE;
    }
    return LBNDL_ERR_NOTFOUND;
}

LBNDL_Result LBNDL_SetString(
    LBNDL_Bundle *bundle,
    const char *key,
    const char *data,
    size_t len)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;
    if (key == nullptr)
        return LBNDL_ERR_NULLSTRING;
    if (data == nullptr)
        return LBNDL_ERR_NULLPTR;
    auto &ref = bundle->data[key];
    ref.data.resize(len);
    ref.type = LBNDL_TYPE_STRING;
    memcpy(&ref.data[0], data, len);
    return LBNDL_ERR_NOERROR;
}


LBNDL_Result LBNDL_GetUserdata(
    LBNDL_Bundle *bundle,
    const char *key,
    void **data,
    size_t *sz,
    char **tname,
    int *type)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;
    if (key == nullptr)
        return LBNDL_ERR_NULLSTRING;

    auto it = bundle->data.find(key);

    if (it != bundle->data.end())
    {
        if (it->second.type == LBNDL_TYPE_LUDATA ||
            it->second.type == LBNDL_TYPE_HUDATA)
        {
            size_t size = it->second.data.size();
            if (data != nullptr)
            {
                *data = malloc(size);
                memcpy(*data, &it->second.data[0], size);
            }
            if (sz != nullptr)
            {
                *sz = size;
            }
            if (tname != nullptr)
            {
                size_t len = it->second.tname.length() + 1;
                *tname = (char *) malloc(len);
                memcpy(*tname, it->second.tname.c_str(), len - 1);
                tname[len-1] = 0;
            }
            if (type != nullptr)
            {
                *type = it->second.type;
            }
            return LBNDL_ERR_NOERROR;
        }
        return LBNDL_ERR_INCORRECTTYPE;
    }
    return LBNDL_ERR_NOTFOUND;
}

LBNDL_Result LBNDL_SetUserData(
    LBNDL_Bundle *bundle,
    const char *key,
    const void *data,
    size_t sz,
    char *tname,
    int type)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;
    if (key == nullptr)
        return LBNDL_ERR_NULLSTRING;
    if (data == nullptr)
        return LBNDL_ERR_NULLPTR;
    auto &ref = bundle->data[key];
    ref.data.resize(sz);
    ref.type = type;
    ref.tname = tname;
    memcpy(&ref.data[0], data, sz);
    return LBNDL_ERR_NOERROR;
}

LBNDL_Result LBNDL_DeleteKey(
    LBNDL_Bundle *bundle,
    const char *key)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;
    if (key == nullptr)
        return LBNDL_ERR_NULLSTRING;
    auto it = bundle->data.find(key);
    if (it != bundle->data.end())
    {
        bundle->data.erase(it);
        return LBNDL_ERR_NOERROR;
    }
    return LBNDL_ERR_NOTFOUND;
}


LBNDL_Result LBNDL_LockBundle(
    LBNDL_Bundle *bundle)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;
    bundle->mtx.lock();
    return LBNDL_ERR_NOERROR;
}

LBNDL_Result LBNDL_UnlockBundle(
    LBNDL_Bundle *bundle)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;
    bundle->mtx.unlock();
    return LBNDL_ERR_NOERROR;
}


LBNDL_Result LBNDL_LockKey(
    LBNDL_Bundle *bundle,
    const char *key)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;
    if (key == nullptr)
        return LBNDL_ERR_NULLSTRING;
    auto it = bundle->data.find(key);
    if (it != bundle->data.end())
    {
        it->second.mtx.lock();
        return LBNDL_ERR_NOERROR;
    }
    return LBNDL_ERR_NOTFOUND;
}

LBNDL_Result LBNDL_UnlockKey(
    LBNDL_Bundle *bundle,
    const char *key)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;
    if (key == nullptr)
        return LBNDL_ERR_NULLSTRING;
    auto it = bundle->data.find(key);
    if (it != bundle->data.end())
    {
        it->second.mtx.unlock();
        return LBNDL_ERR_NOERROR;
    }
    return LBNDL_ERR_NOTFOUND;
}

LBNDL_Result LBNDL_UnlockAllKeys(
    LBNDL_Bundle *bundle)
{
    if (bundle == nullptr)
        return LBNDL_ERR_NULLBUNDLE;
    for (auto &x: bundle->data)
    {
        x.second.mtx.unlock();
    }
    return LBNDL_ERR_NOERROR;
}

const char *LBNDL_GetErrorMsg(
    LBNDL_Result r)
{
    switch (r)
    {
        case LBNDL_ERR_NOERROR:
            return "no error";
        case LBNDL_ERR_NULLMANAGER:
            return "null manager";
        case LBNDL_ERR_NULLSTATE:
            return "null state";
        case LBNDL_ERR_NULLBUNDLE:
            return "null bundle";
        case LBNDL_ERR_NULLSTRING:
            return "null string";
        case LBNDL_ERR_NULLPTR:
            return "null pointer";
        case LBNDL_ERR_NOTFOUND:
            return "not found";
        case LBNDL_ERR_EXISTS:
            return "exists";
        case LBNDL_ERR_INCORRECTTYPE:
            return "incorrect type";
    }
    return "";
}

}
