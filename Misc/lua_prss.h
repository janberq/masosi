/**
 * lua_prss.hpp
 * 
 * A simple library providing pause/resume/stop/sleep functionality to lua, via
 * making use of C++11 threading facilities.
 * 
 * Define PRSS_DEBUG to enable super-verbose logging mechanism.
 * 
 * Canberk Sönmez
 * 
 * Date: 2016-07-28
 * 
 */

#ifndef _LUA_PRSS_HPP_INCLUDED
#define _LUA_PRSS_HPP_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

#include <lua.h>

#ifdef __cplusplus
}
#endif

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * HPCALL
 * 
 * Hooks pcall and xpcall, so that the exit mechanism which is based on
 * lua_error works properly, without being broken by pcall/xpcall.
 * 
 * Functions of HPCALL are not thread-safe.
 * 
 */

/**
 * Registers HPCALL to the given lua instance.
 */
void HPCALL_Register(lua_State *L);

/**
 * Resets the exit flag of the given lua instance, needed for consecutive
 * script executions, call just before any new script execution.
 */
void HPCALL_ResetExitFlag(lua_State *L);

/**
 * Exits the script.
 */
void HPCALL_Exit(lua_State *L);

/**
 * PRSS
 * 
 * Provides pause/resume/start/stop functionality. Once a lua_State is
 * registered, PRSS_Free() must be called.
 * 
 * Functions of PRSS are thread-safe.
 * 
 */

/**
 * Registers PRSS to the given lua_State. Since, HPCALL_* is needed, it also
 * registers HPCALL, by calling HPCALL_Register.
 */
void PRSS_Register(lua_State *L);

/**
 * Cleans-up PRSS, should be called lastly.
 */
void PRSS_Free();

/**
 * Cleans-up PRSS for a specific lua_State. It does not undo any registrations
 * made on the lua_State.
 */
void PRSS_FreeState(lua_State *L);

/**
 * Pauses the given lua_State. It may block until the current request is
 * processed. (i.e. current request is REQUEST_NONE)
 * 
 */
void PRSS_Pause(lua_State *L);

/**
 * Resumes the given lua_State.
 */
void PRSS_Resume(lua_State *L);

/**
 * Waits until the given lua_State is really paused.
 */
void PRSS_WaitForPaused(lua_State *L);

/**
 * Waits until the given lua_State is really paused, or timeout occurs.
 * It returns,
 * 
 *   1    if successfully paused, with no timeout.
 *   0    timeout occurs
 *   2    no timeout occurs, but also not paused
 * 
 * This function requires the timeout duration in milliseconds.
 * 
 */
int PRSS_WaitForPausedTimeout(lua_State *L, uint32_t milli);

/**
 * Returns 1 if paused **really**, 0 otherwise.
 * 
 * To wait until the lua_State is **really** paused, use PRSS_WaitForPaused*
 * functions, rather than using "NOT" of this as a condition of a while loop.
 * 
 */
int PRSS_IsPaused(lua_State *L);

/**
 * Stops the given lua_State waiting.
 */
void PRSS_StopWaiting(lua_State *L);

/**
 * Waits for the given lua_State starts waiting.
 */
void PRSS_WaitForWaiting(lua_State *L);

/**
 * Waits until the given lua_State has really started waiting, or timeout 
 * occurs.
 * It returns,
 * 
 *   1    if successfully started waiting, with no timeout.
 *   0    timeout occurs
 *   2    no timeout occurs, but also not waiting
 * 
 * This function requires the timeout duration in milliseconds.
 * 
 */
int PRSS_WaitForWaitingTimeout(lua_State *L, uint32_t milli);

/**
 * Returns 1 if started waiting **really**, 0 otherwise.
 * 
 * To wait until the lua_State has **really** started waiting, use 
 * PRSS_WaitForWaiting* functions, rather than using "NOT" of this as a 
 * condition of a while loop.
 * 
 */
int PRSS_IsWaiting(lua_State *L);

/**
 * Waits for the given lua_State stops.
 */
void PRSS_WaitForStopped(lua_State *L);

/**
 * Waits until the given lua_State stops, or timeout occurs.
 * It returns,
 *
 *   1    if successfully stops, with no timeout.
 *   0    timeout occurs
 *   2    no timeout occurs, but also not stopped
 *
 * This function requires the timeout duration in milliseconds.
 *
 */
int PRSS_WaitForStoppedTimeout(lua_State *L, uint32_t milli);

/**
 * Waits for the given lua_State becomes in normal state.
 */
void PRSS_WaitForNormal(lua_State *L);

/**
 * Waits until the given lua_State becomes in normal state, or timeout occurs.
 * It returns,
 *
 *   1    if successfully becomes normal, with no timeout.
 *   0    timeout occurs
 *   2    no timeout occurs, but also not became normal
 *
 * This function requires the timeout duration in milliseconds.
 *
 */
int PRSS_WaitForNormalTimeout(lua_State *L, uint32_t milli);

/**
 * Stops the given lua_State. This is totally thread-safe, whereas calling
 * HPCALL_Exit is not. It uses HPCALL_Exit for implementing the stop
 * functionality.
 * 
 */
void PRSS_Stop(lua_State *L);

/**
 * Sets the output function of PRSS_StatusMessage().
 * 
 * target_func is guaranteed to be protected if only PRSS_StatusMessage is used 
 * to access it.
 */
void PRSS_SetTarget(void (*target_func)(const char *, void *), void *data);

/**
 * Starts the internal timer. (debugging)
 */
void PRSS_StartTimer();

/**
 * Prints a status message with time stamp. (debugging)
 */
void PRSS_StatusMessage(const char *msg);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

#include <sstream>
#include <utility>

template <typename ...Args> // templates must have c++ linkage
inline void PRSS_Status(Args&& ...args)
{
    std::stringstream ss;
    using helper = int [];
    (void) helper{ 0, ( (void)(ss << std::forward<Args>(args)), 0 ) ... };
    PRSS_StatusMessage(ss.str().c_str());
}

#endif

#endif // _LUA_PRSS_HPP_INCLUDED
