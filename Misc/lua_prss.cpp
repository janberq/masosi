/**
 * PRSS implementation.
 *
 * Canberk Sönmez
 *
 * Date: 2016-07-28
 *
 */

//BEGIN Includes

#include "lua_prss.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include <lauxlib.h>

#ifdef __cplusplus
}
#endif

#include <boost/current_function.hpp>

#include <mutex>
#include <condition_variable>
#include <unordered_map>
#include <string>
#include <iostream>
#include <sstream>
#include <chrono>
#include <thread>

#include <cassert>

//END

extern "C"
{

/**
 * Debugging functionality
 */

void PRSS_StartTimer();
void PRSS_StatusMessage(const char *msg);

}

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * For HPCALL functionality.
 */

//BEGIN HPCALL Functionality

static const char g_hpcall_exit_key = 'K';
static const char *g_hpcall_exit_msg = "Exit Requested";

void HPCALL_Register(lua_State *L)
{
    lua_pushcfunction(L, [](lua_State *L) -> int {
        // pcall(func, args)

        int r = lua_pcall(L, lua_gettop(L) - 1, LUA_MULTRET, 0);

        if (r == LUA_OK)
        {
            lua_pushboolean(L, true);
            lua_insert(L, 1);
            return lua_gettop(L);
        }
        else if (r == LUA_ERRRUN || r == LUA_ERRERR)
        {
            lua_pushlightuserdata(L, (void *) &g_hpcall_exit_key);
            lua_gettable(L, LUA_REGISTRYINDEX);
            bool exit_flag = lua_toboolean(L, -1);
            lua_pop(L, 1); // pop exit flag

            if (exit_flag)
            {
                lua_error(L);
                return 0;
            }
            else
            {
                lua_pushboolean(L, false);
                lua_insert(L, 1);
                return lua_gettop(L);
            }
        }
        else
        {
            std::stringstream ss;
            ss << "pcall: error number is " << r << std::endl;
            // clear stack, whatever
            lua_settop(L, 0);
            // error
            luaL_error(L, ss.str().c_str());
            return 0;
        }
        return 0;
    });
    lua_setglobal(L, "pcall");

    lua_pushcfunction(L, [](lua_State *L) -> int {
        // xpcall(func, msgh, args)

        /* swap func and msgh first, this is needed! */
        lua_pushvalue(L, 1);
        lua_pushvalue(L, 2);
        lua_replace(L, 1);
        lua_replace(L, 2);

        int r = lua_pcall(L, lua_gettop(L) - 2, LUA_MULTRET, 1);
        lua_remove(L, 1); // the first elem is the msgh

        if (r == LUA_OK)
        {
            lua_pushboolean(L, true);
            lua_insert(L, 1);
            return lua_gettop(L);
        }
        else if (r == LUA_ERRRUN || r == LUA_ERRERR)
        {
            lua_pushlightuserdata(L, (void *) &g_hpcall_exit_key);
            lua_gettable(L, LUA_REGISTRYINDEX);
            bool exit_flag = lua_toboolean(L, -1);
            lua_pop(L, 1); // pop exit flag

            if (exit_flag)
            {
                lua_error(L);
                return 0;
            }
            else
            {
                lua_pushboolean(L, false);
                lua_insert(L, 1);
                return lua_gettop(L);
            }
        }
        else
        {
            std::stringstream ss;
            ss << "xpcall: error number is " << r << std::endl;
            lua_settop(L, 0);
            luaL_error(L, ss.str().c_str());
            return 0;
        }
        return 0;
    });
    lua_setglobal(L, "xpcall");

    lua_pushcfunction(L, [](lua_State *L) -> int {
        // exit()
        HPCALL_Exit(L);
        return 0;
    });
    lua_setglobal(L, "exit");
}

void HPCALL_ResetExitFlag(lua_State *L)
{
    lua_pushlightuserdata(L, (void *) &g_hpcall_exit_key);
    lua_pushboolean(L, false);
    lua_settable(L, LUA_REGISTRYINDEX);
}

void HPCALL_Exit(lua_State *L)
{
    // set exit flag
    lua_pushlightuserdata(L, (void *) &g_hpcall_exit_key);
    lua_pushboolean(L, true);
    lua_settable(L, LUA_REGISTRYINDEX);

    // throw error
    lua_pushstring(L, g_hpcall_exit_msg);
    lua_error(L);
}

//END

/**
 * For PRSS functionality.
 */
struct SPRSS
{

    enum ERequest {
        REQUEST_NONE,
        REQUEST_PAUSE,
        REQUEST_RESUME,
        REQUEST_STOP,
        REQUEST_SLEEP,
        REQUEST_WAIT
    } request { REQUEST_NONE };

    enum EState {
        STATE_NORMAL = 1,
        STATE_PAUSED = 2,
        STATE_SLEEPING = 4 | STATE_NORMAL,
            // sleeping is also considered as the normal state
            // thus, PRSS_WaitForNormal() works, even when the state
            // was paused when sleeping.
            // (if we do not do this bitwise-or, PRSS_WaitForState() will
            // call STATE_NORMAL, and since
            // STATE_NORMAL & STATE_SLEEPING (state) would be 0, it would
            // wait forever.
        STATE_STOPPED = 8,
        STATE_WAITING = 16 | STATE_NORMAL,
            // same reason as the STATE_SLEEPING
        STATE_PAUSED_WHILE_SLEEPING = STATE_PAUSED | STATE_SLEEPING,
        STATE_PAUSED_WHILE_WAITING = STATE_PAUSED | STATE_WAITING
    } state { STATE_NORMAL };

    std::mutex                  mtx_request;
    std::condition_variable     cv_request;

    std::mutex                  mtx_state;
    std::condition_variable     cv_state;

    std::int64_t                remaining_duration;     // in ms
                                                        // not to be shared
                                                        // between different
                                                        // threads

};

//BEGIN Debugging Facilities

static inline const char *RequestToStr(SPRSS::ERequest request)
{
#define SPRSS_REQUEST_(x) case SPRSS :: REQUEST_##x : return "REQUEST_" #x;
    switch (request)
    {
        SPRSS_REQUEST_(NONE)
        SPRSS_REQUEST_(PAUSE)
        SPRSS_REQUEST_(RESUME)
        SPRSS_REQUEST_(STOP)
        SPRSS_REQUEST_(SLEEP)
        SPRSS_REQUEST_(WAIT)
    }
#undef SPRSS_REQUEST
    return "";
}

static inline const char *StateToStr(SPRSS::EState state)
{
#define SPRSS_STATE_(x) case SPRSS :: STATE_##x : return "STATE_" #x;
    switch (state)
    {
        SPRSS_STATE_(NORMAL)
        SPRSS_STATE_(PAUSED)
        SPRSS_STATE_(SLEEPING)
        SPRSS_STATE_(STOPPED)
        SPRSS_STATE_(WAITING)
        SPRSS_STATE_(PAUSED_WHILE_SLEEPING)
        SPRSS_STATE_(PAUSED_WHILE_WAITING)
    }
#undef SPRSS_STATE
    return "";
}

#define PRSS_MESSAGE(...) PRSS_Status( \
        __VA_ARGS__, \
        " function: ", BOOST_CURRENT_FUNCTION , \
        " thread: ", std::this_thread::get_id())

#ifdef PRSS_DEBUG

#ifdef PRSS_DEBUG_BLOCKABLES

static inline void PRSS_BlockableBegin(
    unsigned long line,
    const char *func) {
    PRSS_Status(
        "BLOCKABLE_BEGIN line: ", line,
        " function: ", func,
        " thread: ", std::this_thread::get_id());
}

static inline void PRSS_BlockableEnd(
    unsigned long line,
    const char *func) {
    PRSS_Status("BLOCKABLE_END line: ", line,
        " function: ", func,
        " thread: ", std::this_thread::get_id());
}

#define PRSS_BLOCKABLE(x) \
    PRSS_BlockableBegin( __LINE__ , BOOST_CURRENT_FUNCTION ); \
    x ; \
    PRSS_BlockableEnd( __LINE__ , BOOST_CURRENT_FUNCTION )

#else

#define PRSS_BLOCKABLE(x) x

#endif

static inline void PRSS_DumpSPRSS(
    SPRSS *sprss,
    unsigned long line,
    const char *func)
{
    assert(sprss);
    std::stringstream ss;
    ss.imbue(std::locale("C"));
    ss  << "DUMP_SPRSS_BEGIN line: " << line
        << " function: " << func
        << " thread: " << std::this_thread::get_id() << std::endl;
    {
        // std::unique_lock<std::mutex> lock{sprss->mtx_request};
        ss << "Request = " << RequestToStr(sprss->request) << std::endl;
    }
    {
        // std::unique_lock<std::mutex> lock{sprss->mtx_state};
        ss << "Request = " << StateToStr(sprss->state) << std::endl;
    }
    ss << "DUMP_SPRSS_END";
    PRSS_StatusMessage(ss.str().c_str());
}

#ifdef PRSS_DEBUG_DUMP_SPRSS

#define PRSS_DUMP_SPRSS(sprss) PRSS_DumpSPRSS( \
    (sprss), __LINE__ , BOOST_CURRENT_FUNCTION )

#else

#define PRSS_DUMP_SPRSS(sprss)

#endif

#define PRSS_STATUS(...) PRSS_Status( \
        __VA_ARGS__, \
        " function: ", BOOST_CURRENT_FUNCTION , \
        " thread: ", std::this_thread::get_id())

#else

#define PRSS_BLOCKABLE(x) x

#define PRSS_DUMP_SPRSS(x)

#define PRSS_STATUS(...)

#endif

//END

static std::unordered_map<lua_State *, SPRSS *> g_prss_map;
static std::mutex g_mtx_prss_map;

//BEGIN PRSS Main Implementation

static void PRSS_SetState(SPRSS *sprss, SPRSS::EState state)
{
    assert(sprss != nullptr);

    PRSS_BLOCKABLE(
        std::unique_lock<std::mutex> lock_state { sprss->mtx_state });
    PRSS_STATUS("Old state= ", StateToStr(sprss->state));
    sprss->state = state;
    sprss->cv_state.notify_all();
    PRSS_STATUS("New state= ", StateToStr(sprss->state));
}

static SPRSS::EState PRSS_GetState(SPRSS *sprss)
{
    assert(sprss != nullptr);
    PRSS_BLOCKABLE(
        std::unique_lock<std::mutex> lock_state { sprss->mtx_state });
    return sprss->state;
}

static void PRSS_WaitForState(SPRSS *sprss, SPRSS::EState state)
{
    assert(sprss != nullptr);
    PRSS_STATUS("State= ", StateToStr(state));
    PRSS_BLOCKABLE(
        std::unique_lock<std::mutex> lock_state{ sprss->mtx_state });
    PRSS_BLOCKABLE(sprss->cv_state.wait(lock_state, [sprss, state]() {
        return (sprss->state & state) != 0;
    }));
}

// timeout in ms
// 2 -> no timeout, state is unexpected
// 1 -> no timeout, state is fine
// 0 -> timeout
static int PRSS_WaitForStateTimeout(
    SPRSS *sprss,
    SPRSS::EState state,
    std::uint32_t timeout)
{
    assert(sprss != nullptr);
    PRSS_STATUS("State= ", StateToStr(state));
    PRSS_BLOCKABLE(std::unique_lock<std::mutex> lock_state{ sprss->mtx_state });
    PRSS_BLOCKABLE(std::cv_status r = sprss->cv_state.wait_for(
        lock_state, std::chrono::milliseconds(timeout)));
    if (r == std::cv_status::no_timeout)
    {
        if ((sprss->state & state) != 0)
            return 1;
        else
            return 2;
    }
    else
    {
        // PRSS_MESSAGE("Timeout! state is: ", StateToStr(sprss->state));
        return 0;
    }
}

static void PRSS_HandlerFunction(SPRSS *sprss, lua_State *L)
{
    assert(sprss != nullptr);

    PRSS_BLOCKABLE(
        std::unique_lock<std::mutex> lock_request{ sprss->mtx_request });
    PRSS_DUMP_SPRSS(sprss);

    if (PRSS_GetState(sprss) == SPRSS::STATE_STOPPED)
        // stopped before, but restarted
    {
        PRSS_SetState(sprss, SPRSS::STATE_NORMAL);
    }

    switch (sprss->request)
    {
    case SPRSS::REQUEST_PAUSE:
    {
        PRSS_SetState(sprss, SPRSS::STATE_PAUSED);
        sprss->request = SPRSS::REQUEST_NONE;
        sprss->cv_request.notify_all();

        PRSS_BLOCKABLE(sprss->cv_request.wait(lock_request, [sprss]()
        {
            return sprss->request != SPRSS::REQUEST_NONE;
        }));

        switch (sprss->request)
        {
        case SPRSS::REQUEST_STOP:
        {
            PRSS_SetState(sprss, SPRSS::STATE_STOPPED);
            sprss->request = SPRSS::REQUEST_NONE;
            sprss->cv_request.notify_all();
            lock_request.unlock();
            HPCALL_Exit(L);
        }
            break;
        case SPRSS::REQUEST_RESUME:
        {
            lock_request.unlock();
            PRSS_SetState(sprss, SPRSS::STATE_NORMAL);
        }
            break;
        default:
        {
            // shouldn't happen
        }
            break;
        }

    }
        break;
    case SPRSS::REQUEST_STOP:
    {
        sprss->request = SPRSS::REQUEST_NONE;
        sprss->cv_request.notify_all();
        lock_request.unlock();

        PRSS_SetState(sprss, SPRSS::STATE_STOPPED);
        HPCALL_Exit(L);
    }
        break;
    case SPRSS::REQUEST_SLEEP:
    {
        lock_request.unlock();

        PRSS_SetState(sprss, SPRSS::STATE_SLEEPING);

        bool cont = true;
        while (cont && sprss->remaining_duration > 0)
        {
            // here I may have re-used lock_request object, but whatever
            // no need for adding additional complexity
            PRSS_BLOCKABLE(
                std::unique_lock<std::mutex> lock_request_2
                    { sprss->mtx_request });
            auto begin = std::chrono::high_resolution_clock::now();
            PRSS_BLOCKABLE(
                std::cv_status r = sprss->cv_request.wait_for(lock_request_2,
                    std::chrono::milliseconds(sprss->remaining_duration)));
            auto end = std::chrono::high_resolution_clock::now();
            sprss->remaining_duration -=
                (std::int64_t) 1000.0 * /* ms */ std::chrono::duration_cast<
                    std::chrono::duration<double>>(end - begin).count();
            if (r == std::cv_status::no_timeout)
            {
                // mutex locked here
                switch (sprss->request)
                {
                case SPRSS::REQUEST_PAUSE:
                {
                    PRSS_SetState(sprss, SPRSS::STATE_PAUSED_WHILE_SLEEPING);
                    sprss->request = SPRSS::REQUEST_NONE;
                    sprss->cv_request.notify_all();

                    PRSS_BLOCKABLE(
                        sprss->cv_request.wait(lock_request_2, [sprss]()
                        {
                            return sprss->request != SPRSS::REQUEST_NONE;
                        }));

                    switch (sprss->request)
                    {
                    case SPRSS::REQUEST_STOP:
                    {
                        PRSS_SetState(sprss, SPRSS::STATE_STOPPED);
                        sprss->request = SPRSS::REQUEST_NONE;
                        sprss->cv_request.notify_all();
                        lock_request_2.unlock();
                        HPCALL_Exit(L); // HPCALL does a longjmp, therefore
                                        // C++ objects are **not** destroyed
                                        // and RAII doesn't happen.
                                        // This is why the mutex is locked,
                                        // and in the next step, it stucks
                                        // infinitely.
                                        // Also, since this happens in
                                        // STATE_SLEEPING, it is not observed
                                        // in hard_sleep(...)
                                        // So, whenever I call HPCALL_,
                                        // I unlock just before.
                                        // In addition to that, the request
                                        // flag is also not reset to NONE.
                                        // This causes immediate stops in
                                        // further executions.
                        cont = false;
                    }
                        break;
                    case SPRSS::REQUEST_RESUME:
                    {
                        PRSS_SetState(sprss, SPRSS::STATE_SLEEPING);
                    }
                        break;
                    default:
                    {
                        // shouldn't happen
                    }
                        break;
                    }
                }
                    break;
                case SPRSS::REQUEST_STOP:
                {
                    PRSS_SetState(sprss, SPRSS::STATE_STOPPED);
                    sprss->request = SPRSS::REQUEST_NONE;
                    sprss->cv_request.notify_all();
                    lock_request_2.unlock();
                    HPCALL_Exit(L);
                    cont = false;
                }
                    break;
                default:
                {
                    // should not happen
                }
                    break;
                }
            }
            else
            {
                // timeout
                cont = false; // break the loop and exit normally
                PRSS_SetState(sprss, SPRSS::STATE_NORMAL);
            }
        }
    }
        break;
    case SPRSS::REQUEST_WAIT:
    {
        lock_request.unlock();

        bool cont = true;
        while (cont)
        {
            PRSS_BLOCKABLE(
                std::unique_lock<std::mutex> lock_request_2
                    { sprss->mtx_request });
            sprss->request = SPRSS::REQUEST_NONE;
            sprss->cv_request.notify_all();
                // notify for example PRSS_Pause which is waiting for
                // REQUEST_NONE
            PRSS_SetState(sprss, SPRSS::STATE_WAITING);
            PRSS_BLOCKABLE(
                sprss->cv_request.wait(lock_request_2, [sprss](){
                    return sprss->request != SPRSS::REQUEST_NONE;
                }));
            switch (sprss->request)
            {
            case SPRSS::REQUEST_PAUSE:
            {
                PRSS_SetState(sprss, SPRSS::STATE_PAUSED_WHILE_WAITING);
                sprss->request = SPRSS::REQUEST_NONE;
                sprss->cv_request.notify_all();

                PRSS_BLOCKABLE(
                    sprss->cv_request.wait(lock_request_2, [sprss]()
                    {
                        return sprss->request != SPRSS::REQUEST_NONE;
                    }));

                switch (sprss->request)
                {
                case SPRSS::REQUEST_STOP:
                {
                    PRSS_SetState(sprss, SPRSS::STATE_STOPPED);
                    sprss->request = SPRSS::REQUEST_NONE;
                    sprss->cv_request. notify_all();
                    lock_request_2.unlock();
                    HPCALL_Exit(L);
                    cont = false;
                }
                    break;
                case SPRSS::REQUEST_RESUME:
                {
                    PRSS_SetState(sprss, SPRSS::STATE_WAITING);
                }
                    break;
                default:
                {
                    // shouldn't happen
                }
                    break;
                }
            }
                break;
            case SPRSS::REQUEST_STOP:
            {
                PRSS_SetState(sprss, SPRSS::STATE_STOPPED);
                sprss->request = SPRSS::REQUEST_NONE;
                sprss->cv_request.notify_all();
                lock_request_2.unlock();
                HPCALL_Exit(L);
                cont = false;
            }
                break;
            case SPRSS::REQUEST_RESUME:
            {
                PRSS_SetState(sprss, SPRSS::STATE_NORMAL);
                cont = false;
            }
                break;
            default:
            {
            }
                break;
            }
        }
    }
        break;
    default:
        lock_request.unlock();
        break;
    }

    // always reset the request at the and
    lock_request.lock();
    sprss->request = SPRSS::REQUEST_NONE;
}

//END

//BEGIN PRSS Initials/Finals

// PRS stands for Pause/Resume/Stop
void PRSS_Register(lua_State *L)
{
    {
        std::lock_guard<std::mutex> lock {g_mtx_prss_map};
        g_prss_map[L] = new SPRSS();
    }
    HPCALL_Register(L);

    // register hook
    lua_sethook(L, [](lua_State *L, lua_Debug *) -> void {
        std::unique_lock<std::mutex> lock{g_mtx_prss_map};
        SPRSS *sprss = g_prss_map[L];
        lock.unlock();

        PRSS_HandlerFunction(sprss, L);
    },
    LUA_MASKLINE | LUA_MASKCOUNT, 100);

    lua_pushcfunction(L, [](lua_State *L) -> int {
        // sleep(time)

        std::unique_lock<std::mutex> lock{g_mtx_prss_map};
        SPRSS *sprss = g_prss_map[L];
        lock.unlock();

        std::uint32_t time = luaL_checkinteger(L, 1);

        // no need for sync here, same threads
        sprss->remaining_duration = time;

        {
            std::unique_lock<std::mutex> lock_request{sprss->mtx_request};
            sprss->request = SPRSS::REQUEST_SLEEP;
        }

        PRSS_HandlerFunction(sprss, L);
        return 0;
    });
    lua_setglobal(L, "sleep");

    lua_pushcfunction(L, [](lua_State *L) -> int {
        // wait()

        std::unique_lock<std::mutex> lock{g_mtx_prss_map};
        SPRSS *sprss = g_prss_map[L];
        lock.unlock();

        {
            std::unique_lock<std::mutex> lock_request{sprss->mtx_request};
            sprss->request = SPRSS::REQUEST_WAIT;
        }

        PRSS_HandlerFunction(sprss, L);
        return 0;
    });
    lua_setglobal(L, "wait");

    lua_pushcfunction(L, [](lua_State *L) {
        std::uint32_t time = luaL_checkinteger(L, 1);
        std::this_thread::sleep_for(std::chrono::milliseconds(time));
        return 0;
    });
    lua_setglobal(L, "hard_sleep");

    lua_pushcfunction(L, [](lua_State *L) {
        const char *str = luaL_checkstring(L, 1);
        PRSS_StatusMessage(str);
        return 0;
    });
    lua_setglobal(L, "prss_status");
}

void PRSS_Free()
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    for (auto &x: g_prss_map)
    {
        delete x.second;
    }
    g_prss_map.clear();
}

void PRSS_FreeState(lua_State *L)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    auto it = g_prss_map.find(L);
    if (it != g_prss_map.end())
    {
        delete it->second;
    }
}

//END

void PRSS_Pause(lua_State *L)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS &sprss = *g_prss_map[L];
    lock1.unlock();

    /* std::unique_lock<std::mutex> lock2{ sprss.mtx_request };
    sprss.cv_request.wait(
        lock2,
        [&sprss](){ return sprss.request == SPRSS::REQUEST_NONE; }
    ); // wait until no request is guaranteed */
    // NOTE
    // UNFORTUNATELY, it block above somehow, it's better to get rid off it
    // the piece code as the following:
    //     sprss->request = SPRSS::REQUEST_NONE;
    //     sprss->cv_request.notify_all();
    // it's rubbish for now. (at least the notify_all part)

    sprss.request = SPRSS::REQUEST_PAUSE;
    sprss.cv_request.notify_all();
}

//BEGIN PRSS_WaitForPaused*

void PRSS_WaitForPaused(lua_State *L)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS *sprss = g_prss_map[L];
    lock1.unlock();

    PRSS_WaitForState(sprss, SPRSS::STATE_PAUSED /* thanks bitwise-and */);
}

int PRSS_WaitForPausedTimeout(lua_State *L, uint32_t milli)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS *sprss = g_prss_map[L];
    lock1.unlock();

    return PRSS_WaitForStateTimeout(sprss, SPRSS::STATE_PAUSED, milli);
}

//END

void PRSS_Resume(lua_State *L)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS &sprss = *g_prss_map[L];
    lock1.unlock();

    std::unique_lock<std::mutex> lock2{ sprss.mtx_request };
    /* sprss.cv_request.wait(
        lock2,
        [&sprss](){ return sprss.request == SPRSS::REQUEST_NONE; }
    ); // wait until no request is guaranteed */

    sprss.request = SPRSS::REQUEST_RESUME;
    sprss.cv_request.notify_all();
}

int PRSS_IsPaused(lua_State *L)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS *sprss = g_prss_map[L];
    lock1.unlock();

    return (PRSS_GetState(sprss) & SPRSS::STATE_PAUSED) == SPRSS::STATE_PAUSED;
    // look at the comment in PRSS_IsWaiting()
}

// identical to PRSS_Resume(L)
void PRSS_StopWaiting(lua_State *L)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS &sprss = *g_prss_map[L];
    lock1.unlock();

    std::unique_lock<std::mutex> lock2{ sprss.mtx_request };
    /* sprss.cv_request.wait(
        lock2,
        [&sprss](){ return sprss.request == SPRSS::REQUEST_NONE; }
    ); // wait until no request is guaranteed */
    sprss.request = SPRSS::REQUEST_RESUME;
    sprss.cv_request.notify_all();
}

void PRSS_WaitForWaiting(lua_State *L)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS *sprss = g_prss_map[L];
    lock1.unlock();

    PRSS_WaitForState(sprss, SPRSS::STATE_WAITING /* thanks bitwise-and */);
}

int PRSS_WaitForWaitingTimeout(lua_State *L, uint32_t milli)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS *sprss = g_prss_map[L];
    lock1.unlock();

    return PRSS_WaitForStateTimeout(sprss, SPRSS::STATE_WAITING, milli);
}

int PRSS_IsWaiting(lua_State *L)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS *sprss = g_prss_map[L];
    lock1.unlock();

    return (PRSS_GetState(sprss) & SPRSS::STATE_WAITING)
        == SPRSS::STATE_WAITING     /* 
                                     * IMPORTANT!
                                     * needed instead of != 0
                                     * for example, when sleeping
                                     * STATE_SLEEPING | STATE_WAITING != 0
                                     * as they both have STATE_NORMAL inside.
                                     * but this is absolutely not what we want
                                     */;
}

void PRSS_WaitForStopped(lua_State *L)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS *sprss = g_prss_map[L];
    lock1.unlock();

    PRSS_WaitForState(sprss, SPRSS::STATE_STOPPED /* thanks bitwise-and */);
}

int PRSS_WaitForStoppedTimeout(lua_State *L, uint32_t milli)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS *sprss = g_prss_map[L];
    lock1.unlock();

    return PRSS_WaitForStateTimeout(sprss, SPRSS::STATE_STOPPED, milli);
}

void PRSS_WaitForNormal(lua_State *L)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS *sprss = g_prss_map[L];
    lock1.unlock();

    PRSS_WaitForState(sprss, SPRSS::STATE_NORMAL /* thanks bitwise-and */);
}

int PRSS_WaitForNormalTimeout(lua_State *L, uint32_t milli)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS *sprss = g_prss_map[L];
    lock1.unlock();

    return PRSS_WaitForStateTimeout(sprss, SPRSS::STATE_NORMAL, milli);
}

void PRSS_Stop(lua_State *L)
{
    std::unique_lock<std::mutex> lock1 {g_mtx_prss_map};
    SPRSS &sprss = *g_prss_map[L];
    lock1.unlock();

    std::unique_lock<std::mutex> lock2{ sprss.mtx_request };
    /* sprss.cv_request.wait(
        lock2,
        [&sprss](){ return sprss.request == SPRSS::REQUEST_NONE; }
    ); // wait until no request is guaranteed */
    sprss.request = SPRSS::REQUEST_STOP;
    sprss.cv_request.notify_all();
}

/**
 * Debugging functionality
 */

//BEGIN Debugging

std::mutex g_mtx_cout;

std::chrono::milliseconds g_start;
std::mutex g_mtx_start;

std::mutex g_mtx_target;
static void (*g_target_func)(const char *, void *) = nullptr;
static void *g_target_data;

void PRSS_SetTarget(void (*target_func)(const char *, void *), void *data)
{
    std::unique_lock<std::mutex> lock{g_mtx_target};
    g_target_func = target_func;
    g_target_data = data;
}

void PRSS_StartTimer()
{
    using namespace std::chrono;
    std::lock_guard<std::mutex> lock {g_mtx_start};
    g_start = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
}

void PRSS_StatusMessage(const char *s)
{
    using namespace std::chrono;
    auto current = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
    auto time = (current - g_start).count() / 1000.0 /* to seconds */;

    std::stringstream ss;
    ss.imbue(std::locale("C"));
    ss << "[ " << time << " ] " << s;
    std::string str = ss.str();

    std::unique_lock<std::mutex> lock_target {g_mtx_target};
    if (g_target_func == nullptr)
    {
        lock_target.unlock();
        std::unique_lock<std::mutex> lock {g_mtx_cout};
        std::cout << str << std::endl;
    }
    else
    {
        g_target_func(str.c_str(), g_target_data);
    }
}

//END

#ifdef __cplusplus
}
#endif
