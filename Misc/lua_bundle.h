/**
 * 
 * LuaBundle: Simple multithreaded data sharing between lua_State s.
 * 
 * Canberk Sönmez
 * 
 * Date: 2016-08-13
 * 
 */

#ifndef _LUA_BUNDLE_H_INCLUDED
#define _LUA_BUNDLE_H_INCLUDED

#ifdef __cplusplus

extern "C"
{
#endif

#include <lua.h>
#include <stddef.h>

typedef int LBNDL_Result;

struct LBNDL_Bundle;
struct LBNDL_Manager;

#define LBNDL_ERR_NOERROR           0
#define LBNDL_ERR_NULLMANAGER       1
#define LBNDL_ERR_NULLSTATE         2
#define LBNDL_ERR_NULLBUNDLE        3
#define LBNDL_ERR_NULLSTRING        4
#define LBNDL_ERR_NULLPTR           5
#define LBNDL_ERR_NOTFOUND          6
#define LBNDL_ERR_EXISTS            7
#define LBNDL_ERR_INCORRECTTYPE     8
#define LBNDL_ERR_INCORRECTPARAM    9

#define LBNDL_TRUE                  1
#define LBNDL_FALSE                 2

#define LBNDL_TYPE_INTEGER          1
#define LBNDL_TYPE_NUMBER           2
#define LBNDL_TYPE_STRING           3
#define LBNDL_TYPE_BOOLEAN          4
#define LBNDL_TYPE_LUDATA           5
#define LBNDL_TYPE_HUDATA           6

LBNDL_Manager *LBNDL_CreateBundleManager(
    );

LBNDL_Result LBNDL_RegisterBundleManager(
    LBNDL_Manager *manager,
    lua_State *state,
    const char *name);

LBNDL_Result LBNDL_UnassociateState(
    LBNDL_Manager *manager,
    lua_State *state
);

LBNDL_Result LBNDL_DestroyBundleManager(
    LBNDL_Manager *manager);


LBNDL_Result LBNDL_GetBundle(
    LBNDL_Manager *manager,
    const char *bundle_name,
    LBNDL_Bundle **result);

LBNDL_Result LBNDL_CreateBundle(
    LBNDL_Manager *manager,
    const char *bundle_name,
    LBNDL_Bundle **result);

LBNDL_Result LBNDL_DestroyBundle(
    LBNDL_Bundle *bundle);


LBNDL_Result LBNDL_GetType(
    LBNDL_Bundle *bundle,
    const char *key,
    int *type,
    char **tname);


LBNDL_Result LBNDL_GetInteger(
    LBNDL_Bundle *bundle,
    const char *key,
    lua_Integer *result);

LBNDL_Result LBNDL_SetInteger(
    LBNDL_Bundle *bundle,
    const char *key,
    lua_Integer value);


LBNDL_Result LBNDL_GetBoolean(
    LBNDL_Bundle *bundle,
    const char *key,
    int *result);

LBNDL_Result LBNDL_SetBoolean(
    LBNDL_Bundle *bundle,
    const char *key,
    int value);


LBNDL_Result LBNDL_GetNumber(
    LBNDL_Bundle *bundle,
    const char *key,
    lua_Number *result);

LBNDL_Result LBNDL_SetNumber(
    LBNDL_Bundle *bundle,
    const char *key,
    lua_Number value);


LBNDL_Result LBNDL_GetString(
    LBNDL_Bundle *bundle,
    const char *key,
    char **result,
    size_t *len);

LBNDL_Result LBNDL_SetString(
    LBNDL_Bundle *bundle,
    const char *key,
    const char *data,
    size_t len);

LBNDL_Result LBNDL_GetUserdata(
    LBNDL_Bundle *bundle,
    const char *key,
    void **data,
    size_t *sz,
    char **tname,
    int *type);

LBNDL_Result LBNDL_SetUserData(
    LBNDL_Bundle *bundle,
    const char *key,
    const void *data,
    size_t sz,
    char *tname,
    int type);

LBNDL_Result LBNDL_DeleteKey(
    LBNDL_Bundle *bundle,
    const char *key);


LBNDL_Result LBNDL_LockBundle(
    LBNDL_Bundle *bundle);

LBNDL_Result LBNDL_UnlockBundle(
    LBNDL_Bundle *bundle);


LBNDL_Result LBNDL_LockKey(
    LBNDL_Bundle *bundle,
    const char *key);

LBNDL_Result LBNDL_UnlockKey(
    LBNDL_Bundle *bundle,
    const char *key);

LBNDL_Result LBNDL_UnlockAllKeys(
    LBNDL_Bundle *bundle);

const char *LBNDL_GetErrorMsg(
    LBNDL_Result r);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _LUA_BUNDLE_H_INCLUDED
