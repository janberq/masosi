/**
 * 
 * Canberk Sönmez
 * 
 * 2016-07-12
 * 
 */

#ifndef _SIMULATION_HPP_INCLUDED
#define _SIMULATION_HPP_INCLUDED

#include <string>
#include <functional>
#include <list>
#include <vector>
#include <array>
#include <unordered_map>
#include <string>

struct lua_State;

// there is a function in win32 api called "Polygon", it fucks up my
// polygon class with its name. Moving everything to a common namespace
// is a remedy for that. another remedy could be renaming Polygon class
// but it would be peculiar.

namespace masosi
{

/**
 * Some declarations
 */

/**
 * Simulation Objects
 */

class Vector;
class Color;
class Polygon;
class Simulation;

/**
 * Vector
 */

class Vector
{

public:

    double x, y;

    constexpr Vector() : x(0), y(0) {  }
    constexpr Vector(double _x, double _y) : x(_x), y(_y) {  }
    constexpr Vector(const Vector &p) : x(p.x), y(p.y) {  }

    constexpr Vector operator+(const Vector &p) const {
        return Vector { x + p.x, y + p.y };
    }
    constexpr Vector operator-(const Vector &p) const {
        return Vector { x - p.x, y - p.y };
    }
    constexpr Vector operator*(double s) const {
        return Vector { s * x, s * y };
    }
    constexpr double operator*(const Vector &p) const {
        return x * p.x + y * p.y;
    }
    constexpr Vector operator/(double s) const {
        return Vector { x / s, y / s };
    }

    constexpr Vector operator+() const {
        return *this;
    }
    constexpr Vector operator-() const {
        return Vector { -x, -y };
    }

    constexpr Vector &operator+=(const Vector &p) {
        x += p.x;
        y += p.y;
        return *this;
    }
    constexpr Vector &operator-=(const Vector &p) {
        x -= p.x;
        y -= p.y;
        return *this;
    }
    constexpr Vector &operator*=(double s) {
        x *= s;
        y *= s;
        return *this;
    }
    constexpr Vector &operator/=(double s) {
        x /= s;
        y /= s;
        return *this;
    }
    constexpr Vector &operator=(const Vector &p) {
        if (this != &p) {
            x = p.x; y = p.y;
        }
        return *this;
    }

    constexpr bool operator==(const Vector &p) {
        return x == p.x && y == p.y;
    }
    constexpr bool operator!=(const Vector &p) {
        return x != p.x || y != p.y;
    }

    double norm() const;
    Vector unit() const;
    Vector &normalize();
    double cross(const Vector &p) const;
    Vector &rotate(double a);
    Vector rotated(double a) const;
    Vector normal() const;
    Vector unit_normal() const;

    constexpr void copy_from(const Vector &p)
    { *this = p; }

    constexpr void set(double _x, double _y)
    { x = _x; y = _y; }

    std::string repr() const;

};

constexpr Vector operator*(double a, const Vector &b) {
    return Vector { a * b.x, a * b.y };
}

void RegisterVector(lua_State *L, const std::string &name = "Vector");

/**
 * Color
 */

class Color
{

public:

    int r, g, b, a;

    constexpr Color() : r(0), g(0), b(0), a(255) {  }
    constexpr Color(int _r, int _g, int _b, int _a)
        : r(_r), g(_g), b(_b), a(_a) {  }
    constexpr Color(int _r, int _g, int _b)
        : r(_r), g(_g), b(_b), a(255) {  }
    constexpr Color(const Color &other) :
        r(other.r), g(other.g), b(other.b), a(other.a)
    {  }

    constexpr Color &operator=(const Color &other)
    {
        if (this != &other)
        {
            r = other.r;
            g = other.g;
            b = other.b;
            a = other.a;
        }
        return *this;
    }

    constexpr void copy_from(const Color &other)
    { r = other.r; g = other.g; b = other.b; a = other.a; }

    constexpr void set(int _r, int _g, int _b, int _a)
    { r = _r; g = _g; b = _b; a = _a; }

};

void RegisterColor(lua_State *L, const std::string &name = "Color");

/**
 * Serves as everything!
 */

class Polygon
{
    friend class Simulation;
public:

    Polygon();
    Polygon(const Polygon &other);
    Polygon(Polygon &&other);

    Polygon &operator=(const Polygon &other);
    Polygon &operator=(Polygon &&other);

    void copy_from(const Polygon &other);
    void move_from(Polygon &other);

    void BeginVectors();
    void EndVectors();

    Vector *AddVector(const Vector &p);
    Vector *AddVector(double x, double y);
    Vector *OwnVector(Vector *p);

    void IterateVectors(std::function<bool (Vector *)> f);
    void IterateVectors(std::function<bool (const Vector *)> f) const;

    void DeleteVector(Vector *p);
    Vector *DetachVector(Vector *p);

    void ClearVectors();

    std::size_t GetVectorCount() const;

    void PrepareFast();

    bool IsInside(const Vector &p) const;
    bool IsInside(double x, double y) const;

    bool FastIsInside(const Vector &p) const;
    bool FastIsInside(double x, double y) const;

    // The other Polygon must also be prepared using PrepareFast() method
    bool FastIntersects(const Polygon &polygon) const;

    bool Intersects(const Polygon &polygon) const;

    std::vector<Vector> FastIntersectionsWithSegment(
        const Vector &a,
        const Vector &b);

    std::vector<Vector> IntersectionsWithSegment(
        const Vector &a,
        const Vector &b);

    Vector position     { 0, 0 };
    double rotation     { 0.0 };
    Color color         { 0, 100, 100 };
    Color border_color  { 0, 50, 50 };
    int border          { 0 };
    bool enabled        { true };
    bool shown          { true };
    int z_order         { 0 };

    std::string         name;
    std::string         type;

    virtual ~Polygon();

protected:

    void RefreshVector();

    void DoPreparePoints(std::vector<Vector> &output) const;

    std::list<Vector *> pts_;
    std::vector<Vector *> pts_vect_;
    std::vector<Vector> fast_pts_;
    bool is_in_block_ { false };
    bool is_in_iteration_ { false };

};

void RegisterPolygon(
    lua_State *L,
    const std::string &name = "Polygon");

/**
 * Simulation
 */

class Simulation
{
public:

    Simulation();

    Simulation(const Simulation &) = delete;
    Simulation(Simulation &&) = delete;

    Simulation &operator=(const Simulation &) = delete;
    Simulation &operator=(Simulation &&) = delete;

    Polygon *NewPolygon();
    Polygon *AddPolygon(const Polygon &poly);
    Polygon *OwnPolygon(Polygon *poly);
    void IteratePolygons(std::function<bool (Polygon *)> f);
    void IteratePolygons(std::function<bool (const Polygon *)> f) const;
    void DeletePolygon(Polygon *poly);
    Polygon *DetachPolygon(Polygon *poly);
    void ClearPolygons();
    std::size_t GetPolygonCount() const;

    void CollisionCheck(const Polygon &p, std::function<bool (Polygon *)> func);
    void CollisionCheck(const Polygon &p, std::function<bool (const Polygon *)> func) const;

    void CollisionCheck(const Vector &p, std::function<bool (Polygon *)> func);
    void CollisionCheck(const Vector &p, std::function<bool (const Polygon *)> func) const;

    double time     { 0.0 };

    virtual ~Simulation();

protected:

    std::list<Polygon *> polygons_;
};

void RegisterSimulation(
    lua_State *L,
    const std::string &name = "Simulation");

}

#endif // _SIMULATION_HPP_INCLUDED
