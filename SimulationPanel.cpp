#include "SimulationPanel.hpp"

#include <wx/dcbuffer.h>

#include <Eigen/Core>
#include <Eigen/Dense>

#include <vector>
#include <iterator>
#include <unordered_map>
#include <algorithm>

#include <cassert>

#define UNUSED(x)

namespace masosi {

/**
 * Some other helper stuff
 */

inline wxColour AswxColour(const Color &in)
{
    return wxColour(in.r, in.g, in.b, in.a);
}

inline Vector AsVector(const wxPoint &in)
{
    return Vector { (double) in.x, (double) in.y };
}

inline wxPoint AswxPoint(const Vector &in)
{
    return wxPoint { (int) in.x, (int) in.y };
}

/**
 * The following *Data are used for drawing
 */

class PolygonData
{
public:
    int z_order;
    int borderwi;
    bool shown;
    wxColour bgclr;
    wxColour fgclr;
    std::vector<wxPoint> pts;
};

//BEGIN DefaultZoomScaleProvider

class DefaultZoomScaleProvider : public SimulationPanel::ZoomScaleProvider
{

private:
    static const double zoom_values_ [];
    static std::size_t default_index_;
public:

    double GetZoomMultiplier(int zoom_index) override
    {
        return zoom_values_[zoom_index + default_index_] / 100.0;
    }

    bool IsValidZoomIndex(int zoom_index) override;
};

const double DefaultZoomScaleProvider::zoom_values_ [] = {
    12.0,
    25.0,
    33.0,
    50.0,
    66.0,
    75.0,
    90.0,
    100.0,
    110.0,
    125.0,
    150.0,
    175.0,
    200.0,
    250.0,
    300.0,
    400.0,
    500.0
};

std::size_t DefaultZoomScaleProvider::default_index_ = 5;

bool DefaultZoomScaleProvider::IsValidZoomIndex(int zoom_index)
{
    static std::size_t count = sizeof(zoom_values_) / sizeof(double);
        // size is known here
    static int min = -default_index_;
    static int max = count - 1 - default_index_;
    // minimum_index -> 0
    // zoom_index = 0 -> default_zoom_value_index_
    // maximum_index -> zoom_values_count_ - 1
    return zoom_index <= max && zoom_index >= min;
}


//END

class SimulationPanel::Impl
{
public:

    Impl(SimulationPanel *panel);

    bool Create(
        wxWindow* parent,
        wxWindowID winid,
        const wxPoint& position,
        const wxSize& size,
        const wxString& name
    );

    Simulator *GetSimulator();
    void SetSimulator(Simulator *simulator);

    void ResetPrefsToDefault();
    void ResetPrefs();

    void SetPrefs(const SimulationPanelPrefs &prefs);

    SimulationPanelPrefs GetOriginalPrefs() const;
    SimulationPanelPrefs GetPrefs() const;
    SimulationPanelPrefs &Prefs();

    void RefreshPolygon(Polygon *poly, bool lock_simulation = true);
    void RefreshPolygons(bool lock_simulation = true);
    void RefreshMatrices();
    void RefreshAll(bool lock_simulation = true);

    // you need to call RefreshMatrices() and/or Refresh() explicitly
    void Zoom(
        double new_zoom_x,
        double new_zoom_y);
    void Zoom(
        const Vector &world_anchor,
        const Vector &screen_anchor,
        double new_zoom_x,
        double new_zoom_y);
    void Zoom(
        const wxPoint &screen_anchor,
        double new_zoom_x,
        double new_zoom_y);

//BEGIN OnPaintHook
    void AddOnPaintHook(OnPaintHook *hook);
    OnPaintHook *DetachOnPaintHook(OnPaintHook *hook);
    void DetachAllOnPaintHooks(OnPaintHook ***hooks);
    void DeleteAllOnPaintHooks();

    // returns a NULL terminated array of `OnPaintHook*`s.
    SimulationPanel::OnPaintHook * const * GetOnPaintHooks() const;
//END

//BEGIN ZoomScaleProvider
    SimulationPanel::ZoomScaleProvider *GetZoomScaleProvider();
    SimulationPanel::ZoomScaleProvider *SetZoomScaleProvider(
        SimulationPanel::ZoomScaleProvider *provider);
    SimulationPanel::ZoomScaleProvider *DetachZoomScaleProvider();
//END

    Vector TransformW2S(const Vector &v) const;
    Vector TransformS2W(const Vector &v) const;

    ~Impl();

protected:
    void DoRefreshPolygon(Polygon *obj);

    /* Simulation Panel */
    SimulationPanel *panel_;

    /* Transformation Matrix */
    Eigen::Matrix3d transformation_;
    Eigen::Matrix3d inverse_transformation_;

    /* Rendering related */
    double scale_;
    Vector origin_;
    Vector screen_origin_;

    /* Preferences */
    SimulationPanelPrefs original_prefs_;
    SimulationPanelPrefs prefs_;

    /* Panning */
    bool panning_ { false };
    wxPoint panning_last_ { 0, 0 };

    /* Simulation */
    Simulation *    simulation_     { nullptr };
    Simulator *     simulator_      { nullptr };

    /* Caches */
    std::unordered_map<Polygon *, PolygonData*> polygon_cache_;
    std::vector<PolygonData *> polygon_cache_ordered_;

    void ReorderPolygonCache();

    void DoClearCache();

    /* Hooks */
    std::vector<SimulationPanel::OnPaintHook *> on_paint_hooks_;

    wxPoint ConvW2S(const Vector &p);
    Vector ConvS2W(const wxPoint &p);

    void DrawPolygon(wxDC &dc, PolygonData &data);

    void DrawPolygon(wxDC &dc, Polygon *obj);

    DefaultZoomScaleProvider defaultZoomScaleProvider_;

    SimulationPanel::ZoomScaleProvider *zoomScaleProvider_ {nullptr};

    SimulationPanel::ZoomScaleProvider &ZoomScaleProvider();

    /* zoom factors */
    double zoomFactorX_{1}, zoomFactorY_{1};

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

SimulationPanel::Impl::Impl(SimulationPanel* panel)
{
    panel_ = panel;
}

bool SimulationPanel::Impl::Create(
    wxWindow* parent,
    wxWindowID winid,
    const wxPoint& position,
    const wxSize& size,
    const wxString& name)
{
    assert(panel_ != nullptr);

    on_paint_hooks_.push_back(nullptr); // push nullptr for the sake of the
                                        // on paint hooks' functions

    if (!panel_->wxPanel::Create(
        parent,
        winid,
        position,
        size,
        wxTAB_TRAVERSAL,
        name))
    {
        return false;
    }

    panel_->SetBackgroundColour(*wxWHITE);
    panel_->SetBackgroundStyle(wxBG_STYLE_PAINT);
    panel_->Bind(wxEVT_PAINT, [this](wxPaintEvent &WXUNUSED(evt))
    {
        if (!simulation_) return;
        wxBufferedPaintDC dc(panel_);

        dc.Clear();

        auto it = on_paint_hooks_.begin();
        for (; it != on_paint_hooks_.end() && (*it)->z_order < 0; ++it)
            (*it)->OnPaint(dc);

        // we only draw caches, they have z_order = 0,
        // also a sub-z_order which may vary internally
        for (auto &polygon: polygon_cache_ordered_)
        {
            if (polygon->shown)
            {
                DrawPolygon(dc, *polygon);
            }
        }

        for (; it != on_paint_hooks_.end() - 1 /* exclude the nullptr */; ++it)
            (*it)->OnPaint(dc);

    });
    panel_->Bind(wxEVT_SIZE, [this](wxSizeEvent &evt) {
        RefreshAll();
        evt.Skip(); // for wxSizer as noted in docs
    });
    panel_->Bind(wxEVT_LEFT_DOWN, [this](wxMouseEvent &evt) {
        panning_ = true;
        panning_last_ = evt.GetPosition();
        evt.Skip(); // on wxMsw, to scroll a window (to zoom)
        // we need the window to have the focus
        // (even w/o that works fine on windows 10,
        // the problem was first observed on windows 7)
    });
    panel_->Bind(wxEVT_LEFT_UP, [this](wxMouseEvent &UNUSED(evt)) {
        panning_ = false;
    });
    panel_->Bind(wxEVT_MOTION, [this](wxMouseEvent &evt){
        if (panning_)
        {
            prefs_.screenOriginOffset +=
                AsVector(evt.GetPosition() - panning_last_);
            panning_last_ = evt.GetPosition();
            RefreshAll();
        }
    });
    panel_->Bind(wxEVT_MOUSEWHEEL, [this](wxMouseEvent &evt) {
        auto &zsprov = ZoomScaleProvider();
        if (evt.ControlDown() && evt.ShiftDown())
        {
            double common_zoom;
            int common_zoom_index;
            if (evt.GetWheelRotation() > 0) // zoom in
            {
                if (zoomFactorX_ < zoomFactorY_)
                    common_zoom_index = prefs_.zoomFactorXindex;
                else
                    common_zoom_index = prefs_.zoomFactorYindex;
                ++common_zoom_index;
            }
            else    // zoom out
            {
                if (zoomFactorX_ > zoomFactorY_)
                    common_zoom_index = prefs_.zoomFactorXindex;
                else
                    common_zoom_index = prefs_.zoomFactorYindex;
                --common_zoom_index;
            }
            if (zsprov.IsValidZoomIndex(common_zoom_index))
            {
                common_zoom = zsprov.GetZoomMultiplier(common_zoom_index);
                Zoom(evt.GetPosition(), common_zoom, common_zoom);
                prefs_.zoomFactorXindex = prefs_.zoomFactorYindex = common_zoom_index;
                RefreshAll();
            }
        }
        else if (evt.ControlDown())
        {
            // only change x axis
            int new_zoom_index = prefs_.zoomFactorXindex;
            if (evt.GetWheelRotation() > 0)
                ++new_zoom_index;
            else
                --new_zoom_index;
            if (zsprov.IsValidZoomIndex(new_zoom_index))
            {
                double new_zoom_factor = zsprov.
                    GetZoomMultiplier(new_zoom_index);
                Zoom(evt.GetPosition(), new_zoom_factor, zoomFactorY_);
                prefs_.zoomFactorXindex = new_zoom_index;
                RefreshAll();
            }
        }
        else if (evt.ShiftDown())
        {
            // only change y axis
            int new_zoom_index = prefs_.zoomFactorYindex;
            if (evt.GetWheelRotation() > 0)
                ++new_zoom_index;
            else
                --new_zoom_index;
            if (zsprov.IsValidZoomIndex(new_zoom_index))
            {
                double new_zoom_factor = zsprov.
                    GetZoomMultiplier(new_zoom_index);
                Zoom(evt.GetPosition(), zoomFactorX_, new_zoom_factor);
                prefs_.zoomFactorYindex = new_zoom_index;
                RefreshAll();
            }
        }
        else
        {
            int new_zoom_index_x = prefs_.zoomFactorXindex;
            int new_zoom_index_y = prefs_.zoomFactorYindex;
            if (evt.GetWheelRotation() > 0)
            { ++new_zoom_index_x; ++new_zoom_index_y; }
            else
            { --new_zoom_index_x; --new_zoom_index_y; }
            if (zsprov.IsValidZoomIndex(new_zoom_index_x) &&
                zsprov.IsValidZoomIndex(new_zoom_index_y))
            {
                double new_zoom_factor_x = zsprov.
                    GetZoomMultiplier(new_zoom_index_x);
                double new_zoom_factor_y = zsprov.
                    GetZoomMultiplier(new_zoom_index_y);
                Zoom(evt.GetPosition(), new_zoom_factor_x, new_zoom_factor_y);
                prefs_.zoomFactorXindex = new_zoom_index_x;
                prefs_.zoomFactorYindex = new_zoom_index_y;
                RefreshAll();
            }
        }
    });

    return true;
}

Simulator* SimulationPanel::Impl::GetSimulator()
{
    return simulator_;
}

void SimulationPanel::Impl::SetSimulator(Simulator* simulator)
{
    if (simulator != nullptr)
    {
        simulation_ = simulator->GetSimulation();
        simulator_ = simulator;
    }
    else
    {
        simulator_ = nullptr;
        simulation_ = nullptr;
    }
}

void SimulationPanel::Impl::ResetPrefsToDefault()
{
    original_prefs_ = SimulationPanelPrefs();
    prefs_ = SimulationPanelPrefs();
}

void SimulationPanel::Impl::ResetPrefs()
{
    prefs_ = original_prefs_;
}

void SimulationPanel::Impl::SetPrefs(const SimulationPanelPrefs& prefs)
{
    original_prefs_ = prefs;
    prefs_ = prefs;
}

SimulationPanelPrefs SimulationPanel::Impl::GetOriginalPrefs() const
{
    return original_prefs_;
}

SimulationPanelPrefs SimulationPanel::Impl::GetPrefs() const
{
    return prefs_;
}

SimulationPanelPrefs & SimulationPanel::Impl::Prefs()
{
    return prefs_;
}

void SimulationPanel::Impl::RefreshPolygon(Polygon *poly, bool lock_simulation)
{
    if (!lock_simulation)
    {
        DoRefreshPolygon(poly);
        return ;
    }

    if (simulator_ == nullptr) return ;
    if (simulator_->IsRunning())
    {
        if (simulator_->LockSimulation(50))
        {
            DoRefreshPolygon(poly);
            simulator_->UnlockSimulation();
        }
        else
        {
        }
    }
    else
    {
        DoRefreshPolygon(poly);
    }
}

void SimulationPanel::Impl::RefreshPolygons(bool lock_simulation)
{
#define REFRESH_CODE \
    DoClearCache(); \
    simulation_->IteratePolygons([this](Polygon *obj) -> bool { \
        DoRefreshPolygon(obj); \
        return true; \
            });

    if (!lock_simulation)
    {
        REFRESH_CODE
        return;
    }

    if (simulator_ == nullptr) return ;
    if (simulator_->IsRunning())
    {
        if (simulator_->LockSimulation(50))
        {
            REFRESH_CODE
            simulator_->UnlockSimulation();
        }
        else
        {
        }
    }
    else
    {
        REFRESH_CODE
    }
#undef REFRESH_CODE
}

void SimulationPanel::Impl::RefreshAll(bool lock_simulation)
{
    RefreshMatrices();
    RefreshPolygons(lock_simulation);
    panel_->Refresh();
}

void SimulationPanel::Impl::RefreshMatrices()
{

    auto &zsprov = ZoomScaleProvider();
    zoomFactorX_ = zsprov.GetZoomMultiplier(prefs_.zoomFactorXindex);
    zoomFactorY_ = zsprov.GetZoomMultiplier(prefs_.zoomFactorYindex);

    // find the screen origin
    Vector screenOrigin;
    int wi, he;
    panel_->GetSize(&wi, &he);

    switch (prefs_.screenOrigin)
    {
        case SimulationPanelPrefs::ORIGIN_TOP_LEFT:
            screenOrigin.x = 0.0; screenOrigin.y = 0.0;
            break;
        case SimulationPanelPrefs::ORIGIN_TOP_CENTER:
            screenOrigin.x = wi / 2.0; screenOrigin.y = 0.0;
            break;
        case SimulationPanelPrefs::ORIGIN_TOP_RIGHT:
            screenOrigin.x = wi - 1.0; screenOrigin.y = 0.0;
            break;
        case SimulationPanelPrefs::ORIGIN_CENTER_LEFT:
            screenOrigin.x = 0.0; screenOrigin.y = he / 2.0;
            break;
        case SimulationPanelPrefs::ORIGIN_CENTER_RIGHT:
            screenOrigin.x = wi - 1.0; screenOrigin.y = he / 2.0;
            break;
        case SimulationPanelPrefs::ORIGIN_BOTTOM_LEFT:
            screenOrigin.x = 0.0; screenOrigin.y = he - 1.0;
            break;
        case SimulationPanelPrefs::ORIGIN_BOTTOM_CENTER:
            screenOrigin.x = wi / 2.0; screenOrigin.y = he - 1.0;
            break;
        case SimulationPanelPrefs::ORIGIN_BOTTOM_RIGHT:
            screenOrigin.x = wi - 1.0; screenOrigin.y = he - 1.0;
            break;
        case SimulationPanelPrefs::ORIGIN_CENTER:
        default:
            screenOrigin.x = wi / 2.0; screenOrigin.y = he / 2.0;
            break;
    }
    screenOrigin += prefs_.screenOriginOffset;

    // transformation maps origin to screenOrigin

    double translate_x = 0.0, translate_y = 0.0;

    translate_x = screenOrigin.x - prefs_.scaleX * zoomFactorX_ * prefs_.origin.x;
    translate_y = screenOrigin.y + prefs_.scaleY * zoomFactorY_ * prefs_.origin.y;

    transformation_ <<
        prefs_.scaleX * zoomFactorX_, 0, translate_x,
        0, -prefs_.scaleY * zoomFactorY_, translate_y,
        0, 0, 1
    ;
    inverse_transformation_ = transformation_.inverse();
}

void SimulationPanel::Impl::Zoom(
    const Vector& world_anchor,
    const Vector& screen_anchor,
    double new_zoom_x,
    double new_zoom_y)
{
    zoomFactorX_ = new_zoom_x;
    zoomFactorY_ = new_zoom_y;
    prefs_.screenOriginOffset.x =
        prefs_.scaleX * zoomFactorX_ * (prefs_.origin.x - world_anchor.x)
        + screen_anchor.x;
    prefs_.screenOriginOffset.y =
        prefs_.scaleY * zoomFactorY_ * (- prefs_.origin.y + world_anchor.y)
        + screen_anchor.y;

    Vector screenRawOrigin;
    int wi, he;
    panel_->GetSize(&wi, &he);
    
    switch (prefs_.screenOrigin)
    {
        case SimulationPanelPrefs::ORIGIN_TOP_LEFT:
            screenRawOrigin.x = 0.0; screenRawOrigin.y = 0.0;
            break;
        case SimulationPanelPrefs::ORIGIN_TOP_CENTER:
            screenRawOrigin.x = wi / 2.0; screenRawOrigin.y = 0.0;
            break;
        case SimulationPanelPrefs::ORIGIN_TOP_RIGHT:
            screenRawOrigin.x = wi - 1.0; screenRawOrigin.y = 0.0;
            break;
        case SimulationPanelPrefs::ORIGIN_CENTER_LEFT:
            screenRawOrigin.x = 0.0; screenRawOrigin.y = he / 2.0;
            break;
        case SimulationPanelPrefs::ORIGIN_CENTER_RIGHT:
            screenRawOrigin.x = wi - 1.0; screenRawOrigin.y = he / 2.0;
            break;
        case SimulationPanelPrefs::ORIGIN_BOTTOM_LEFT:
            screenRawOrigin.x = 0.0; screenRawOrigin.y = he - 1.0;
            break;
        case SimulationPanelPrefs::ORIGIN_BOTTOM_CENTER:
            screenRawOrigin.x = wi / 2.0; screenRawOrigin.y = he - 1.0;
            break;
        case SimulationPanelPrefs::ORIGIN_BOTTOM_RIGHT:
            screenRawOrigin.x = wi - 1.0; screenRawOrigin.y = he - 1.0;
            break;
        case SimulationPanelPrefs::ORIGIN_CENTER:
        default:
            screenRawOrigin.x = wi / 2.0; screenRawOrigin.y = he / 2.0;
            break;
    }

    prefs_.screenOriginOffset -= screenRawOrigin;
}

void SimulationPanel::Impl::Zoom(
    const wxPoint& screen_anchor_,
    double new_zoom_x,
    double new_zoom_y)
{
    auto world_anchor = ConvS2W(screen_anchor_);
    auto screen_anchor = AsVector(screen_anchor_);

    Zoom(world_anchor, screen_anchor, new_zoom_x, new_zoom_y);
}

void SimulationPanel::Impl::Zoom(
    double new_zoom_x,
    double new_zoom_y)
{
    zoomFactorX_ = new_zoom_x;
    zoomFactorY_ = new_zoom_y;
}

void SimulationPanel::Impl::AddOnPaintHook(SimulationPanel::OnPaintHook* hook)
{
    assert(hook != nullptr);
    on_paint_hooks_.back() = hook;
    on_paint_hooks_.push_back(nullptr);
    std::sort(
        on_paint_hooks_.begin(),
        on_paint_hooks_.end() - 1 /* exclude nullptr */,
        [](
            SimulationPanel::OnPaintHook *hook1,
            SimulationPanel::OnPaintHook *hook2) {
            return hook1->z_order < hook2->z_order;
        }
    );
}

SimulationPanel::OnPaintHook * SimulationPanel::Impl::DetachOnPaintHook(
    SimulationPanel::OnPaintHook* hook)
{
    assert(hook != nullptr);
    auto it = std::find(on_paint_hooks_.begin(), on_paint_hooks_.end(), hook);
    if (it != on_paint_hooks_.end())
    {
        on_paint_hooks_.erase(it);
    }
    return hook;
}

void SimulationPanel::Impl::DetachAllOnPaintHooks(
    SimulationPanel::OnPaintHook *** hooks)
{
    assert(hooks != nullptr);
    *hooks = new SimulationPanel::OnPaintHook *[on_paint_hooks_.size()];
    std::size_t i = 0;
    for (auto &o: on_paint_hooks_)
    {
        (*hooks)[i] = o;
        i++;
    }
    on_paint_hooks_.clear();
    on_paint_hooks_.push_back(nullptr);
}

void SimulationPanel::Impl::DeleteAllOnPaintHooks()
{
    for (auto &o: on_paint_hooks_)
        delete o;
    on_paint_hooks_.clear();
    on_paint_hooks_.push_back(nullptr);
}

SimulationPanel::OnPaintHook * const * SimulationPanel::Impl::GetOnPaintHooks() const
{
    return &on_paint_hooks_[0];
}

SimulationPanel::ZoomScaleProvider *
    SimulationPanel::Impl::GetZoomScaleProvider()
{
    if (zoomScaleProvider_)
        return zoomScaleProvider_;
    return &defaultZoomScaleProvider_;
}

SimulationPanel::ZoomScaleProvider *
    SimulationPanel::Impl::SetZoomScaleProvider(
        SimulationPanel::ZoomScaleProvider* provider)
{
    auto old = zoomScaleProvider_;
    zoomScaleProvider_ = provider;
    return old;
}

SimulationPanel::ZoomScaleProvider *
    SimulationPanel::Impl::DetachZoomScaleProvider()
{
    return SetZoomScaleProvider(nullptr);
}

Vector SimulationPanel::Impl::TransformW2S(const Vector& v) const
{
    Eigen::Vector3d in, out;
    in << v.x, v.y, 1.0;
    out = transformation_ * in;
    return Vector{out(0), out(1)};
}

SimulationPanel::ZoomScaleProvider & SimulationPanel::Impl::ZoomScaleProvider()
{
    if (zoomScaleProvider_)
        return *zoomScaleProvider_;
    return defaultZoomScaleProvider_;
}

Vector SimulationPanel::Impl::TransformS2W(const Vector& v) const
{
    Eigen::Vector3d in, out;
    in << v.x, v.y, 1.0;
    out = inverse_transformation_ * in;
    return Vector{out(0), out(1)};
}

wxPoint SimulationPanel::Impl::ConvW2S(const Vector& p)
{
    Eigen::Vector3d in, out;
    in << p.x, p.y, 1.0;
    out = transformation_ * in;
    return wxPoint(out(0), out(1));
}

Vector SimulationPanel::Impl::ConvS2W(const wxPoint& p)
{
    Eigen::Vector3d in, out;
    in << (double) p.x, (double) p.y, 1.0;
    out = inverse_transformation_ * in;
    return Vector(out(0), out(1));
}

SimulationPanel::Impl::~Impl()
{
    DoClearCache();
    DeleteAllOnPaintHooks();
    if (zoomScaleProvider_) delete zoomScaleProvider_;
}

void SimulationPanel::Impl::DrawPolygon(wxDC& dc, PolygonData& data)
{
    if (data.borderwi == 0)
    {
        dc.SetPen(*wxTRANSPARENT_PEN);
    }
    else
    {
        dc.SetPen(wxPen(data.fgclr, data.borderwi));
    }
    dc.SetBrush(wxBrush(data.bgclr));
    dc.DrawPolygon(data.pts.size(), &(data.pts[0]));
}

void SimulationPanel::Impl::DrawPolygon(wxDC& dc, Polygon* obj)
{
    assert(obj != nullptr);
    DrawPolygon(dc, *polygon_cache_[obj]);
}

void SimulationPanel::Impl::DoRefreshPolygon(Polygon* obj)
{
    assert(obj != nullptr);
    auto data = [&] {
        auto it = polygon_cache_.find(obj);
        if (it == polygon_cache_.end())
        {
            return polygon_cache_[obj] = new PolygonData;
        }
        else
        {
            return it->second;
        }
    }();
    data->pts.clear();
    data->pts.reserve(obj->GetVectorCount());
    obj->IterateVectors([&data, obj, this](Vector *p) -> bool {
        data->pts.push_back(ConvW2S(
            p->rotated(obj->rotation) + obj->position));
        return true;
    });
    data->bgclr = AswxColour(obj->color);
    data->fgclr = AswxColour(obj->border_color);
    data->borderwi = obj->border;
    data->shown = obj->shown;
    data->z_order = obj->z_order;
    ReorderPolygonCache();
}

void SimulationPanel::Impl::ReorderPolygonCache()
{
    polygon_cache_ordered_.clear();
    polygon_cache_ordered_.reserve(polygon_cache_.size());
    for (auto &o: polygon_cache_)
    {
        polygon_cache_ordered_.push_back(o.second);
    }
    std::sort(
        polygon_cache_ordered_.begin(),
        polygon_cache_ordered_.end(),
        [](PolygonData * const & poly1, PolygonData * const & poly2) {
            return poly1->z_order < poly2->z_order;
        });
}

void SimulationPanel::Impl::DoClearCache()
{
    for (auto &o: polygon_cache_)
    {
        delete o.second;
    }
    polygon_cache_.clear();
    polygon_cache_ordered_.clear();
}

//BEGIN SimulationPanel Impl

SimulationPanel::SimulationPanel()
{
}

SimulationPanel::SimulationPanel(
    wxWindow* parent,
    wxWindowID winid,
    const wxPoint& position,
    const wxSize& size,
    const wxString& name)
{
    SimulationPanel::Create(parent, winid, position, size, name);
}

bool SimulationPanel::Create(
    wxWindow* parent,
    wxWindowID winid,
    const wxPoint& position,
    const wxSize& size,
    const wxString& name)
{
    impl_ = std::make_unique<Impl>(this);
    return impl_->Create(parent, winid, position, size, name);
}

// forwarding funcs

Simulator* SimulationPanel::GetSimulator()
{
    assert(impl_);
    return impl_->GetSimulator();
}

void SimulationPanel::SetSimulator(Simulator* simulator)
{
    assert(impl_);
    impl_->SetSimulator(simulator);
}

void SimulationPanel::ResetPrefsToDefault()
{
    assert(impl_);
    impl_->ResetPrefsToDefault();
}

void SimulationPanel::ResetPrefs()
{
    assert(impl_);
    impl_->ResetPrefs();
}

void SimulationPanel::SetPrefs(const SimulationPanelPrefs& prefs)
{
    assert(impl_);
    impl_->SetPrefs(prefs);
}

SimulationPanelPrefs SimulationPanel::GetOriginalPrefs() const
{
    assert(impl_);
    return impl_->GetOriginalPrefs();
}

SimulationPanelPrefs SimulationPanel::GetPrefs() const
{
    assert(impl_);
    return impl_->GetPrefs();
}

SimulationPanelPrefs & SimulationPanel::Prefs()
{
    assert(impl_);
    return impl_->Prefs();
}

void SimulationPanel::RefreshPolygon(Polygon* obj, bool lock_simulation)
{
    assert(impl_);
    impl_->RefreshPolygon(obj, lock_simulation);
}

void SimulationPanel::RefreshPolygons(bool lock_simulation)
{
    assert(impl_);
    impl_->RefreshPolygons(lock_simulation);
}

void SimulationPanel::RefreshMatrices()
{
    assert(impl_);
    impl_->RefreshMatrices();
}

void SimulationPanel::RefreshAll(bool lock_simulation)
{
    assert(impl_);
    impl_->RefreshAll(lock_simulation);
}

void SimulationPanel::AddOnPaintHook(SimulationPanel::OnPaintHook* hook)
{
    assert(impl_);
    impl_->AddOnPaintHook(hook);
}

SimulationPanel::OnPaintHook * SimulationPanel::DetachOnPaintHook(
    SimulationPanel::OnPaintHook* hook)
{
    assert(impl_);
    return impl_->DetachOnPaintHook(hook);
}

void SimulationPanel::DetachAllOnPaintHooks(
    SimulationPanel::OnPaintHook *** hooks)
{
    assert(impl_);
    impl_->DetachAllOnPaintHooks(hooks);
}

void SimulationPanel::DeleteAllOnPaintHooks()
{
    assert(impl_);
    impl_->DeleteAllOnPaintHooks();
}

SimulationPanel::OnPaintHook * const * SimulationPanel::GetOnPaintHooks() const
{
    assert(impl_);
    return impl_->GetOnPaintHooks();
}

SimulationPanel::ZoomScaleProvider * SimulationPanel::GetZoomScaleProvider()
{
    assert(impl_);
    return impl_->GetZoomScaleProvider();
}

SimulationPanel::ZoomScaleProvider * SimulationPanel::SetZoomScaleProvider(
    SimulationPanel::ZoomScaleProvider* provider)
{
    assert(impl_);
    return impl_->SetZoomScaleProvider(provider);
}

SimulationPanel::ZoomScaleProvider * SimulationPanel::DetachZoomScaleProvider()
{
    assert(impl_);
    return impl_->DetachZoomScaleProvider();
}

Vector SimulationPanel::TransformW2S(const Vector& v) const
{
    assert(impl_);
    return impl_->TransformW2S(v);
}

Vector SimulationPanel::TransformS2W(const Vector& v) const
{
    assert(impl_);
    return impl_->TransformS2W(v);
}

SimulationPanel::~SimulationPanel()
{
}

// some other stuff

void SimulationPanel::OnPaintHook::OnPaint(wxDC& UNUSED(dc))
{
}

SimulationPanel::OnPaintHook::~OnPaintHook()
{
}

double SimulationPanel::ZoomScaleProvider::GetZoomMultiplier(
    int UNUSED(zoom_index))
{
    return 1;
}

bool SimulationPanel::ZoomScaleProvider::IsValidZoomIndex(
    int UNUSED(zoom_index))
{
    return true;
}

SimulationPanel::ZoomScaleProvider::~ZoomScaleProvider()
{
}

//END

}
