#ifndef _SIMULATIONPANEL_HPP_INCLUDED
#define _SIMULATIONPANEL_HPP_INCLUDED

#include <wx/panel.h>
#include <wx/dc.h>
#include <memory>

#include "Simulation.hpp"
#include "Simulator.hpp"

namespace masosi {

class SimulationPanelPrefs
{
public:

    enum EScreenOrigin {
        ORIGIN_CENTER = 0,
        ORIGIN_TOP_LEFT,
        ORIGIN_TOP_CENTER,
        ORIGIN_TOP_RIGHT,
        ORIGIN_CENTER_LEFT,
        ORIGIN_CENTER_RIGHT,
        ORIGIN_BOTTOM_LEFT,
        ORIGIN_BOTTOM_CENTER,
        ORIGIN_BOTTOM_RIGHT
    } screenOrigin { ORIGIN_CENTER };

    Vector screenOriginOffset { 0.0, 0.0 };

    double scaleX { 100.0 }, scaleY { 100.0 };

    int zoomFactorXindex {0}, zoomFactorYindex {0};

    Vector origin { 0.0, 0.0 };

};

class SimulationPanel : public wxPanel
{

public:

    SimulationPanel();

    SimulationPanel(
        wxWindow *parent,
        wxWindowID winid = wxID_ANY,
        const wxPoint &position = wxDefaultPosition,
        const wxSize &size = wxDefaultSize,
        const wxString &name = wxT("SimulationPanel"));

    bool Create(
        wxWindow *parent,
        wxWindowID winid = wxID_ANY,
        const wxPoint &position = wxDefaultPosition,
        const wxSize &size = wxDefaultSize,
        const wxString &name = wxT("SimulationPanel"));

    Simulator *GetSimulator();
    // You need to call RefreshAll() explicitly
    void SetSimulator(Simulator *simulator);

    void ResetPrefsToDefault();
    void ResetPrefs();

    void SetPrefs(const SimulationPanelPrefs &prefs);

    SimulationPanelPrefs GetOriginalPrefs() const;
    SimulationPanelPrefs GetPrefs() const;
    SimulationPanelPrefs &Prefs();

    // you need call Refresh() explicitly
    void RefreshPolygon(Polygon *obj, bool lock_simulation = true);

    // you need call Refresh() explicitly
    void RefreshPolygons(bool lock_simulation = true);

    void RefreshMatrices();

    // you do not need to call Refresh() explicitly
    void RefreshAll(bool lock_simulation = true);

    //BEGIN OnPaintHook

    class OnPaintHook
    {
    public:
        int z_order = {-1};
        std::string hook_name;
        virtual void OnPaint(wxDC &dc);
        virtual ~OnPaintHook();
    };

    void AddOnPaintHook(OnPaintHook *hook);
    OnPaintHook *DetachOnPaintHook(OnPaintHook *hook);
    // delete the output with delete [] .
    void DetachAllOnPaintHooks(OnPaintHook ***hooks);
    void DeleteAllOnPaintHooks();

    // returns a NULL terminated array of `OnPaintHook*`s.
    OnPaintHook * const * GetOnPaintHooks() const;

    //END

    //BEGIN ZoomScaleProvider

    class ZoomScaleProvider
    {
    public:
        virtual double GetZoomMultiplier(int zoom_index);
        virtual bool IsValidZoomIndex(int zoom_index);
        virtual ~ZoomScaleProvider();
    };

    ZoomScaleProvider *GetZoomScaleProvider();
    ZoomScaleProvider *SetZoomScaleProvider(ZoomScaleProvider *provider);
    ZoomScaleProvider *DetachZoomScaleProvider();

    //END

    Vector TransformW2S(const Vector &v) const;
    Vector TransformS2W(const Vector &v) const;

    virtual ~SimulationPanel();

private:

    class Impl;
    std::unique_ptr<Impl> impl_;

};

}

#endif // _SIMULATIONPANEL_HPP_INCLUDED
