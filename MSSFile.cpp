#include "MSSFile.hpp"

#include <utility>
#include <sstream>
#include <boost/filesystem/fstream.hpp>

namespace masosi {

MSSFile MSSFile::Load(const std::string& path)
{
    return Load(boost::filesystem::path(path));
}

MSSFile MSSFile::Load(const boost::filesystem::path& path)
{
    MSSFile result;
    boost::filesystem::ifstream ifs(path, boost::filesystem::ifstream::binary);
    if (ifs)
    {
        ifs.seekg(0, ifs.end);
        std::size_t fsize = ifs.tellg();
        ifs.seekg(0, ifs.beg);

        std::vector<char> data;
        data.resize(fsize);

        ifs.read(&data[0], fsize);

        if (ifs)
        {
            // we are fine!
            ifs.close();
        }
        else
        {
            ifs.close();
            throw MSSFile::Exception("Couldn't read the file!");
        }

        // check the magic number
        if (data.size() >= 6 &&
            data[0] == 'M' &&
            data[1] == 'a' &&
            data[2] == 'S' &&
            data[3] == 'o' &&
            data[4] == 'S' &&
            data[5] == 'i')
        {
            // we are fine
        }
        else
        {
            throw MSSFile::Exception("Malformed file!");
        }

        auto it = data.begin() + 6; // beginning of the data
        auto end = data.end();
        auto current_script = (std::size_t) 0;
        while (it != end && current_script < Simulator::SCRIPT_COUNT)
        {
            std::stringstream ss;
            ss.imbue(std::locale("C")); // imbue default C locale

            while (it != end && *it != ' ')  // read the size of the script
            {
                if (*it <= '9' && *it >= '0')
                {
                    // correct digit
                }
                else
                {
                    throw MSSFile::Exception("Malformed size specifier!");
                }
                ss << *it;
                ++it;
            }
            ++it; // skip the WS

            std::size_t script_size = 0;
            ss >> script_size; // extract the size

            if ((end - it) >= script_size) // if we have enough data
            {
                result.scripts_[current_script] =
                    std::string(it.base(), script_size);
                it += script_size;
                current_script++;
            }
            else
            {
                throw MSSFile::Exception("Not enough data on file!");
            }
        }
    }
    else
    {
        throw MSSFile::Exception("Couldn't open file!");
    }
    return result;
}

void MSSFile::Save(const std::string& path)
{
    Save((boost::filesystem::path) path);
}

void MSSFile::Save(const boost::filesystem::path& path)
{
    boost::filesystem::ofstream ofs(path, boost::filesystem::ifstream::binary);
    if (ofs)
    {
        ofs << "MaSoSi";
        for (auto i = 0; i < Simulator::SCRIPT_COUNT; ++i)
        {
            ofs << scripts_[i].size() << " " << scripts_[i];
        }
        ofs.close();
    }
    else
    {
        throw MSSFile::Exception("Couldn't open file!");
    }
}

std::string MSSFile::GetScript(Simulator::EScriptType type) const
{
    if (type >= Simulator::SCRIPT_COUNT) return "";
    return scripts_[type];
}

void MSSFile::SetScript(
    Simulator::EScriptType type,
    const std::string& script)
{
    if (type >= Simulator::SCRIPT_COUNT) return ;
    scripts_[type] = script;
}

void MSSFile::TransmitToSimulator(Simulator& simulator)
{
    if (simulator.IsRunning())
        throw MSSFile::Exception("Already running simulation!");
    for (int i = 0; i < Simulator::SCRIPT_COUNT; ++i)
    {
        simulator.SetScript((Simulator::EScriptType) i, scripts_[i]);
    }
}

void MSSFile::TransmitFromSimulator(Simulator& simulator)
{
    for (int i = 0; i < Simulator::SCRIPT_COUNT; ++i)
    {
        scripts_[i] = simulator.GetScript((Simulator::EScriptType) i);
    }
}

}
