#ifndef _REGISTERWXWIDGETS_HPP_INCLUDED
#define _REGISTERWXWIDGETS_HPP_INCLUDED

struct lua_State;

namespace masosi {

void RegisterWxFont(lua_State *L);
void RegisterWxColour(lua_State *L);
void RegisterWxPen(lua_State *L);
void RegisterWxBrush(lua_State *L);
void RegisterWxPoint(lua_State *L);
void RegisterWxSize(lua_State *L);
void RegisterWxRect(lua_State *L);
void RegisterWxBitmap(lua_State *L);
void RegisterWxDC(lua_State *L);

void RegisterWxWidgets(lua_State *L);

}

#endif // _REGISTERWXWIDGETS_HPP_INCLUDED
