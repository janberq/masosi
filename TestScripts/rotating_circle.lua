
_SIM:clear_polygons()

circle = _SIM:new_polygon()

r = 0.6

R = 2

circle:begin_points()
for th=0,2*math.pi,math.pi/32 do
    circle:add_point(r * math.cos(th), r * math.sin(th))
end
circle:end_points()

th = 0
omega = 1
T = 10

while true
do

    circle.position.x = R * math.cos(th)
    circle.position.y = R * math.sin(th)

    refresh()

    th = th + omega * T / 1000

    sleep(T)

end
 
