
_SIM:clear_polygons()

poly = _SIM:new_polygon()

poly:begin_points()
    poly:add_point(-1, 1)
    poly:add_point(-1, -1)
    poly:add_point(1, -1)
    poly:add_point(1, 1)
poly:end_points()

refresh()

v = Point:new(1, 1)

while true
do
    poly.position = poly.position + v * 0.01
    sleep(10)
    refresh()
end
