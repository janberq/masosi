-- DEFINITIONS

a = 0.1    -- side of robot
v = 0.75   -- velocity of the robot
T = 10     -- interval between simulation steps
d = 1.0    -- distance to be taken

_SIM:clear_polygons()

polyRobot = _SIM:new_polygon()

polyRobot:begin_points()
    polyRobot:add_point(-a/2, -a/2)
    polyRobot:add_point(-a/2, a/2)
    polyRobot:add_point(a/2, a/2)
    polyRobot:add_point(a/2, -a/2)
polyRobot:end_points()

obstacleColor = Color.new(0, 100, 150)

polyA = _SIM:new_polygon()
polyA:begin_points()
    polyA:add_point(0.0, -1.0)
    polyA:add_point(0.0, 1.0)
    polyA:add_point(-0.1, 1.0)
    polyA:add_point(-0.1, -1.0)
polyA:end_points()

polyA.color = obstacleColor
polyA.border_color = obstacleColor
polyA.position.x = -a/2

polyB = _SIM:new_polygon()
polyB:begin_points()
    polyB:add_point(0.0, -1.0)
    polyB:add_point(0.0, 1.0)
    polyB:add_point(0.1, 1.0)
    polyB:add_point(0.1, -1.0)
polyB:end_points()

polyB.position.x = a/2 + d
polyB.color = obstacleColor
polyB.border_color = obstacleColor

refresh()

function move(x)
    local remaining = x
    local vel = Vector.new(
       math.cos(polyRobot.rotation),
       math.sin(polyRobot.rotation)) * v
    local cont = true
    while cont
    do
        local distance = vel:norm() * T / 1000
        if remaining - distance > 0
        then
            sleep(T)
            polyRobot.position = polyRobot.position + vel * T / 1000
            remaining = remaining - distance
        else
            local t = remaining/vel:norm() * 1000
            sleep(math.floor(t))
            polyRobot.position = polyRobot.position + vel * t / 1000
            cont = false
        end
        refresh()
    end
end

sleep(500)
move(d)
 
