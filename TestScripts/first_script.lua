-- create an obstacle

function in_deg(x)
    return x * math.pi / 180.0
end

obst1 = simenv:NewObstacle()

side_length = 2.5

obst1:AddPoint(-side_length/2.0, -side_length/2.0)
obst1:AddPoint(-side_length/2.0, side_length/2.0)
obst1:AddPoint(side_length/2.0, side_length/2.0)
obst1:AddPoint(side_length/2.0, -side_length/2.0)

obst1.rotation = in_deg(45.0)
obst1.position = Point.new(0.0, 0.0)

obst1.color = Color.new(100, 0, 0)
obst1.border_color = Color.new(50, 0, 0)
