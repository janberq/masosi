function random_color()
    return Color.new(
        math.random(0, 255),
        math.random(0, 255),
        math.random(0, 255)
    )
end

if execution_count == nil then

    math.randomseed(os.time())

    execution_count = 1

    -- initialization code here

    obst = simenv:NewObstacle()
    obst:BeginPoints()
        obst:AddPoint(-1, -1)
        obst:AddPoint(-1, 1)
        obst:AddPoint(1, 1)
        obst:AddPoint(1, -1)
    obst:EndPoints()

    obst.color = random_color()

else

    obst.color = random_color()
    execution_count = execution_count + 1

end
 
