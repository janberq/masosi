
r = 0.6
R = 2
th = nil
omega = 1
T = 1
oh = math.sqrt(R*R-r*r) -- obstacle
ow = 0.1
opos = Vector.new(0, oh/2)

while not th
do
    local th_ = tonumber(get_text("Please enter an angle in degrees:"))
    if th_ then th = th_ * math.pi / 180.0 end
end

_SIM:clear_polygons()

circle = _SIM:new_polygon()

circle:begin_points()
for th=0,2*math.pi,math.pi/32 do
    circle:add_point(r * math.cos(th), r * math.sin(th))
end
circle:end_points()
circle.name = "circle"

obstacle = _SIM:new_polygon()
obstacle.position = opos
obstacle:begin_points()
    obstacle:add_point(-ow/2, -oh/2)
    obstacle:add_point(-ow/2, oh/2)
    obstacle:add_point(ow/2, oh/2)
    obstacle:add_point(ow/2, -oh/2)
obstacle:end_points()
obstacle.color.r = 255

cont = true
while cont
do

    circle.position.x = R * math.cos(th)
    circle.position.y = R * math.sin(th)

    refresh()

    _SIM:collision_check(circle, function (p)
        if p == circle -- this is possible
        then
            return true
        else
            cont = false
            raw_output("collision!")
            return false -- one collision is enough
        end
    end)

    th = th + omega * T / 1000
    sleep(T)

end 
