function draw_circle(
    radius,
    count,
    wi_relative,
    he_relative,
    color,
    border_color,
    border_wi)

    local origin = Point.new(0, 0)

    -- start calculating

    local step = 2 * math.pi / count

    local he_max = 2.0 * radius * math.sin(step / 2.0)
    local wi_max = he_max * (1.0 - he_relative) / math.tan(step / 2.0)

    local wi = wi_max * wi_relative
    local he = he_max * he_relative

    -- start creating obstacles

    local template_obstacle = Obstacle.new() -- create the template_obstacle object
    template_obstacle:BeginPoints()
        template_obstacle:AddPoint(-wi/2, -he/2) -- add the points to it
        template_obstacle:AddPoint(-wi/2, he/2)
        template_obstacle:AddPoint(wi/2, he/2)
        template_obstacle:AddPoint(wi/2, -he/2)
    template_obstacle:EndPoints()
    template_obstacle.border = border_wi -- set various properties
    template_obstacle.color = color
    template_obstacle.border_color = border_color


    for i = 1, count do
        local obstacle = simenv:NewObstacle() -- create the obstacle object
        obstacle:CopyFrom(template_obstacle)
        local angle = (i-1.0) * step + step / 2.0
        obstacle.position = Point.new(
            math.cos(angle) * radius,
            math.sin(angle) * radius) + origin
        obstacle.rotation = angle
    end

end

draw_circle(
    3,
    12,
    0.5,
    0.9,
    Color.new(150, 0, 0),
    Color.new(100, 0, 0),
	2
)
