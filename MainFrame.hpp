#ifndef _MAINFRAME_HPP_INCLUDED
#define _MAINFRAME_HPP_INCLUDED

#include <wx/frame.h>
#include <memory>

namespace masosi {

class MainFrame : public wxFrame
{
public:

    MainFrame();

    MainFrame(wxWindow *parent, wxWindowID id);
    bool Create(wxWindow *parent, wxWindowID id);

    virtual ~MainFrame();

private:

    struct Impl;
    std::unique_ptr<Impl> impl_;
};

}

#endif // _MAINFRAME_HPP_INCLUDED
