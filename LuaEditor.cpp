#include "LuaEditor.hpp"
#include "RegisterWxWidgets.hpp"

#include <algorithm>

/**
 * Implement wxString for Sol2, thanks to ThePhD
 */

#include <sol.hpp>
namespace sol { namespace stack {
     template <>
     struct pusher< ::wxString > {
          static int push ( lua_State* L, const ::wxString& str ) { 
               // same as before
           }
     };

     template <>
     struct getter< ::wxString > {
          static ::wxString get ( lua_State* L, int index, record& tracking ) {
               tracking.use(1); // THIS IS THE ONLY BIT THAT CHANGES
               const char* luastr = stack::get<const char*>(L, index);
               // same as before
          }
     };
} // stack

template <>
struct lua_type_of< ::wxString > : std::integral_constant<type, type::string> {};
}


namespace masosi {

LuaEditorPrefs::Style::Style(
            int index_,
            const wxColour &fg_,
            const wxColour &bg_,
            const wxFont &font_) :
            index(index_),
            foreground(fg_),
            background(bg_),
            font(font_)
{
}

LuaEditorPrefs::Style::Style()
{
    index = -1;
    foreground = wxNullColour;
    background = wxNullColour;
    font = wxNullFont;
}

LuaEditorPrefs::LuaEditorPrefs()
{
    Defaults();
}

LuaEditorPrefs::LuaEditorPrefs(const std::string& script)
{
    LoadScript(script);
}

void LuaEditorPrefs::LoadScript(const std::string& script)
{
    sol::state lua;
    lua.open_libraries(
        sol::lib::base,
        sol::lib::io,
        sol::lib::math,
        sol::lib::string,
        sol::lib::table);

    RegisterWxColour(lua);
    RegisterWxFont(lua);
    RegisterToLua(lua);

    lua["prefs"] = this;

    lua.script(script);
}

void LuaEditorPrefs::RegisterToLua(lua_State* L)
{
    sol::state_view lua{L};
    lua.new_usertype<LuaEditorPrefs::Style>(
        "Style",
        sol::constructors<
            sol::types<>,
            sol::types<int, const wxColour &, const wxColour &, const wxFont &>
        >(),
        "index", &LuaEditorPrefs::Style::index,
        "foreground", &LuaEditorPrefs::Style::foreground,
        "background", &LuaEditorPrefs::Style::background,
        "font", &LuaEditorPrefs::Style::font
    );

    lua.new_usertype<LuaEditorPrefs>(
        "LuaEditorPrefs",
        "words1", &LuaEditorPrefs::words1,
        "words2", &LuaEditorPrefs::words2,
        "words3", &LuaEditorPrefs::words3,
        "defaultFont", &LuaEditorPrefs::defaultFont,
        "wrapMode", &LuaEditorPrefs::wrapMode,
        "lineNumWidth", &LuaEditorPrefs::lineNumWidth,
        "lineNumForeground", &LuaEditorPrefs::lineNumForeground,
        "lineNumBackground", &LuaEditorPrefs::lineNumBackground,
        "lineNumFont", &LuaEditorPrefs::lineNumFont,
        "indentationGuides", &LuaEditorPrefs::indentationGuides,
        "indentationGuidesColor", &LuaEditorPrefs::indentationGuidesColor,
        "tabWidth", &LuaEditorPrefs::tabWidth,
        "indentWidth", &LuaEditorPrefs::indentWidth,
        "tabIndents", &LuaEditorPrefs::tabIndents,
        "useTabs", &LuaEditorPrefs::useTabs,
        "edgeMode", &LuaEditorPrefs::edgeMode,
        "edgeColor", &LuaEditorPrefs::edgeColor,
        "edgeColumn", &LuaEditorPrefs::edgeColumn,
        "autoIndent", &LuaEditorPrefs::autoIndent,
        "addStyle", [](
            LuaEditorPrefs *me,
            const LuaEditorPrefs::Style &style)
        {
            auto it = std::find_if(me->styles.begin(), me->styles.end(),
                [&style](LuaEditorPrefs::Style &style_) {
                    if (style.index == style_.index)
                        return true;
                    return false;
                });
            if (it != me->styles.end()) // if we have such style, then alter it
            {
                *it = style;
            }
            else
            {
                me->styles.push_back(style);
            }
        },
        "deleteStyle", [](LuaEditorPrefs *me, int index)
        {
            auto it = std::find_if(me->styles.begin(), me->styles.end(),
                [index](LuaEditorPrefs::Style &style) {
                    if (index == style.index)
                        return true;
                    return false;
                });
            if (it != me->styles.end())
            {
                me->styles.erase(it);
            }
        },
        "clearStyles", [](LuaEditorPrefs *me)
        {
            me->styles.clear();
        },
        "getStyle", [](LuaEditorPrefs *me, int index) -> LuaEditorPrefs::Style *
        {
            auto it = std::find_if(me->styles.begin(), me->styles.end(),
                [index](LuaEditorPrefs::Style &style) {
                    if (index == style.index)
                        return true;
                    return false;
                });
            if (it != me->styles.end())
            {
                return &(*it);
            }
            return nullptr; // nil
        }
    );

#define REGISTER_WXSTC_ENUM(x) #x, wxSTC_ ## x
    lua.new_enum(
        "wxSTC",
        REGISTER_WXSTC_ENUM(WRAP_NONE),
        REGISTER_WXSTC_ENUM(WRAP_WORD),
        REGISTER_WXSTC_ENUM(WRAP_CHAR),
        REGISTER_WXSTC_ENUM(EDGE_NONE),
        REGISTER_WXSTC_ENUM(EDGE_LINE),
        REGISTER_WXSTC_ENUM(EDGE_BACKGROUND),
        REGISTER_WXSTC_ENUM(LUA_DEFAULT),
        REGISTER_WXSTC_ENUM(LUA_COMMENT),
        REGISTER_WXSTC_ENUM(LUA_COMMENTLINE),
        REGISTER_WXSTC_ENUM(LUA_COMMENTDOC),
        REGISTER_WXSTC_ENUM(LUA_NUMBER),
        REGISTER_WXSTC_ENUM(LUA_WORD),
        REGISTER_WXSTC_ENUM(LUA_STRING),
        REGISTER_WXSTC_ENUM(LUA_CHARACTER),
        REGISTER_WXSTC_ENUM(LUA_LITERALSTRING),
        REGISTER_WXSTC_ENUM(LUA_PREPROCESSOR),
        REGISTER_WXSTC_ENUM(LUA_OPERATOR),
        REGISTER_WXSTC_ENUM(LUA_IDENTIFIER),
        REGISTER_WXSTC_ENUM(LUA_STRINGEOL),
        REGISTER_WXSTC_ENUM(LUA_WORD2),
        REGISTER_WXSTC_ENUM(LUA_WORD3),
        REGISTER_WXSTC_ENUM(LUA_WORD4),
        REGISTER_WXSTC_ENUM(LUA_WORD5),
        REGISTER_WXSTC_ENUM(LUA_WORD6),
        REGISTER_WXSTC_ENUM(LUA_WORD7),
        REGISTER_WXSTC_ENUM(LUA_WORD8),
        REGISTER_WXSTC_ENUM(LUA_LABEL)
    );

#undef REGISTER_WXSTC_ENUM
}

void LuaEditorPrefs::Defaults()
{
    words1 = "and break do else elseif end false for function if in local nil not or repeat return then true until while";
    words2 = "";
    words3 = "";

    wxFont font_normal(
        12,
        wxFONTFAMILY_TELETYPE,
        wxFONTSTYLE_NORMAL,
        wxFONTWEIGHT_NORMAL);

    wxFont font_bold(
        12,
        wxFONTFAMILY_TELETYPE,
        wxFONTSTYLE_NORMAL,
        wxFONTWEIGHT_BOLD);

    styles.emplace_back(
        wxSTC_LUA_CHARACTER,
        wxColour(255, 0, 0),
        wxNullColour,
        font_normal);

    styles.emplace_back(
        wxSTC_LUA_COMMENT,
        wxColour(50, 120, 50),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_COMMENTDOC,
        wxColour(70, 150, 70),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_COMMENTLINE,
        wxColour(50, 100, 50),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_DEFAULT,
        wxColour(0, 0, 0),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_IDENTIFIER,
        wxColour(0, 50, 50),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_LABEL,
        wxColour(10, 20, 20),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_LITERALSTRING,
        wxColour(255, 0, 0),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_NUMBER,
        wxColour(100, 100, 0),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_OPERATOR,
        wxColour(0, 0, 100),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_PREPROCESSOR,
        wxColour(0, 100, 255),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_STRING,
        wxColour(255, 0, 0),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_STRINGEOL,
        wxColour(255, 0, 200),
        wxNullColour,
        font_normal
    );

    styles.emplace_back(
        wxSTC_LUA_WORD,
        wxColour(0, 0, 255),
        wxNullColour,
        font_bold
    );

    defaultFont = font_normal;
    lineNumFont = font_normal;

    wrapMode = wxSTC_WRAP_NONE;
    lineNumWidth = 50;
    lineNumForeground = wxColour(75, 75, 75);
    lineNumBackground = wxColour(220, 220, 220);
    indentationGuides = true;
    indentationGuidesColor = wxColour(220, 220, 220);
    tabWidth = 4;
    indentWidth = 4;
    tabIndents = true;
    useTabs = false;
    edgeMode = wxSTC_EDGE_LINE;
    edgeColor = wxColour(180, 180, 180);
    edgeColumn = 80;
    autoIndent = true;
}

/**
 * Lua editor impl
 */

LuaEditor::LuaEditor()
{
}

LuaEditor::LuaEditor(
    wxWindow* parent,
    wxWindowID id,
    const LuaEditorPrefs& prefs,
    const wxString& name)
{
    LuaEditor::Create(parent, id, prefs, name);
}

bool LuaEditor::Create(
    wxWindow* parent,
    wxWindowID id,
    const LuaEditorPrefs& prefs,
    const wxString& name)
{
    if (!wxStyledTextCtrl::Create(
        parent,
        id,
        wxDefaultPosition,
        wxDefaultSize,
        0,
        name))
        return false;

    SetPrefs(prefs);

    Bind(wxEVT_STC_CHARADDED, [this](wxStyledTextEvent &event) {
        if (!last_prefs_.autoIndent) return ;
        char chr = (char)event.GetKey();
        int currentLine = GetCurrentLine();
        // Change this if support for mac files with \r is needed
        if (chr == '\n') {
            int lineInd = 0;
            if (currentLine > 0) {
                lineInd = GetLineIndentation(currentLine - 1);
            }
            if (lineInd == 0) return;
            SetLineIndentation (currentLine, lineInd);
            GotoPos(PositionFromLine (currentLine) + lineInd);
        }
    });

    return true;
}

LuaEditorPrefs LuaEditor::GetPrefs() const
{
    return last_prefs_;
}

void LuaEditor::SetPrefs(const LuaEditorPrefs& prefs)
{
    last_prefs_ = prefs;

    StyleClearAll();
    SetLexer(wxSTC_LEX_LUA);

    enum {
        MARGIN_LINE_NUMBERS
    };

    SetMarginWidth(MARGIN_LINE_NUMBERS, last_prefs_.lineNumWidth);
    SetMarginType(MARGIN_LINE_NUMBERS, wxSTC_MARGIN_NUMBER);
    StyleSetForeground(wxSTC_STYLE_LINENUMBER, last_prefs_.lineNumForeground);
    StyleSetBackground(wxSTC_STYLE_LINENUMBER, last_prefs_.lineNumBackground);
    SetWrapMode(last_prefs_.wrapMode);
    SetKeyWords(0, last_prefs_.words1);
    SetKeyWords(1, last_prefs_.words2);
    SetKeyWords(2, last_prefs_.words3);
    SetTabIndents(last_prefs_.tabIndents);
    SetIndentationGuides(last_prefs_.indentationGuides);
    StyleSetForeground(
        wxSTC_STYLE_INDENTGUIDE,
        last_prefs_.indentationGuidesColor);
    SetTabWidth(last_prefs_.tabWidth);
    SetIndent(last_prefs_.indentWidth);
    SetTabIndents(last_prefs_.tabIndents);
    SetUseTabs(last_prefs_.useTabs);
    SetEdgeMode(last_prefs_.edgeMode);
    SetEdgeColour(last_prefs_.edgeColor);
    SetEdgeColumn(last_prefs_.edgeColumn);
    StyleSetFont(wxSTC_STYLE_DEFAULT, last_prefs_.defaultFont);
    StyleSetFont(wxSTC_STYLE_LINENUMBER, last_prefs_.lineNumFont);
    SetFont(last_prefs_.defaultFont);   // I do not think this is necessary
                                        // but unsure :/

    // now load styles
    for (auto &style: last_prefs_.styles)
    {
        if (style.font.IsOk())
        {
            StyleSetFont(style.index, style.font);
        }
        if (style.foreground.IsOk())
        {
            StyleSetForeground(style.index, style.foreground);
        }
        if (style.background.IsOk())
        {
            StyleSetBackground(style.index, style.background);
        }
    }
}

LuaEditor::~LuaEditor()
{
}

}
