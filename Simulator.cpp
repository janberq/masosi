#include "Simulator.hpp"

#include "Simulation.hpp"

#include "RegisterWxWidgets.hpp"
#include "Misc/lua_prss.h"
#include "Misc/lua_bundle.h"

#include <thread>
#include <mutex>
#include <atomic>
#include <stdexcept>

#include <iostream> // for debugging

#include <sol.hpp>

namespace masosi {

struct Simulator::Impl
{
    Impl();

    void SetScript(
        Simulator::EScriptType type,
        const std::string &script);
    std::string GetScript(
        Simulator::EScriptType type);

    void Start();

    bool IsRunning();
    bool IsPaused();
    bool IsWaiting();

    void Pause();
    void PauseWait();
    void PauseWait(std::uint32_t timeout_ms);

    void Resume();
    void ResumeWait();
    void ResumeWait(std::uint32_t timeout_ms);

    void WakeUp();
    void WakeUpWait();
    void WakeUpWait(std::uint32_t timeout_ms);

    void Stop();
    void StopWait();
    void StopWait(std::uint32_t timeout_ms);

    void WaitForWaiting();
    void WaitForWaiting(std::uint32_t timeout_ms);

    void ExecuteDrawingScript();

    lua_State *ResetLua(
        Simulator::ELuaState state);
    lua_State *GetLua(
        Simulator::ELuaState state);

    void ResetLuaAll();

    Simulation *GetSimulation();

    void LockSimulation();
    bool LockSimulation(std::size_t timeout);
    void UnlockSimulation();

    bool DisallowRunning();
    void AllowRunning();

    void SetExecutionErrorHandler(std::function<void (const std::exception &ex)> f);
    void SetExitHandler(std::function<void ()> f);

    ~Impl();

private:

    sol::state states_[Simulator::STATE_COUNT];
    struct {
        std::string script;
        std::mutex mtx;
    } scripts_[Simulator::SCRIPT_COUNT];

    std::mutex allow_drawing_mutex_;
    std::mutex running_mutex_;
    std::atomic_bool allow_drawing_ {false};
    std::atomic_bool running_ {false};
    bool resume_after_unlock_ {false};

    std::thread thread_;

    std::unique_ptr<Simulation> simulation_;
    std::unique_ptr<LBNDL_Manager, decltype(&LBNDL_DestroyBundleManager)>
        bundle_manager_;
    std::mutex simulation_mtx_;

    std::function<void (const std::exception &ex)> error_handler_;
    std::function<void ()> exit_handler_;
};

Simulator::Impl::Impl() :
    bundle_manager_(    // not possible to default construct (w/ null deleter 
                        // func) so init in constructor list
                        // specific when special dtor func used
        LBNDL_CreateBundleManager(),
        &LBNDL_DestroyBundleManager)
{
    simulation_ = std::make_unique<Simulation>();

    ResetLua(Simulator::STATE_DRAWING);
    ResetLua(Simulator::STATE_SECOND_THREAD);
}

void Simulator::Impl::SetScript(
    Simulator::EScriptType type,
    const std::string& script)
{
    std::lock_guard<std::mutex> lock{ scripts_[type].mtx };
    scripts_[type].script = script;
}

std::string Simulator::Impl::GetScript(
    Simulator::EScriptType type)
{
    std::lock_guard<std::mutex> lock{ scripts_[type].mtx };
    return scripts_[type].script;
}

void Simulator::Impl::Start()
{
    if (IsRunning())
    {
        throw std::runtime_error("Already running!");
    }

    if (thread_.joinable()) thread_.join();
        // idk why but it seems that thread_ is still joinable
        // even if it stops

    thread_ = std::thread([this]() {
#ifdef PRSS_DEBUG
        PRSS_Status("Secondary Thread ID: ", std::this_thread::get_id());
#endif
        std::unique_lock<std::mutex> running_lock{running_mutex_};
        running_ = true;

        sol::state &lua = states_[Simulator::STATE_SECOND_THREAD];

        auto load_script = [&lua, this](Simulator::EScriptType type) {
            std::string script;
            HPCALL_ResetExitFlag(lua);
            {
                std::lock_guard<std::mutex> lock{ scripts_[type].mtx };
                script = scripts_[type].script; // I do not want the mutex
                // to be locked for a long time
            }
            try
            {
                lua.script(script);
            }
            catch (std::exception &ex)
            {
                if (error_handler_) error_handler_(ex);
            }
        };

        auto allow_drawing = [this](bool allow) {
            std::lock_guard<std::mutex> lock{ allow_drawing_mutex_ };
            allow_drawing_ = allow;
        };

        allow_drawing(false);
        load_script(Simulator::SCRIPT_PRELUDE);
        allow_drawing(true);
        load_script(Simulator::SCRIPT_SIMULATION);
        allow_drawing(false);
        load_script(Simulator::SCRIPT_POSTLUDE);

        running_ = false;

        running_lock.unlock();

        if (exit_handler_) exit_handler_();
    });
}

bool Simulator::Impl::IsRunning()
{
    return running_;
}

bool Simulator::Impl::IsPaused()
{
    return PRSS_IsPaused(states_[Simulator::STATE_SECOND_THREAD]);
}

bool Simulator::Impl::IsWaiting()
{
    return PRSS_IsWaiting(states_[Simulator::STATE_SECOND_THREAD]);
}

void Simulator::Impl::Pause()
{
    if (!running_) return ;
    PRSS_Pause(states_[Simulator::STATE_SECOND_THREAD]);
}

void Simulator::Impl::PauseWait()
{
    if (!running_) return ;
    Pause();
    PRSS_WaitForPaused(
        states_[Simulator::STATE_SECOND_THREAD]);
}

void Simulator::Impl::PauseWait(std::uint32_t timeout_ms)
{
    if (!running_) return ;
    Pause();
    PRSS_WaitForPausedTimeout(
        states_[Simulator::STATE_SECOND_THREAD],
        timeout_ms);
}

void Simulator::Impl::Resume()
{
    if (!running_) return ;
    PRSS_Resume(states_[Simulator::STATE_SECOND_THREAD]);
}

void Simulator::Impl::ResumeWait()
{
    if (!running_) return ;
    Resume();
    PRSS_WaitForNormal(
        states_[Simulator::STATE_SECOND_THREAD]);
}

void Simulator::Impl::ResumeWait(std::uint32_t timeout_ms)
{
    if (!running_) return ;
    Resume();
    PRSS_WaitForNormalTimeout(
        states_[Simulator::STATE_SECOND_THREAD],
        timeout_ms);
}

void Simulator::Impl::WakeUp()
{
    if (!running_) return ;
    Resume();
}

void Simulator::Impl::WakeUpWait()
{
    if (!running_) return ;
    ResumeWait();
}

void Simulator::Impl::WakeUpWait(std::uint32_t timeout_ms)
{
    if (!running_) return ;
    ResumeWait(timeout_ms);
}

void Simulator::Impl::Stop()
{
    if (!running_) return ;
    PRSS_Stop(states_[Simulator::STATE_SECOND_THREAD]);
}

void Simulator::Impl::StopWait()
{
    if (!running_) return ;
    Stop();
    PRSS_WaitForStopped(states_[Simulator::STATE_SECOND_THREAD]);
}

void Simulator::Impl::StopWait(std::uint32_t timeout_ms)
{
    if (!running_) return ;
    Stop();
    PRSS_WaitForStoppedTimeout(states_[Simulator::STATE_SECOND_THREAD], timeout_ms);
}

void Simulator::Impl::WaitForWaiting()
{
    PRSS_WaitForWaiting(states_[Simulator::STATE_SECOND_THREAD]);
}

void Simulator::Impl::WaitForWaiting(std::uint32_t timeout_ms)
{
    PRSS_WaitForWaitingTimeout(
        states_[Simulator::STATE_SECOND_THREAD], timeout_ms);
}

void Simulator::Impl::ExecuteDrawingScript()
{
    std::lock_guard<std::mutex> lock{ allow_drawing_mutex_ };
    if (allow_drawing_)
    {
        std::lock_guard<std::mutex> lock{
            scripts_[Simulator::SCRIPT_DRAWING].mtx };
        try {
            states_[Simulator::STATE_DRAWING].script(
                scripts_[Simulator::SCRIPT_DRAWING].script
            );
        } catch (std::runtime_error &ex) {
            if (error_handler_) error_handler_(ex);
        }
    }
}

#define RESET_DRAWING_STATE_CODE \
        auto & ref = states_[Simulator::STATE_DRAWING]; \
        LBNDL_UnassociateState(bundle_manager_.get(), ref); \
        ref = sol::state{}; \
        ref.open_libraries( \
            sol::lib::base, \
            sol::lib::io, \
            sol::lib::math, \
            sol::lib::string, \
            sol::lib::table, \
            sol::lib::os); \
        RegisterWxWidgets(ref); \
        RegisterVector(ref); \
        RegisterColor(ref); \
        LBNDL_RegisterBundleManager(bundle_manager_.get(), ref, "_B");

#define RESET_SECOND_THREAD_STATE_CODE \
        auto & ref = states_[Simulator::STATE_SECOND_THREAD]; \
        LBNDL_UnassociateState(bundle_manager_.get(), ref); \
        ref = sol::state{}; \
        ref.open_libraries( \
            sol::lib::base, \
            sol::lib::io, \
            sol::lib::math, \
            sol::lib::string, \
            sol::lib::table, \
            sol::lib::os); \
        RegisterVector(ref); \
        RegisterPolygon(ref); \
        RegisterColor(ref); \
        RegisterSimulation(ref); \
        ref["_SIM"] = simulation_.get(); \
        PRSS_Register(ref); \
        LBNDL_RegisterBundleManager(bundle_manager_.get(), ref, "_B");

lua_State * Simulator::Impl::ResetLua(Simulator::ELuaState state)
{
    if (state == Simulator::STATE_DRAWING)
    {
        RESET_DRAWING_STATE_CODE
        return ref;
    }
    else if (state == Simulator::STATE_SECOND_THREAD)
    {
        RESET_SECOND_THREAD_STATE_CODE
        return ref;
    }
    else
    {
        // problem
    }
    return nullptr;
}

void Simulator::Impl::ResetLuaAll()
{
    { RESET_DRAWING_STATE_CODE }
    { RESET_SECOND_THREAD_STATE_CODE }
}

#undef RESET_DRAWING_STATE_CODE
#undef RESET_SECOND_THREAD_STATE_CODE

lua_State * Simulator::Impl::GetLua(Simulator::ELuaState state)
{
    return states_[state];
}

Simulation * Simulator::Impl::GetSimulation()
{
    return simulation_.get();
}

void Simulator::Impl::LockSimulation()
{
    if (IsPaused())
    {
        resume_after_unlock_ = false;
    }
    else
    {
        PauseWait();
        resume_after_unlock_ = true;
    }
    simulation_mtx_.lock();
}

bool Simulator::Impl::LockSimulation(std::size_t timeout)
{
    if (IsPaused())
    {
        resume_after_unlock_ = false;
        simulation_mtx_.lock();
        return true;
    }
    else
    {
        PauseWait(timeout);
        if (IsPaused())
        {
            resume_after_unlock_ = true;
            simulation_mtx_.lock();
            return true;
        }
        else
        {
            return false;
        }
    }
}

void Simulator::Impl::UnlockSimulation()
{
    simulation_mtx_.unlock();
    if (resume_after_unlock_) Resume();
}

bool Simulator::Impl::DisallowRunning()
{
    return running_mutex_.try_lock();
}

void Simulator::Impl::AllowRunning()
{
    running_mutex_.unlock();
}

void Simulator::Impl::SetExecutionErrorHandler(std::function<void (const std::exception &)> f)
{
    error_handler_ = std::move(f);
}

void Simulator::Impl::SetExitHandler(std::function<void ()> f)
{
    exit_handler_ = std::move(f);
}

Simulator::Impl::~Impl()
{
    Stop();
    if (thread_.joinable()) thread_.join();
}

//BEGIN pseudo-implementation

Simulator::Simulator()
{
    impl_ = std::make_unique<Impl>();
}

void Simulator::SetScript(
    Simulator::EScriptType type,
    const std::string& script)
{
    impl_->SetScript(type, script);
}

std::string Simulator::GetScript(
    Simulator::EScriptType type)
{
    return impl_->GetScript(type);
}

void Simulator::Start()
{
    impl_->Start();
}

bool Simulator::IsRunning()
{
    return impl_->IsRunning();
}

bool Simulator::IsPaused()
{
    return impl_->IsPaused();
}

bool Simulator::IsWaiting()
{
    return impl_->IsWaiting();
}

void Simulator::Pause()
{
    impl_->Pause();
}

void Simulator::PauseWait()
{
    impl_->PauseWait();
}

void Simulator::PauseWait(std::uint32_t timeout_ms)
{
    impl_->PauseWait(timeout_ms);
}

void Simulator::Resume()
{
    impl_->Resume();
}

void Simulator::ResumeWait()
{
    impl_->ResumeWait();
}

void Simulator::ResumeWait(std::uint32_t timeout_ms)
{
    impl_->ResumeWait(timeout_ms);
}

void Simulator::WakeUp()
{
    impl_->WakeUp();
}

void Simulator::WakeUpWait()
{
    impl_->WakeUpWait();
}

void Simulator::WakeUpWait(std::uint32_t timeout_ms)
{
    impl_->WakeUpWait(timeout_ms);
}

void Simulator::Stop()
{
    impl_->Stop();
}

void Simulator::StopWait()
{
    impl_->StopWait();
}

void Simulator::StopWait(std::uint32_t timeout_ms)
{
    impl_->StopWait(timeout_ms);
}

void Simulator::WaitForWaiting()
{
    impl_->WaitForWaiting();
}

void Simulator::WaitForWaiting(std::uint32_t timeout_ms)
{
    impl_->WaitForWaiting(timeout_ms);
}

void Simulator::ExecuteDrawingScript()
{
    impl_->ExecuteDrawingScript();
}

lua_State * Simulator::ResetLua(Simulator::ELuaState state)
{
    return impl_->ResetLua(state);
}

void Simulator::ResetLuaAll()
{
    impl_->ResetLuaAll();
}

lua_State * Simulator::GetLua(Simulator::ELuaState state)
{
    return impl_->GetLua(state);
}

Simulation * Simulator::GetSimulation()
{
    return impl_->GetSimulation();
}

void Simulator::LockSimulation()
{
    impl_->LockSimulation();
}

bool Simulator::LockSimulation(std::size_t timeout)
{
    return impl_->LockSimulation(timeout);
}

void Simulator::UnlockSimulation()
{
    impl_->UnlockSimulation();
}

bool Simulator::DisallowRunning()
{
    return impl_->DisallowRunning();
}

void Simulator::AllowRunning()
{
    impl_->AllowRunning();
}

void Simulator::SetExecutionErrorHandler(std::function<void (const std::exception &)> f)
{
    impl_->SetExecutionErrorHandler(std::move(f));
}

void Simulator::SetExitHandler(std::function<void ()> f)
{
    impl_->SetExitHandler(std::move(f));
}

Simulator::~Simulator()
{
}

//END

}
