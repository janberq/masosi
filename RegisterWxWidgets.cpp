// 2016-08-18

#include "RegisterWxWidgets.hpp"

#include <sol.hpp>

#include <stdexcept>
#include <vector>

#include <wx/dc.h>
#include <wx/string.h>
#include <wx/font.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/colour.h>

//BEGIN wxString

namespace sol {
namespace stack {
template <>
struct pusher<wxString> {
    static int push ( lua_State* L, const ::wxString& str ) {
        return stack::push(L, str.mb_str(wxConvUTF8).data());
    }
};

template <>
struct getter< ::wxString > {
    static wxString get ( lua_State* L, int index, record& tracking ) {
        tracking.use(1);
        const char* luastr = stack::get<const char*>(L, index);
        return wxString::FromUTF8(luastr);
    }
};
} // stack

template <>
struct lua_type_of<::wxString> : std::integral_constant<type, type::string> {};
} //sol

//END

namespace masosi {

//BEGIN RegisterWxFont
void RegisterWxFont(lua_State *L)
{
    sol::state_view lua{L};

    lua.new_enum(
        "wxFontFamily",
        "default", wxFontFamily::wxFONTFAMILY_DEFAULT,
        "decorative", wxFontFamily::wxFONTFAMILY_DECORATIVE,
        "roman", wxFontFamily::wxFONTFAMILY_ROMAN,
        "script", wxFontFamily::wxFONTFAMILY_SCRIPT,
        "swiss", wxFontFamily::wxFONTFAMILY_SWISS,
        "modern", wxFontFamily::wxFONTFAMILY_MODERN,
        "teletype", wxFontFamily::wxFONTFAMILY_TELETYPE
    );

    lua.new_enum(
        "wxFontStyle",
        "normal", wxFontStyle::wxFONTSTYLE_NORMAL,
        "italic", wxFontStyle::wxFONTSTYLE_ITALIC,
        "slant", wxFontStyle::wxFONTSTYLE_SLANT,
        "max", wxFontStyle::wxFONTSTYLE_MAX
    );

    lua.new_enum(
        "wxFontWeight",
        "normal", wxFontWeight::wxFONTWEIGHT_NORMAL,
        "light", wxFontWeight::wxFONTWEIGHT_LIGHT,
        "bold", wxFontWeight::wxFONTWEIGHT_BOLD
    );

    lua.new_enum(
        "wxFontSymbolicSize",
        "xxsmall", wxFontSymbolicSize::wxFONTSIZE_XX_SMALL,
        "xsmall", wxFontSymbolicSize::wxFONTSIZE_X_SMALL,
        "small", wxFontSymbolicSize::wxFONTSIZE_SMALL,
        "medium", wxFontSymbolicSize::wxFONTSIZE_MEDIUM,
        "large", wxFontSymbolicSize::wxFONTSIZE_LARGE,
        "xlarge", wxFontSymbolicSize::wxFONTSIZE_X_LARGE,
        "xxlarge", wxFontSymbolicSize::wxFONTSIZE_XX_LARGE
    );

#define FONT_FUNC_REGISTER(f) #f , & wxFont :: f

    lua.new_usertype<wxFont>(
        "wxFont",
        sol::constructors<
            sol::types<>,
            sol::types<const wxFont&>,
            sol::types<
                int,
                wxFontFamily,
                wxFontStyle,
                wxFontWeight,
                bool,
                wxString>
        >(),
#if wxVERSION_NUMBER >= 3100
        FONT_FUNC_REGISTER(GetBaseFont),
#endif
        FONT_FUNC_REGISTER(GetEncoding),
        FONT_FUNC_REGISTER(GetFaceName),
        FONT_FUNC_REGISTER(GetFamily),
        FONT_FUNC_REGISTER(GetNativeFontInfoDesc),
        FONT_FUNC_REGISTER(GetNativeFontInfoUserDesc),
//      FONT_FUNC_REGISTER(GetNativeFontInfo),
        FONT_FUNC_REGISTER(GetPointSize),
        FONT_FUNC_REGISTER(GetPixelSize),
        FONT_FUNC_REGISTER(GetStyle),
        FONT_FUNC_REGISTER(GetUnderlined),
        FONT_FUNC_REGISTER(GetStrikethrough),
        FONT_FUNC_REGISTER(GetWeight),
        FONT_FUNC_REGISTER(IsFixedWidth),
        FONT_FUNC_REGISTER(IsOk),
        FONT_FUNC_REGISTER(Bold),
        FONT_FUNC_REGISTER(Italic),
        FONT_FUNC_REGISTER(Larger),
        FONT_FUNC_REGISTER(Smaller),
        FONT_FUNC_REGISTER(Underlined),
        FONT_FUNC_REGISTER(Strikethrough),
        FONT_FUNC_REGISTER(MakeBold),
        FONT_FUNC_REGISTER(MakeItalic),
        FONT_FUNC_REGISTER(MakeLarger),
        FONT_FUNC_REGISTER(MakeSmaller),
        FONT_FUNC_REGISTER(MakeUnderlined),
        FONT_FUNC_REGISTER(MakeStrikethrough),
        FONT_FUNC_REGISTER(Scale),
        FONT_FUNC_REGISTER(Scaled),
        FONT_FUNC_REGISTER(SetEncoding),
        FONT_FUNC_REGISTER(SetFaceName),
//      FONT_FUNC_REGISTER(SetFamily),
        "SetFamily", /* sol::overload( */
            [](wxFont &font, wxFontFamily family) {
                font.SetFamily(family);
            }
        /* ) */,
//      FONT_FUNC_REGISTER(SetNativeFontInfo),
        "SetNativeFontInfo", sol::overload(
            [](wxFont &font, const wxString &info) {
                font.SetNativeFontInfo(info);
            }/* ,
            [](wxFont &font, const wxNativeFontInfo &info) {
                font.SetNativeFontInfo(info);
            } */
        ),
        FONT_FUNC_REGISTER(SetNativeFontInfoUserDesc),
//      FONT_FUNC_REGISTER(SetNativeFontInfo),
        FONT_FUNC_REGISTER(SetPointSize),
        FONT_FUNC_REGISTER(SetPixelSize),
//      FONT_FUNC_REGISTER(SetStyle),
        "SetStyle", [](wxFont &font, wxFontStyle style) {
            font.SetStyle(style);
        },
        FONT_FUNC_REGISTER(SetSymbolicSize),
        FONT_FUNC_REGISTER(SetSymbolicSizeRelativeTo),
        FONT_FUNC_REGISTER(SetUnderlined),
        FONT_FUNC_REGISTER(SetStrikethrough),
//      FONT_FUNC_REGISTER(SetWeight)
        "SetWeight", [](wxFont &font, wxFontWeight weight) {
            font.SetWeight(weight);
        }
    );

#undef FONT_FUNC_REGISTER

    lua["wxNullFont"] = wxNullFont;
}
//END

//BEGIN RegisterWxColour
void RegisterWxColour(lua_State *L)
{
    sol::state_view lua{L};

#define COLOUR_FUNC_REGISTER(x) #x , & wxColour :: x

    wxColour x;
    lua.new_usertype<wxColour>(
        "wxColour",
        sol::constructors<
            sol::types<>,
            sol::types<const wxColour &>,
            sol::types<const wxString &>,
            sol::types<
                unsigned char,
                unsigned char,
                unsigned char,
                unsigned char>,
            sol::types<
                unsigned char,
                unsigned char,
                unsigned char
                >
        >(),
        COLOUR_FUNC_REGISTER(Red),
        COLOUR_FUNC_REGISTER(Green),
        COLOUR_FUNC_REGISTER(Blue),
        COLOUR_FUNC_REGISTER(Alpha),
        COLOUR_FUNC_REGISTER(GetAsString),
        COLOUR_FUNC_REGISTER(IsOk),
//      COLOUR_FUNC_REGISTER(MakeDisabled),
        "MakeDisabled", [](wxColour &colour, unsigned char brightness) {
            colour.MakeDisabled(brightness);
        },
//      COLOUR_FUNC_REGISTER(ChangeLightness),
        "ChangeLightness", [](wxColour &colour, int ialpha) {
            colour.ChangeLightness(ialpha);
        },
        COLOUR_FUNC_REGISTER(SetRGB),
        COLOUR_FUNC_REGISTER(SetRGBA),
        COLOUR_FUNC_REGISTER(GetRGB),
        COLOUR_FUNC_REGISTER(GetRGBA),
//      COLOUR_FUNC_REGISTER(Set),
        "Set", sol::overload(
            [](
            wxColour &colour,
            unsigned char r,
            unsigned char g,
            unsigned char b,
            unsigned char a) {
                colour.Set(r, g, b, a);
            },
            [](wxColour &colour, unsigned long rgb) {
                colour.Set(rgb);
            },
            [](wxColour &colour, const wxString &str) {
                colour.Set(str);
            }
        )
    );

#undef COLOUR_FUNC_REGISTER

}
//END

//BEGIN RegisterWxPen
void RegisterWxPen(lua_State *L)
{

    sol::state_view lua{L};

    lua.new_enum(
        "wxPenCap",
        "invalid", wxPenCap::wxCAP_INVALID,
        "round", wxPenCap::wxCAP_ROUND,
        "projecting", wxPenCap::wxCAP_PROJECTING,
        "butt", wxPenCap::wxCAP_BUTT
    );

    lua.new_enum(
        "wxPenJoin",
        "invalid", wxPenJoin::wxJOIN_INVALID,
        "bevel", wxPenJoin::wxJOIN_BEVEL,
        "miter", wxPenJoin::wxJOIN_MITER,
        "round", wxPenJoin::wxJOIN_ROUND
    );

    lua.new_enum(
        "wxPenStyle",
        "invalid", wxPenStyle::wxPENSTYLE_INVALID,
        "solid", wxPenStyle::wxPENSTYLE_SOLID,
        "dot", wxPenStyle::wxPENSTYLE_DOT,
        "long_dash", wxPenStyle::wxPENSTYLE_LONG_DASH,
        "short_dash", wxPenStyle::wxPENSTYLE_SHORT_DASH,
        "dot_dash", wxPenStyle::wxPENSTYLE_DOT_DASH,
        "user_dash", wxPenStyle::wxPENSTYLE_USER_DASH,
        "transparent", wxPenStyle::wxPENSTYLE_TRANSPARENT,
        "stipple_mask_opaque", wxPenStyle::wxPENSTYLE_STIPPLE_MASK_OPAQUE,
        "stipple_mask", wxPenStyle::wxPENSTYLE_STIPPLE_MASK,
        "stipple", wxPenStyle::wxPENSTYLE_STIPPLE,
        "bdiagonal_hatch", wxPenStyle::wxPENSTYLE_BDIAGONAL_HATCH,
        "crossdiag_hatch", wxPenStyle::wxPENSTYLE_CROSSDIAG_HATCH,
        "fdiagonal_hatch", wxPenStyle::wxPENSTYLE_FDIAGONAL_HATCH,
        "cross_hatch", wxPenStyle::wxPENSTYLE_CROSS_HATCH,
        "horizontal_hatch", wxPenStyle::wxPENSTYLE_HORIZONTAL_HATCH,
        "vertical_hatch", wxPenStyle::wxPENSTYLE_VERTICAL_HATCH,
        "first_hatch", wxPenStyle::wxPENSTYLE_FIRST_HATCH,
        "last_hatch", wxPenStyle::wxPENSTYLE_LAST_HATCH
    );

    class wxDashes
    {
        std::vector<wxDash> dashes_;
        wxDash *ptr_ {nullptr};
        std::size_t size_ {0};
    public:
        wxDashes() {
            
        }

        wxDashes(std::size_t size, wxDash *dashes) {
            size_ = size;
            ptr_ = dashes;
        }

        void Resize(std::size_t n) {
            if (ptr_ != nullptr)
                throw std::runtime_error("wxDashes, pre-allocated");
            dashes_.resize(n);
        }

        void Add(long dash) {
            if (ptr_ != nullptr)
                throw std::runtime_error("wxDashes, pre-allocated");
            dashes_.push_back(dash);
        }

        long Get(std::size_t n) const {
            if (ptr_ == nullptr) {
                if (n >= dashes_.size())
                    throw std::out_of_range("wxDashes, out of range");
                return dashes_[n];
            }
            else {
                if (n >= size_)
                    throw std::out_of_range("wxDashes, out of range");
                return ptr_[n];
            }
        };

        void Set(std::size_t n, long dash) {
            if (ptr_ == nullptr) {
                if (n >= dashes_.size())
                    throw std::out_of_range("wxDashes, out of range");
                dashes_[n] = dash;
            }
            else {
                if (n >= size_)
                    throw std::out_of_range("wxDashes, out of range");
                ptr_[n] = dash;
            }
        }

        std::size_t Size() const {
            if (ptr_)
                return size_;
            return dashes_.size();
        }

        wxDash *Ptr() {
            if (ptr_)
                return ptr_;
            return &dashes_[0];
        }

    };

#define DASHES_FUNC_REGISTER(x) #x , & wxDashes :: x

    lua.new_usertype<wxDashes>(
        "wxDashes",
        sol::constructors<
            sol::types<>
        >(),
        DASHES_FUNC_REGISTER(Resize),
        DASHES_FUNC_REGISTER(Add),
        DASHES_FUNC_REGISTER(Get),
        DASHES_FUNC_REGISTER(Set),
        DASHES_FUNC_REGISTER(Size)
    );

#undef DASHES_FUNC_REGISTER

#define PEN_FUNC_REGISTER(x) #x , & wxPen :: x

    lua.new_usertype<wxPen>(
        "wxPen",
        sol::constructors<
            sol::types<>,
            sol::types<
                const wxColour &,
                int,
                wxPenStyle>,
#if defined(__WXMSW__) || defined(__WXOSX_COCOA__)
            sol::types<
                const wxBitmap &,
                int
            >,
#endif
            sol::types<
                const wxPen &
            >
        >(),
        PEN_FUNC_REGISTER(GetCap),
        PEN_FUNC_REGISTER(GetColour),
        /* PEN_FUNC_REGISTER(GetDashes), */
        "GetDashes", [](wxPen &pen) {
            wxDash *ptr;
            std::size_t n;
            n = pen.GetDashes(&ptr);
            return wxDashes(n, ptr);
        },
        PEN_FUNC_REGISTER(GetJoin),
        PEN_FUNC_REGISTER(GetStipple),
        PEN_FUNC_REGISTER(GetStyle),
        PEN_FUNC_REGISTER(GetWidth),
        PEN_FUNC_REGISTER(IsOk),
        PEN_FUNC_REGISTER(IsNonTransparent),
        PEN_FUNC_REGISTER(IsTransparent),
        PEN_FUNC_REGISTER(SetCap),
        /* PEN_FUNC_REGISTER(SetDashes), */
        "SetDashes", [](wxPen &pen, wxDashes &dashes) {
            pen.SetDashes(dashes.Size(), dashes.Ptr());
        },
        PEN_FUNC_REGISTER(SetJoin),
        PEN_FUNC_REGISTER(SetStipple),
        /* PEN_FUNC_REGISTER(SetStyle), */
        "SetStyle", [](wxPen &pen, wxPenStyle style) {
            pen.SetStyle(style);
        },
        PEN_FUNC_REGISTER(SetWidth),
        /* PEN_FUNC_REGISTER(SetColour), */
        "SetColour",
        [](wxPen &pen, const wxColour &colour) {
            pen.SetColour(colour);
        }
    );

#undef PEN_FUNC_REGISTER

}
//END

//BEGIN RegisterWxBrush
void RegisterWxBrush(lua_State *L)
{
    sol::state_view lua{L};

    lua.new_enum(
        "wxBrushStyle",
        "invalid", wxBrushStyle::wxBRUSHSTYLE_INVALID,
        "solid", wxBrushStyle::wxBRUSHSTYLE_SOLID,
        "transparent", wxBrushStyle::wxBRUSHSTYLE_TRANSPARENT,
        "stipple_mask_opaque", wxBrushStyle::wxBRUSHSTYLE_STIPPLE_MASK_OPAQUE,
        "stipple_mask", wxBrushStyle::wxBRUSHSTYLE_STIPPLE_MASK,
        "stipple", wxBrushStyle::wxBRUSHSTYLE_STIPPLE,
        "bdiagonal_hatch", wxBrushStyle::wxBRUSHSTYLE_BDIAGONAL_HATCH,
        "crossdiag_hatch", wxBrushStyle::wxBRUSHSTYLE_CROSSDIAG_HATCH,
        "fdiagonal_hatch", wxBrushStyle::wxBRUSHSTYLE_FDIAGONAL_HATCH,
        "cross_hatch", wxBrushStyle::wxBRUSHSTYLE_CROSS_HATCH,
        "horizontal_hatch", wxBrushStyle::wxBRUSHSTYLE_HORIZONTAL_HATCH,
        "vertical_hatch", wxBrushStyle::wxBRUSHSTYLE_VERTICAL_HATCH,
        "first_hatch", wxBrushStyle::wxBRUSHSTYLE_FIRST_HATCH,
        "last_hatch", wxBrushStyle::wxBRUSHSTYLE_LAST_HATCH
    );

#define BRUSH_FUNC_REGISTER(x) #x , & wxBrush :: x

    lua.new_usertype<wxBrush>(
        "wxBrush",
        sol::constructors<
            sol::types<>,
            sol::types<const wxColour &, wxBrushStyle>,
            sol::types<const wxBitmap &>,
            sol::types<const wxBrush &>
        >(),
        BRUSH_FUNC_REGISTER(GetColour),
        BRUSH_FUNC_REGISTER(GetStipple),
        BRUSH_FUNC_REGISTER(GetStyle),
        BRUSH_FUNC_REGISTER(IsHatch),
        BRUSH_FUNC_REGISTER(IsOk),
        BRUSH_FUNC_REGISTER(IsNonTransparent),
        BRUSH_FUNC_REGISTER(IsTransparent),
        BRUSH_FUNC_REGISTER(SetStipple),
        /* BRUSH_FUNC_REGISTER(SetStyle), */
        "SetStyle", [](wxBrush &brush, wxBrushStyle style) {
            brush.SetStyle(style);
        },
        /* BRUSH_FUNC_REGISTER(SetColour), */
        "SetColour",
        [](wxBrush &brush, const wxColour &colour) {
            brush.SetColour(colour);
        }
    );

#undef BRUSH_FUNC_REGISTER

}
//END

//BEGIN RegisterWxPoint
void RegisterWxPoint(lua_State *L)
{
    sol::state_view lua{L};

    lua.new_usertype<wxPoint>(
        "wxPoint",
        sol::constructors<
            sol::types<>,
            sol::types<int, int>,
            sol::types<const wxPoint &>
        >(),
        "x", &wxPoint::x,
        "y", &wxPoint::y,
        sol::meta_function::addition,
            [](const wxPoint &a, const wxPoint &b) -> wxPoint
            { return a + b; }, // since they have unary overloads
        sol::meta_function::subtraction,
            [](const wxPoint &a, const wxPoint &b) -> wxPoint
            { return a - b; },
        sol::meta_function::multiplication, sol::overload(
            [](double a, const wxPoint &b) {
                return a * b;
            },
            [](const wxPoint &a, double b) {
                return a * b;
            }
        ),
        sol::meta_function::division,
            [](const wxPoint &p, double s) {
                return wxPoint(p.x/s, p.y/s);
            },
        sol::meta_function::unary_minus,
            [](const wxPoint &a) -> wxPoint {
                return -a;
            },
        sol::meta_function::equal_to,
            [](const wxPoint &p1, const wxPoint &p2) {
                return p1 == p2;
            }
    );
}
//END

//BEGIN RegisterWxSize
void RegisterWxSize(lua_State *L)
{
    sol::state_view lua{L};

    lua.new_usertype<wxSize>(
        "wxSize",
        sol::constructors<
            sol::types<>,
            sol::types<int, int>,
            sol::types<const wxSize &>
        >(),
        "wi", sol::property(
            &wxSize::GetWidth,
            &wxSize::SetWidth),
        "he", sol::property(
            &wxSize::GetHeight,
            &wxSize::SetHeight),
        sol::meta_function::addition,
            [](const wxSize &a, const wxSize &b) -> wxSize
            { return a + b; }, // since they have unary overloads
        sol::meta_function::subtraction,
            [](const wxSize &a, const wxSize &b) -> wxSize
            { return a - b; },
        sol::meta_function::multiplication, sol::overload(
            [](double a, const wxSize &b) {
                return a * b;
            },
            [](const wxSize &a, double b) {
                return a * b;
            }
        ),
        sol::meta_function::division,
            [](const wxSize &a, double b) {
                return wxSize(a.GetWidth()/b, a.GetHeight()/b);
            },
        sol::meta_function::equal_to,
            [](const wxSize &a, const wxSize &b) {
                return a == b;
            }
    );
}
//END

//BEGIN
void RegisterWxRect(lua_State *L)
{
    sol::state_view lua{L};

    lua.new_usertype<wxRect>(
        "wxRect",
        sol::constructors<
            sol::types<>,
            sol::types<int, int, int, int>,
            sol::types<const wxPoint&, const wxPoint &>,
            sol::types<const wxPoint&, const wxSize &>,
            sol::types<const wxSize &>,
            sol::types<const wxRect &>
        >(),
        "height", &wxRect::height,
        "width", &wxRect::width,
        "x", &wxRect::x,
        "y", &wxRect::y
    );
}
//END

//BEGIN RegisterWxBitmap
// TODO In far far far future, I may implement wxImage
void RegisterWxBitmap(lua_State *L)
{
    sol::state_view lua{L};

#define BITMAP_FUNC_REGISTER(x) #x , & wxBitmap :: x

    lua.new_usertype<wxBitmap>(
        "wxBitmap",
        "new", sol::initializers(
            [](wxBitmap *bmp, const wxString &fpath) {
                wxImage img;
                img.LoadFile(fpath);
                new (bmp) wxBitmap(img);
            },
            [](wxBitmap *bmp) {
                new (bmp) wxBitmap;
            },
            [](wxBitmap *bmp, wxBitmap *bmp_other) {
                new (bmp) wxBitmap(*bmp_other);
            }
        ),
        BITMAP_FUNC_REGISTER(GetWidth),
        BITMAP_FUNC_REGISTER(GetHeight),
        BITMAP_FUNC_REGISTER(GetDepth),
        BITMAP_FUNC_REGISTER(IsOk),
        "__gc", sol::destructor([](wxBitmap *bmp) {
            bmp->~wxBitmap();
        })
    );

#undef BITMAP_FUNC_REGISTER

}
//END

//BEGIN RegisterVector

// since std::vector<T> is handled specially by sol2
// it's best to use my vector
// note: this is not a complete implementation of vector,
// for most functions, const versions arent provided
template <typename T>
class Vector
{
public:
    Vector() {}
    Vector(std::size_t sz) : backend_(sz) {}
    Vector(std::size_t sz, const T& t) : backend_(sz, t) {}
    Vector(const Vector<T> &other) { backend_ = other.backend_; }

    T& front() { return backend_.front(); }
    T& back() { return backend_.back(); }
    bool empty() const { return backend_.empty(); }
    std::size_t size() const { return backend_.size(); }
    std::size_t max_size() const { return backend_.max_size(); }
    void resize(std::size_t sz) { backend_.resize(sz); }
    void resize(std::size_t sz, const T& t) { backend_.resize(sz, t); }
    void reserve(std::size_t sz) { backend_.reserve(sz); }
    std::size_t capacity() const { return backend_.capacity(); }
    void shrink_to_fit() { backend_.shrink_to_fit(); }
    void clear() { backend_.clear(); }
    void insert(std::size_t pos, const T &val) {
        backend_.insert(backend_.begin() + pos, val);
    }
    void erase(std::size_t pos1, std::size_t pos2) {
        backend_.erase(
            backend_.begin() + pos1,
            backend_.begin() + pos2);
    }
    void erase(std::size_t pos) {
        backend_.erase(backend_.begin() + pos);
    }
    void push_back(const T &t) {
        backend_.push_back(t);
    }
    void pop_back() {
        backend_.pop_back();
    }
    void swap(Vector &other) {
        backend_.swap(other.backend_);
    }
    void iterate(std::function<bool (T&)> f) {
        for (auto &o: backend_)
        {
            if (!f(o)) break;
        }
    }

    T& operator[](std::size_t n) {
        return backend_[n];
    }

    const T& operator[](std::size_t n) const {
        return backend_[n];
    }

protected:
    std::vector<T> backend_;
};

template <class T>
void RegisterVector(lua_State *L, const char *name)
{

    sol::state_view lua{L};
    typedef Vector<T> V;
    lua.new_usertype<V>(
        name,
        sol::constructors<
            sol::types<>,
            sol::types<std::size_t>,
            sol::types<std::size_t, const T&>,
            sol::types<const V&>
        >(),
        sol::meta_function::index,
        [](
            V *v,
            std::size_t n,
            sol::this_state s) -> sol::object {
            sol::state_view lua{s};
            if (n < v->size()) {
                return sol::make_object(lua, (T) (*v)[n]);
            }
            else {
                return sol::make_object(lua, sol::nil);
            }
        },
        sol::meta_function::new_index,
        [](
            V *v,
            std::size_t n,
            const T &value,
            sol::this_state s) {
            sol::state_view lua{s};
            if (n < v->size())
            {
                (*v)[n] = value;
            }
            else
            {
                luaL_error(lua, "range overflow!");
            }
        },
        "front", (T& (V::*)(void)) &V::front,
        "back", (T& (V::*)(void)) &V::back,
        "empty", &V::empty,
        "size", &V::size,
        "max_size", &V::max_size,
        "reserve", &V::reserve,
        "capacity", &V::capacity,
        "shrink_to_fit", &V::shrink_to_fit,
        "clear", &V::clear,
        "insert", &V::insert,
        "erase", sol::overload(
            (void (V::*)(std::size_t, std::size_t)) &V::erase,
            (void (V::*)(std::size_t)) &V::erase
        ),
        "push_back", (void (V::*)(const T&)) &V::push_back,
        "pop_back", &V::pop_back,
        "resize", sol::overload(
            (void (V::*)(std::size_t)) &V::resize,
            (void (V::*)(std::size_t, const T&)) &V::resize),
        "swap", [](V *v, V *other) {
            v->swap(*other);
        },
        "iterate", &V::iterate
    );
}
//END

//BEGIN RegisterWxDC
void RegisterWxDC(lua_State *L)
{
    sol::state_view lua{L};

    lua.new_enum(
        "wxLayoutDirection",
        "default", wxLayoutDirection::wxLayout_Default,
        "ltr", wxLayoutDirection::wxLayout_LeftToRight,
        "rtl", wxLayoutDirection::wxLayout_RightToLeft
    );

    lua.new_enum(
        "wxRasterOperationMode",
        "clear", wxRasterOperationMode::wxCLEAR,
        "xor", wxRasterOperationMode::wxXOR,
        "invert", wxRasterOperationMode::wxINVERT,
        "or_reverse", wxRasterOperationMode::wxOR_REVERSE,
        "and_reverse", wxRasterOperationMode::wxAND_REVERSE,
        "copy", wxRasterOperationMode::wxCOPY,
        "and", wxRasterOperationMode::wxAND,
        "and_invert", wxRasterOperationMode::wxAND_INVERT,
        "no_op", wxRasterOperationMode::wxNO_OP,
        "nor", wxRasterOperationMode::wxNOR,
        "equiv", wxRasterOperationMode::wxEQUIV,
        "src_invert", wxRasterOperationMode::wxSRC_INVERT,
        "or_invert", wxRasterOperationMode::wxOR_INVERT,
        "nand", wxRasterOperationMode::wxNAND,
        "or", wxRasterOperationMode::wxOR,
        "set", wxRasterOperationMode::wxSET
    );

    lua.new_enum(
        "wxMappingMode",
        "text", wxMappingMode::wxMM_TEXT,
        "metric", wxMappingMode::wxMM_METRIC,
        "lometric", wxMappingMode::wxMM_LOMETRIC,
        "twips", wxMappingMode::wxMM_TWIPS,
        "points", wxMappingMode::wxMM_POINTS
    );

    lua.new_enum(
        "wxFloodFillStyle",
        "surface", wxFloodFillStyle::wxFLOOD_SURFACE,
        "border", wxFloodFillStyle::wxFLOOD_BORDER
    );

    lua.new_enum(
        "wxDirection",
        "left", wxDirection::wxLEFT,
        "right", wxDirection::wxRIGHT,
        "up", wxDirection::wxUP,
        "down", wxDirection::wxDOWN,
        "top", wxDirection::wxTOP,
        "bottom", wxDirection::wxBOTTOM,
        "north", wxDirection::wxNORTH,
        "south", wxDirection::wxSOUTH,
        "west", wxDirection::wxWEST,
        "east", wxDirection::wxEAST,
        "all", wxDirection::wxALL,
        "mask", wxDirection::wxDIRECTION_MASK
    );

    lua.new_enum(
        "wxPolygonFillMode",
        "oddeven_rule", wxPolygonFillMode::wxODDEVEN_RULE,
        "winding_rule", wxPolygonFillMode::wxWINDING_RULE
    );

#define FONTMETRICS_REGISTER(x) #x , & wxFontMetrics :: x

    lua.new_usertype<wxFontMetrics>(
        "wxFontMetrics",
        FONTMETRICS_REGISTER(height),
        FONTMETRICS_REGISTER(ascent),
        FONTMETRICS_REGISTER(descent),
        FONTMETRICS_REGISTER(internalLeading),
        FONTMETRICS_REGISTER(externalLeading),
        FONTMETRICS_REGISTER(averageWidth)
    );

#undef FONTMETRICS_REGISTER

    // Other stuff
    RegisterVector<wxPoint>(lua, "wxPointVector");
    RegisterVector<int>(lua, "wxIntVector");

#define DC_FUNC_REGISTER(x) #x , & wxDC :: x
    // to solve the overloaded functions problems
#define DC_MEMBER_FUNC_REGISTER(x, ret, ...) \
    #x, (ret ( wxDC :: * ) (__VA_ARGS__) ) & wxDC :: x

#define DC_MEMBER_FUNC_CONST_REGISTER(x, ret, ...) \
    #x, (ret ( wxDC :: * ) (__VA_ARGS__) const) & wxDC :: x

    lua.new_usertype<wxDC>(
        "wxDC",
        "new", sol::no_constructor,
        DC_MEMBER_FUNC_REGISTER(CopyAttributes, void, const wxDC&),
        DC_MEMBER_FUNC_CONST_REGISTER(IsOk, bool),
        DC_MEMBER_FUNC_CONST_REGISTER(CanDrawBitmap, bool, void),
        DC_MEMBER_FUNC_CONST_REGISTER(CanGetTextExtent, bool, void),
        DC_MEMBER_FUNC_CONST_REGISTER(GetSize, wxSize, void),
        DC_MEMBER_FUNC_CONST_REGISTER(GetSizeMM, wxSize, void),
        DC_MEMBER_FUNC_CONST_REGISTER(GetDepth, int, void),
        DC_MEMBER_FUNC_CONST_REGISTER(GetPPI, wxSize, void),
        DC_MEMBER_FUNC_CONST_REGISTER(GetResolution, int, void),
        DC_MEMBER_FUNC_CONST_REGISTER(GetContentScaleFactor, double, void),
        DC_MEMBER_FUNC_REGISTER(SetLayoutDirection, void, wxLayoutDirection),
        DC_MEMBER_FUNC_CONST_REGISTER(GetLayoutDirection, wxLayoutDirection, void),
        DC_MEMBER_FUNC_REGISTER(StartDoc, bool, const wxString&),
        DC_MEMBER_FUNC_REGISTER(EndDoc, void, void),
        DC_MEMBER_FUNC_REGISTER(StartPage, void, void),
        DC_MEMBER_FUNC_REGISTER(EndPage, void, void),
        DC_MEMBER_FUNC_REGISTER(CalcBoundingBox, void, wxCoord, wxCoord),
        DC_MEMBER_FUNC_REGISTER(ResetBoundingBox, void, void),
        DC_MEMBER_FUNC_CONST_REGISTER(MinX, wxCoord, void),
        DC_MEMBER_FUNC_CONST_REGISTER(MaxX, wxCoord, void),
        DC_MEMBER_FUNC_CONST_REGISTER(MinY, wxCoord, void),
        DC_MEMBER_FUNC_CONST_REGISTER(MaxY, wxCoord, void),
        DC_MEMBER_FUNC_REGISTER(SetFont, void, const wxFont&),
        DC_MEMBER_FUNC_CONST_REGISTER(GetFont, const wxFont &, void),
        DC_MEMBER_FUNC_REGISTER(SetPen, void, const wxPen &),
        DC_MEMBER_FUNC_CONST_REGISTER(GetPen, const wxPen &, void),
        DC_MEMBER_FUNC_REGISTER(SetBrush, void, const wxBrush &),
        DC_MEMBER_FUNC_CONST_REGISTER(GetBrush, const wxBrush &, void),
        DC_MEMBER_FUNC_REGISTER(SetBackground, void, const wxBrush &),
        DC_MEMBER_FUNC_CONST_REGISTER(GetBackground, const wxBrush &, void),
        DC_MEMBER_FUNC_REGISTER(SetBackgroundMode, void, int),
        DC_MEMBER_FUNC_CONST_REGISTER(GetBackgroundMode, int, void),
        DC_MEMBER_FUNC_REGISTER(SetTextForeground, void, const wxColour &),
        DC_MEMBER_FUNC_CONST_REGISTER(GetTextForeground, const wxColour &, void),
        DC_MEMBER_FUNC_REGISTER(SetTextBackground, void, const wxColour &),
        DC_MEMBER_FUNC_CONST_REGISTER(GetTextBackground, const wxColour &, void),
        DC_MEMBER_FUNC_REGISTER(SetLogicalFunction, void, wxRasterOperationMode),
        DC_MEMBER_FUNC_CONST_REGISTER(GetLogicalFunction, wxRasterOperationMode, void),
        DC_MEMBER_FUNC_CONST_REGISTER(GetCharHeight, wxCoord, void),
        DC_MEMBER_FUNC_CONST_REGISTER(GetCharWidth, wxCoord, void),
        DC_MEMBER_FUNC_CONST_REGISTER(GetFontMetrics, wxFontMetrics, void),
        DC_MEMBER_FUNC_CONST_REGISTER(GetTextExtent, wxSize, const wxString &),
        DC_MEMBER_FUNC_CONST_REGISTER(GetMultiLineTextExtent, wxSize, const wxString &),
        "GetPartialTextExtents", [](wxDC &dc, const wxString &text, sol::this_state s) {
            wxArrayInt widths; //TODO check! , I don't why I wrote that TODO
            bool r = dc.GetPartialTextExtents(text, widths);
            sol::state_view lua{s};
            if (r)
            {
                auto t = lua.create_table(widths.size());
                std::size_t n = 0;
                for (auto &x: widths)
                {
                    t[n] = x;
                    ++n;
                }
                return std::make_tuple(true, (sol::object) t);
            }
            else
            {
                return std::make_tuple(false, sol::make_object(lua, sol::nil));
            }
        },
        DC_MEMBER_FUNC_REGISTER(Clear, void, void),
        DC_MEMBER_FUNC_REGISTER(SetClippingRegion, void, const wxRect&),
        // DC_MEMBER_FUNC_REGISTER(SetDeviceClippingRegion, void, const wxRegion &) // TODO register wxRegion
        DC_MEMBER_FUNC_REGISTER(DestroyClippingRegion, void, void),
        "GetClippingBox", [](wxDC &dc) -> wxRect {
            wxRect r;
            dc.GetClippingBox(r);
            return r;
        },
        DC_MEMBER_FUNC_CONST_REGISTER(DeviceToLogicalX, wxCoord, wxCoord),
        DC_MEMBER_FUNC_CONST_REGISTER(DeviceToLogicalY, wxCoord, wxCoord),
        DC_MEMBER_FUNC_CONST_REGISTER(DeviceToLogicalXRel, wxCoord, wxCoord),
        DC_MEMBER_FUNC_CONST_REGISTER(DeviceToLogicalYRel, wxCoord, wxCoord),
        DC_MEMBER_FUNC_CONST_REGISTER(LogicalToDeviceX, wxCoord, wxCoord),
        DC_MEMBER_FUNC_CONST_REGISTER(LogicalToDeviceY, wxCoord, wxCoord),
        DC_MEMBER_FUNC_CONST_REGISTER(LogicalToDeviceXRel, wxCoord, wxCoord),
        DC_MEMBER_FUNC_CONST_REGISTER(LogicalToDeviceYRel, wxCoord, wxCoord),
        DC_MEMBER_FUNC_REGISTER(SetMapMode, void, wxMappingMode),
        DC_MEMBER_FUNC_CONST_REGISTER(GetMapMode, wxMappingMode, void),
        DC_MEMBER_FUNC_REGISTER(SetUserScale, void, double, double),
        "GetUserScale", [](wxDC &dc) {
            double x, y;
            dc.GetUserScale(&x, &y);
            return std::make_tuple(x, y);
        },
        DC_MEMBER_FUNC_REGISTER(SetLogicalScale, void, double, double),
        "GetLogicalScale", [](wxDC &dc) {
            double x, y;
            dc.GetLogicalScale(&x, &y);
            return std::make_tuple(x, y);
        },
        DC_MEMBER_FUNC_REGISTER(SetLogicalOrigin, void, wxCoord, wxCoord),
        DC_MEMBER_FUNC_CONST_REGISTER(GetLogicalOrigin, wxPoint, void),
        DC_MEMBER_FUNC_REGISTER(SetDeviceOrigin, void, wxCoord, wxCoord),
        DC_MEMBER_FUNC_CONST_REGISTER(GetDeviceOrigin, wxPoint, void),
        DC_MEMBER_FUNC_REGISTER(SetAxisOrientation, void, bool, bool),
        DC_MEMBER_FUNC_CONST_REGISTER(CanUseTransformMatrix, bool, void),
        // DC_MEMBER_FUNC_REGISTER(SetTransformMatrix, bool, const wxAffineMatrix2D &), // TODO register wxAffineMatrix2D
        // DC_MEMBER_FUNC_CONST_REGISTER(GetTransformMatrix, wxAffineMatrix2D, void), // TODO register wxAffineMatrix2D
        DC_MEMBER_FUNC_REGISTER(ResetTransformMatrix, void, void),
        DC_MEMBER_FUNC_REGISTER(SetDeviceLocalOrigin, void, wxCoord, wxCoord),
        // After this point, I register actual drawing API
        DC_MEMBER_FUNC_REGISTER(FloodFill, bool, const wxPoint &, const wxColour &, wxFloodFillStyle),
        DC_MEMBER_FUNC_REGISTER(GradientFillConcentric, void, const wxRect&, const wxColour&, const wxColour&, const wxPoint&),
        DC_MEMBER_FUNC_REGISTER(GradientFillLinear, void, const wxRect&, const wxColour&, const wxColour&, wxDirection),
        "GetPixel", [](wxDC &dc, const wxPoint &pt) {
            wxColour c;
            bool r = dc.GetPixel(pt, &c);
            return std::make_tuple(r, c);
        },
        DC_MEMBER_FUNC_REGISTER(DrawLine, void, const wxPoint&, const wxPoint&),
        DC_MEMBER_FUNC_REGISTER(CrossHair, void, const wxPoint&),
        DC_MEMBER_FUNC_REGISTER(DrawArc, void, const wxPoint&, const wxPoint&, const wxPoint&),
        DC_MEMBER_FUNC_REGISTER(DrawCheckMark, void, const wxRect&),
        DC_MEMBER_FUNC_REGISTER(DrawEllipticArc, void, const wxPoint&, const wxSize&, double, double),
        DC_MEMBER_FUNC_REGISTER(DrawPoint, void, const wxPoint&),
        "DrawLines", [](
            wxDC &dc,
            const Vector<wxPoint> *pts,
            wxCoord offsetx, wxCoord offsety) {
            dc.DrawLines(pts->size(), &((*pts)[0]), offsetx, offsety);
        },
        "DrawPolygon", [](
            wxDC &dc,
            const Vector<wxPoint> *pts,
            wxCoord offsetx, wxCoord offsety,
            wxPolygonFillMode fillstyle) {
            dc.DrawPolygon(pts->size(), &((*pts)[0]), offsetx, offsety, fillstyle);
        },
        "DrawPolyPolygon", [](
            wxDC &dc,
            const Vector<int> *counts,
            const Vector<wxPoint> *points,
            wxCoord offsetx, wxCoord offsety,
            wxPolygonFillMode fillstyle) {
            dc.DrawPolyPolygon(counts->size(), &((*counts)[0]), &((*points)[0]), offsetx, offsety, fillstyle);
        },
        DC_MEMBER_FUNC_REGISTER(DrawRectangle, void, const wxRect &),
        DC_MEMBER_FUNC_REGISTER(DrawRoundedRectangle, void, const wxRect&, double),
        DC_MEMBER_FUNC_REGISTER(DrawCircle, void, const wxPoint&, wxCoord),
        DC_MEMBER_FUNC_REGISTER(DrawEllipse, void, const wxRect &),
        DC_MEMBER_FUNC_REGISTER(DrawBitmap, void, const wxBitmap&, const wxPoint&, bool),
        DC_MEMBER_FUNC_REGISTER(DrawText, void, const wxString&, const wxPoint&),
        DC_MEMBER_FUNC_REGISTER(DrawRotatedText, void, const wxString&, const wxPoint&, double)
        // That's enough, TODO: maybe I can continue other parts later
    );

#undef DC_FUNC_REGISTER
#undef DC_MEMBER_FUNC_REGISTER
#undef DC_MEMBER_FUNC_CONST_REGISTER

}
//END

//BEGIN RegisterWxWidgets
void RegisterWxWidgets(lua_State* L)
{
    RegisterWxFont(L);
    RegisterWxColour(L);
    RegisterWxPen(L);
    RegisterWxBrush(L);
    RegisterWxPoint(L);
    RegisterWxSize(L);
    RegisterWxRect(L);
    RegisterWxBitmap(L);
    RegisterWxDC(L);
}
//END

}
