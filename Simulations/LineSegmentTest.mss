MaSoSi0 0 1678 print = function (...)
    s = ""
    for k, v in ipairs({...}) do
        s = s .. tostring(v) .. "\t"
    end
    raw_output(s)
end

_SIM:clear_polygons()

robot = _SIM:new_polygon()
robot:begin_points()
    do
        local w = 0.25 -- width/2
        robot:add_point(-w, -w)
        robot:add_point(-w, w)
        robot:add_point(w, w)
        robot:add_point(w, -w)
        _G.check = function ()
            local smallest = -1
            _SIM:iterate_polygons(function (polygon)
                local a = robot.position + Vector.new(w, 0)
                local b = robot.position + Vector.new(w + 0.20, 0)
                local intersections =
                    polygon:fast_intersections_with_segment(a, b)
                for k,v in pairs(intersections)
                do
                    local norm = (v-a):norm()
                    if smallest > 0
                    then
                        if norm < smallest then smallest = norm end
                    else
                        smallest = norm
                    end
                end
                return true
            end)
            return smallest
        end
    end
robot:end_points()
robot.color:set(0,0,0,255)
robot.position:set(-4, 0)

function move(d)
    robot.position.x = robot.position.x + d
    refresh_polygon(robot)
end

poly = _SIM:new_polygon()
poly:begin_points()
    poly:add_point(-1, -1)
    poly:add_point(-1, 1)
    poly:add_point(1, 1)
    poly:add_point(1, -1)
poly:end_points()
poly:prepare_fast()

refresh()

v = 0.05
while true
do
    move(v)
    sleep(50)
    refresh_polygon(robot)
    if check() > -1 and check() < 0.10
    then
        break
    end
end
0 