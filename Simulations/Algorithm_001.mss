MaSoSi0 13597 
-- Maze Solver Simulation configuration

maze = {}
maze.wi = 12 -- units
maze.he = 12 -- units
maze.cell_len = 0.20 -- m
maze.wall_wi = 0.02 -- m
maze.wall_template = Polygon.new()
maze.wall_template.color:set(0, 0, 0, 255) -- black
maze.wall_template.border = 0

robot = {}
robot.wi = 0.12 -- m
robot.he = 0.12 -- m
robot.speed = 0.7 -- m/s
robot.angular_speed = 5*math.pi/2 -- rad/s
robot.front_indicator = Color.new(255, 0, 0, 255)

robot.sensors = {}

robot.sensors[1] = {}
robot.sensors[1].type = "prox"
robot.sensors[1].name = "prox_sensor_left"
robot.sensors[1].x = 0
robot.sensors[1].y = robot.he/2
robot.sensors[1].theta = math.pi/2 -- ray direction
robot.sensors[1].d = 0.80 -- ray length

robot.sensors[2] = {}
robot.sensors[2].type = "prox"
robot.sensors[2].name = "prox_sensor_front"
robot.sensors[2].x = robot.wi/2
robot.sensors[2].y = 0
robot.sensors[2].theta = 0
robot.sensors[2].d = 0.80

robot.sensors[3] = {}
robot.sensors[3].type = "prox"
robot.sensors[3].name = "prox_sensor_right"
robot.sensors[3].x = 0
robot.sensors[3].y = -robot.he/2
robot.sensors[3].theta = 3*math.pi/2
robot.sensors[3].d = 0.80

robot.sensors[4] = {}
robot.sensors[4].type = "ir"
robot.sensors[4].name = "ir_sensor"
robot.sensors[4].x = 0
robot.sensors[4].y = robot.he * 0.4

rules = {}
rules.start_pt = { x=1, y=1 }
rules.start_theta = 3*math.pi/2

function make_endpoints_from_region(x0, y0, x1, y1)
    local i = 1
    local res = {}
    for x = x0,x1
    do
        for y = y0,y1
        do
            res[i] = { x=x, y=y }
            i = i + 1
        end
    end
end

rules.end_pts =
    make_endpoints_from_region(
        maze.wi-1, maze.wi,
        maze.he-1, maze.he)

opts = {}

opts.move = {}
opts.move.time_step = 10 -- ms, used for both collisions and smooth moves
opts.move.smooth = true

opts.rotate = {}
opts.rotate.time_step = 10
opts.rotate.smooth = true

-- End Maze Solver Simulation configuration

_SIM:clear_polygons() -- clear everything

-- some helper functions

_print = print -- store old print

print = function (...)
    s = ""
    for k, v in ipairs({...}) do
        s = s .. tostring(v) .. "\t"
    end
    raw_output(s)
end

-- now let's create a maze

-- maze generator algorithm
function create_maze(WI, HE, carve_in_out)
    local seed = os.time()
    math.randomseed(seed)
    print("random seed: " .. seed)

    local N = 1
    local S = 2
    local E = 4
    local W = 8

    local DX = {  }

    DX[N] = 0
    DX[E] = 1
    DX[W] = -1
    DX[S] = 0

    local DY = {  }

    DY[N] = -1
    DY[E] = 0
    DY[W] = 0
    DY[S] = 1

    local X = {  }    -- opposite directions

    X[N] = S
    X[E] = W
    X[W] = E
    X[S] = N

    local GRID = {  }
    for i = 1, WI do
        GRID[i] = {  }
        for j = 1, HE do
            GRID[i][j] = 0
        end
    end

    local function get_randomized_directions()
        local RESULT = { N, S, E, W }
        local j
        for i = 4, 2, -1 do
            j = math.random(i)
            RESULT[i], RESULT[j] = RESULT[j], RESULT[i]
        end
        return RESULT
    end

    local function carve_passages_from(CX, CY)
        local DIRECTIONS = get_randomized_directions()

        for key, DIRECTION in pairs(DIRECTIONS) do
            local NX = CX + DX[DIRECTION]
            local NY = CY + DY[DIRECTION]
            if
                NX >= 1 and NX <= WI and
                NY >= 1 and NY <= HE and    -- check boundaries
                GRID[NX][NY] == 0           -- not visited
            then
                GRID[CX][CY] = GRID[CX][CY] | DIRECTION
                GRID[NX][NY] = GRID[NX][NY] | X[DIRECTION]
                carve_passages_from(NX, NY)
            end
        end
    end

	carve_passages_from(1, 1)

    if carve_in_out then
        GRID[WI][HE] = GRID[WI][HE] | S
        GRID[1][1] = GRID[1][1] | N
    end

    return GRID
end

-- GRID:        a maze grid generated before
-- OBSTACLE:    obstacle function, it should place the needed obstacle to its
--              place, in the form: function OBSTACLE(X, Y, DIRECTION)
--              where DIRECION is
--                  1 for north,
--                  2 for south
--                  4 for east and
--                  8 for west
function draw_maze_grid(GRID, OBSTACLE)

    local WI = #GRID
    local HE = #(GRID[1])

    -- to avoid duplicates, go only one way, south-east
    for i = 1, WI do
        for j = 1, HE do
            if GRID[i][j] & 4 == 0 then
                OBSTACLE(i, j, 4)
            end
            if GRID[i][j] & 2 == 0 then
                OBSTACLE(i, j, 2)
            end
        end
    end

    -- check the norths of the first row
    for i = 1, WI do
        if GRID[i][1] & 1 == 0 then
            OBSTACLE(i, 1, 1)
        end
    end

    -- check the wests of the first column
    for j = 1, HE do
        if GRID[1][j] & 8 == 0 then
            OBSTACLE(1, j, 8)
        end
    end

end

-- It returns two functions. The first one is the obstacle generator function
-- and it's to be passed as the second argument of draw_maze_grid.
-- The latter one can be used for translating grid coords to world coords.
function default_maze_obstacle_func(
    primitive_template,
    maze_pos,
    maze_wi,
    maze_he,
    wall_wi,
    cell_len)

    local template = Polygon.new(primitive_template)
    template:begin_points()
        template:add_point(-(cell_len+wall_wi) / 2.0, -wall_wi / 2.0)
        template:add_point(-(cell_len+wall_wi) / 2.0, wall_wi / 2.0)
        template:add_point((cell_len+wall_wi) / 2.0, wall_wi / 2.0)
        template:add_point((cell_len+wall_wi) / 2.0, -wall_wi / 2.0)
    template:end_points()

    return
    function (X, Y, DIRECTION)
        obst = _SIM:new_polygon()
        obst:copy_from(template)

        local cell_origin = Vector.new(
            (2 * X - 1) * cell_len / 2,
            -(2 * Y - 1) * cell_len / 2
        ) + maze_pos

        if      DIRECTION == 1 then -- north
            obst.position =
                cell_origin + Vector.new(0.0, cell_len / 2.0)
        elseif  DIRECTION == 2 then -- south
            obst.position =
                cell_origin + Vector.new(0.0, - cell_len / 2.0)
        elseif  DIRECTION == 4 then -- east
            obst.position =
                cell_origin + Vector.new(cell_len / 2.0, 0.0)
            obst.rotation = math.pi / 2.0
        elseif  DIRECTION == 8 then -- west
            obst.position =
                cell_origin + Vector.new(- cell_len / 2.0, 0.0)
            obst.rotation = math.pi / 2.0
        end
        obst.name = "maze_wall"
        obst:prepare_fast() -- they are static, so it's a good idea 
                            -- to prepare them
    end,
    function (X, Y)
        return Vector.new(
            (2 * X - 1) * cell_len / 2,
            -(2 * Y - 1) * cell_len / 2
        ) + maze_pos
    end
end

-- I want maze to be at the origin
-- So, we should calculate its size first

maze_real_size = {
    wi = maze.wi * maze.cell_len + maze.wall_wi,
    he = maze.he * maze.cell_len + maze.wall_wi
}

maze_grid = create_maze(maze.wi, maze.he)
obstacle_func, cell_origin = default_maze_obstacle_func(
    maze.wall_template,
    Vector.new( -- maze position
        -maze_real_size.wi/2,
        maze_real_size.he/2
    ),
    maze.wi,
    maze.he,
    maze.wall_wi,
    maze.cell_len
)

draw_maze_grid(maze_grid, obstacle_func)

-- Now let's create the robot

robot_polygon = _SIM:new_polygon()
robot_polygon:begin_points()
    robot_polygon:add_point(robot.wi/2, robot.he/2)
    robot_polygon:add_point(robot.wi/2, -robot.he/2)
    robot_polygon:add_point(-robot.wi/2, -robot.he/2)
    robot_polygon:add_point(-robot.wi/2, robot.he/2)
robot_polygon:end_points()

robot_polygon.position = cell_origin(rules.start_pt.x, rules.start_pt.y)
robot_polygon.rotation = rules.start_theta

prox_sensor_segment_pts = {}

function update_prox_sensor(sensor_obj)
    local a = Vector.new()
    local b = Vector.new()
    a = Vector.new(sensor_obj.x, sensor_obj.y):rotated(robot_polygon.rotation)
        + robot_polygon.position
    b = a + Vector.new(sensor_obj.d, 0):rotated(
        sensor_obj.theta + robot_polygon.rotation)
    sensor_obj.a = a
    sensor_obj.b = b
    -- print("sensor: " .. sensor_obj.name .. " a: " .. a:repr() .. " b: " .. b:repr())
end

function update_prox_sensors()
    for k, v in pairs(robot.sensors)
    do
        if v.type == "prox"
        then
            update_prox_sensor(v)
        end
    end
end

update_prox_sensors()

if robot.front_indicator ~= nil
then
    indicator_polygon = _SIM:new_polygon()
    indicator_polygon.enabled = false
    indicator_polygon.color = robot.front_indicator
    indicator_polygon.z_order = 1
    local H = robot.he
    local W = robot.wi
    local h = H * 0.50
    local w = W * 0.20
    indicator_polygon:begin_points()
        indicator_polygon:add_point(W/2-w, h/2)
        indicator_polygon:add_point(W/2-w, -h/2)
        indicator_polygon:add_point(W/2, -h/2)
        indicator_polygon:add_point(W/2, h/2)
    indicator_polygon:end_points()
end

-- call just after the rotation or position of the robot is changed
-- it DOES refresh
function update_robot()
    refresh_polygon(robot_polygon)
    if indicator_polygon ~= nil
    then
        indicator_polygon.rotation = robot_polygon.rotation
        indicator_polygon.position = robot_polygon.position
        refresh_polygon(indicator_polygon)
    end
end

update_robot()

-- returns false when collides
function move(dist)
    local u = Vector.new(
        math.cos(robot_polygon.rotation),
        math.sin(robot_polygon.rotation)) -- unit vector along the front
    local s = robot.speed -- speed
    local v = u * s -- velocity
    local left = dist
    local cont = true
    local initial_pos = Vector.new(robot_polygon.position)
    local deltaT = opts.move.time_step
    local deltaS = s * deltaT / 1000 -- ms to s conversion
    local deltaX = v * deltaT / 1000 -- ms to s conversion
    local smooth = opts.move.smooth
    while cont
    do
        local collision = false
        _SIM:collision_check(robot_polygon, function (p)
                if  p ~=robot_polygon  -- ensure not itself
                    and p.enabled then
                    collision = true
                    return false
                end
                return true
            end)
        if collision then
            return false
        end
        if (left - deltaS) < 0
        then
            sleep(math.floor(left/s*1000))
            robot_polygon.position = initial_pos + u * dist
            cont=false
        else
            sleep(deltaT)
            robot_polygon.position = robot_polygon.position + deltaX
            left = left - deltaS
        end
        if smooth then update_robot() end
    end
    update_prox_sensors()
    update_robot()
    return true
end

math.sgn = function (x)
    if x > 0 then
        return 1
    elseif x < 0 then
        return -1
    else
        return 0
    end
end

-- returns false when collides
function rotate(theta)
    local omega = robot.angular_speed * math.sgn(theta) -- angular speed
    local left = theta
    local cont = true
    local initial_theta = robot_polygon.rotation
    local deltaT = opts.rotate.time_step
    local deltaTheta = omega * deltaT / 1000 -- ms to s conversion
    local smooth = opts.rotate.smooth
    while cont
    do
        local collision = false
        _SIM:collision_check(robot_polygon, function (p)
                if  p ~=robot_polygon  -- ensure not itself
                    and p.enabled then
                    collision = true
                    return false
                end
                return true
            end)
        if collision then
            return false
        end
        if (left - deltaTheta) * math.sgn(theta) < 0
        then
            sleep(math.floor(left/omega*1000) * math.sgn(theta))
            robot_polygon.rotation = initial_theta + theta
            cont=false
        else
            sleep(deltaT)
            robot_polygon.rotation = robot_polygon.rotation + deltaTheta
            left = left - deltaTheta
        end
        if smooth then update_robot() end
    end
    robot_polygon.rotation = math.fmod(robot_polygon.rotation, math.pi * 2)
        -- normalize the rotation value
    update_prox_sensors()
    update_robot() -- do this anyway
    return true
end

function prox(sensor)
    local smallest_distance = -1
    _SIM:iterate_polygons(function (polygon)
        if polygon.name == "maze_wall"
        then
            for k,v in pairs(
                polygon:fast_intersections_with_segment(sensor.a, sensor.b))
            do
                local norm = (sensor.a - v):norm()
                -- we have Vector's here: v
                if smallest_distance > 0
                then
                    if norm < smallest_distance
                    then
                        smallest_distance = norm
                    end
                else
                    smallest_distance = norm
                end
            end
        end
        return true
    end)
    return smallest_distance
end

-- register sensor functions:
--    prox_left
--    prox_right
--    prox_front
for k, v in pairs(robot.sensors)
do
    if v.type == "prox"
    then
        _G["prox_" .. v.name:sub(13)] = function () return prox(v) end
    end
end

refresh()

-- Uncomment the following code if you want to perform a quick check
-- what is published or what is hidden in _G
--[[

for k, v in pairs(_G)
do
    raw_output("key: " .. tostring(k) .. " value: " .. tostring(v))
end

]]

1702 
danger_level = (maze.cell_len - robot.wi) / 2 * 1.10 -- error
step_wi = maze.cell_len

X = 1 -- initial vars
Y = 1

-- some constants
NORTH   = 0
EAST    = 1
SOUTH   = 2
WEST    = 3

function OPPOSITE(x)
    return (x + 2) % 4
end

ORIENT = SOUTH -- by default

-- move forward
function step()
    if not move(step_wi) then
        return false
    end
    if      ORIENT == NORTH then Y = Y - 1
    elseif  ORIENT == EAST  then X = X + 1
    elseif  ORIENT == SOUTH then Y = Y + 1
    elseif  ORIENT == WEST  then X = X - 1 end
    return true
end

-- rotate +
function turn_left()
    if not rotate(math.pi/2) then
        return false
    end
    ORIENT = (ORIENT - 1) % 4
    return true
end

-- rotate -
function turn_right()
    if not rotate(-math.pi/2) then
        return false
    end
    ORIENT = (ORIENT + 1) % 4
    return true
end

-- use prox sensor to check if front is full
function check_front()
    local d = prox_front()
    if d > 0 and d < danger_level then return true end
    return false
end

-- use prox sensor to check if left is full
function check_left()
    local d = prox_left()
    if d > 0 and d < danger_level then return true end
    return false
end

-- use prox sensor to check if right is full
function check_right()
    local d = prox_right()
    if d > 0 and d < danger_level then return true end
    return false
end

local sth = true

while true
do
    if check_front() then
        local old_orient = ORIENT
        local I = 0
        while
            ORIENT == old_orient or
            (I < 3 and ORIENT == OPPOSITE(old_orient)) or
            check_front()
            do
            turn_left()
            I = I + 1
        end
    end
    step()
end
0 