MaSoSi0 5145 
-- resources
-- https://coronalabs.com/blog/2014/09/30/tutorial-how-to-shuffle-table-items/
-- http://weblog.jamisbuck.org/2010/12/27/maze-generation-recursive-backtracking

function create_maze(WI, HE)
    math.randomseed(os.time())

    local N = 1
    local S = 2
    local E = 4
    local W = 8

    local DX = {  }

    DX[N] = 0
    DX[E] = 1
    DX[W] = -1
    DX[S] = 0

    local DY = {  }

    DY[N] = -1
    DY[E] = 0
    DY[W] = 0
    DY[S] = 1

    local X = {  }    -- opposite directions

    X[N] = S
    X[E] = W
    X[W] = E
    X[S] = N

    local GRID = {  }
    for i = 1, WI do
        GRID[i] = {  }
        for j = 1, HE do
            GRID[i][j] = 0
        end
    end

    function get_randomized_directions()
        local RESULT = { N, S, E, W }
        local j
        for i = 4, 2, -1 do
            j = math.random(i)
            RESULT[i], RESULT[j] = RESULT[j], RESULT[i]
        end
        return RESULT
    end

    function carve_passages_from(CX, CY)
        local DIRECTIONS = get_randomized_directions()

        for key, DIRECTION in pairs(DIRECTIONS) do
            local NX = CX + DX[DIRECTION]
            local NY = CY + DY[DIRECTION]
            if
                NX >= 1 and NX <= WI and
                NY >= 1 and NY <= HE and    -- check boundaries
                GRID[NX][NY] == 0           -- not visited
            then
                GRID[CX][CY] = GRID[CX][CY] | DIRECTION
                GRID[NX][NY] = GRID[NX][NY] | X[DIRECTION]
                carve_passages_from(NX, NY)
            end
        end
    end

	carve_passages_from(1, 1)
    GRID[WI][HE] = GRID[WI][HE] | S
    GRID[1][1] = GRID[1][1] | N

    return GRID
end

-- GRID:        a maze grid generated before
-- OBSTACLE:    obstacle function, it should place the needed obstacle to its
--              place, in the form: function OBSTACLE(X, Y, DIRECTION)
--              where DIRECION is
--                  1 for north,
--                  2 for south
--                  4 for east and
--                  8 for west
function draw_maze_grid(GRID, OBSTACLE)

    local WI = #GRID
    local HE = #(GRID[1])

    -- to avoid duplicates, go only one way, south-east
    for i = 1, WI do
        for j = 1, HE do
            if GRID[i][j] & 4 == 0 then
                OBSTACLE(i, j, 4)
            end
            if GRID[i][j] & 2 == 0 then
                OBSTACLE(i, j, 2)
            end
        end
    end

    -- check the norths of the first row
    for i = 1, WI do
        if GRID[i][1] & 1 == 0 then
            OBSTACLE(i, 1, 1)
        end
    end

    -- check the wests of the first column
    for j = 1, HE do
        if GRID[1][j] & 8 == 0 then
            OBSTACLE(1, j, 8)
        end
    end

end

function default_maze_obstacle_func(
    primitive_template,
    maze_pos,
    maze_wi,
    maze_he,
    wall_wi,
    cell_len)

    local template = Polygon.new(primitive_template)
    template:begin_points()
        template:add_point(-(cell_len+2*wall_wi) / 2.0, -wall_wi / 2.0)
        template:add_point(-(cell_len+2*wall_wi) / 2.0, wall_wi / 2.0)
        template:add_point((cell_len+2*wall_wi) / 2.0, wall_wi / 2.0)
        template:add_point((cell_len+2*wall_wi) / 2.0, -wall_wi / 2.0)
    template:end_points()

    return
    function (X, Y, DIRECTION)
        obst = _SIM:new_polygon()
        obst:copy_from(template)

        cell_origin = Vector.new(
            (X + 0.5) * cell_len + (X + 1.0) * wall_wi,
            -((Y + 0.5) * cell_len + (Y + 1.0) * wall_wi)
        )
        cell_origin = cell_origin + maze_pos

        if      DIRECTION == 1 then -- north
            obst.position =
                cell_origin + Vector.new(0.0, (cell_len + wall_wi) / 2.0)
        elseif  DIRECTION == 2 then -- south
            obst.position =
                cell_origin + Vector.new(0.0, - (cell_len + wall_wi) / 2.0)
        elseif  DIRECTION == 4 then -- east
            obst.position =
                cell_origin + Vector.new((cell_len + wall_wi) / 2.0, 0.0)
            obst.rotation = math.pi / 2.0
        elseif  DIRECTION == 8 then -- west
            obst.position =
                cell_origin + Vector.new(- (cell_len + wall_wi) / 2.0, 0.0)
            obst.rotation = math.pi / 2.0
        end
    end
end

function maze_obstacle_func_debugger(X, Y, DIRECTION)
    local msg = "Put a point at: (" .. X .. ", " .. Y .. ") "
    if      DIRECTION == 1 then -- north
        msg = msg .. "NORTH"
    elseif  DIRECTION == 2 then -- south
        msg = msg .. "SOUTH"
    elseif  DIRECTION == 4 then -- east
        msg = msg .. "EAST"
    elseif  DIRECTION == 8 then -- west
        msg = msg .. "WEST"
    else
        msg = msg .. "?"
    end
end

-- now, let's make it

_SIM:clear_polygons() -- some cleaning

maze_wi = 12
maze_he = 12

maze = create_maze(maze_wi, maze_he)

temp_obst = Polygon.new()
temp_obst.color = Color.new(100, 0, 0)
temp_obst.border_color = Color.new(100, 0, 0)

draw_maze_grid(
    maze,
    default_maze_obstacle_func(
        temp_obst,
        Vector.new(-3.0, 3.0),
        maze_wi,
        maze_he,
        0.2,
        0.5
    ))

refresh()
0 0 