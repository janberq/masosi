MaSoSi0 423 -- VARIABLES

maze = {
    wi = 8,
    he = 8,
    cell_wi = 0.20,
    cell_he = 0.20,
    wall_wi = 0.02
}

rules = {
    
}

robot = {
    wi = 0.12,
    he = 0.12,
    sensors = {
        { -- prox_sensor_left
            type = "prox_sensor",
            
        },
        { -- prox_sensor_right
        
        },
        { -- prox_sensor_front
        
        },
        { -- IR_sensor
        
        }
    }
}
0 0 