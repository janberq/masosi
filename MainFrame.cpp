#include "MainFrame.hpp"
#include "Simulation.hpp"
#include "CompoundView.hpp"
#include "MSSFile.hpp"

#include <wx/menu.h>
#include <wx/msgdlg.h>
#include <wx/event.h>
#include <wx/filedlg.h>
#include <wx/sizer.h>
#include <wx/checklst.h>
#include <wx/button.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/aui/aui.h>

#include <cassert>
#include <unordered_map>
#include <string>
#include <algorithm>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

namespace masosi {

//BEGIN auxiliary stuff

static inline wxString toWxString(const std::string &str) {
    return wxString::FromUTF8(str.c_str(), str.size());
}

static inline std::string toStdString(const wxString &str) {
    return str.mb_str(wxConvUTF8).data();
}

#define UNUSED(x)

//END

static wxString MENU_PAUSE_TEXT = _("Pause\tF8");
static wxString MENU_RESUME_TEXT = _("Resume\tF8");
static wxString MENU_START_TEXT = _("Start\tF11");
static wxString MENU_STOP_TEXT = _("Stop\tF11");

struct MainFrame::Impl
{
public:

    Impl(MainFrame *frm);

    virtual ~Impl();

    bool Create(wxWindow *parent, wxWindowID id);

private:
    MainFrame *frm_ {nullptr};

    wxAuiManager mgr_;
    wxAuiNotebook *notebook_;

    struct Document
    {
        long ID;
        std::string path {""};
        std::string name {toStdString(_("new file"))};
        CompoundView *compoundView {nullptr};
        std::string latestStatusText {};
        bool isModified {false};

        Document() {  }
        Document(
            long _ID,
            const std::string &_path,
            CompoundView *_compoundview) :
            ID {_ID}, path{_path}, compoundView {_compoundview}
        {  }

        wxString MakeTitle(bool asterisk = true)
        {
            return wxString::Format(
                _("%s [id: %ld]%s"),
                name,
                ID,
                asterisk && isModified ? " (*)" : "");
        }
    };
    std::unordered_map<long, Document> documents_;
    long active_document_ {-1 /* no active file */};

    Document *GetDocumentFromWindow(wxWindow *win);
    Document *GetDocumentFromID(long n);
    Document *GetActiveDocument();

    long AcquireDocID() const;

    void UpdateDocTitle(Document *doc);

    void DoCreateNew();

    void DoOpen();
    void DoOpen(const std::string &path);

    /**
     * DoSave(...) return false if the user cancelled the dialog.
     */
    bool DoSave();
    bool DoSave(long n);
    bool DoSave(Document *doc);

    void DoSaveAs();
    void DoSaveAs(long n);
    void DoSaveAs(Document *doc);

    void DoSaveAll();

    void DoClose();
    void DoClose(Document *doc);

    /**
     * DoCloseAll() returns false if the user cancelled the dialog.
     */
    bool DoCloseAll();

    void DoQuit();

    void DoClearPolygons();
    void DoClearOutput();
    void DoClear();

    void DoRefreshPolygons();
    void DoRefreshDrawing();
    void DoRefresh();

    void DoResetLua1();
    void DoResetLua2();
    void DoResetLua();

    void DoSaveCurrentScript();
    void DoRestoreCurrentScript();
    void DoClearCurrentScript();

    void DoSaveCurrentScriptToFile();
    void DoLoadCurrentScriptFromFile();

    void DoSaveAllScripts();

    void DoShowTopics();
    void DoShowAbout();

    void DoSimulationStartStop();
    void DoSimulationPauseResume();

    class MyEventHandler : public CompoundView::EventHandler
    {
        Impl *impl_;
        long n_;
    public:
        MyEventHandler(Impl *impl, long n) :
            impl_{impl}, n_{n}
        {  }

        bool OnStopped() override;
        bool OnStarted() override;
        bool OnPaused() override;
        bool OnResumed() override;
        bool OnCurrentScriptChanged(
            Simulator::EScriptType old_script,
            Simulator::EScriptType new_script) override;
        bool OnScriptModified(Simulator::EScriptType script) override;
        bool OnScriptRestored(Simulator::EScriptType script) override;
        bool OnScriptSaved(Simulator::EScriptType script) override;
        bool OnStatusChanged(const std::string & status) override;
        bool OnInputRequested(
            const std::string & ir_type,
            const std::list<std::string> & ir_strings) override;
        bool OnOutput(const std::string & output) override;

    };

    friend class MyEventHandler;

    CompoundView::EventHandler *CreateEventHandler(long n);

    CompoundView *CreateNewCompoundView(const wxString &title, long n);

    void SetStatusText(const wxString &text);

    /* menu bar stuff */

    wxMenu *simulation_menu_ {nullptr}, *script_menu_ {nullptr};

#if I_DECIDE_USING_TOOLBAR
    /* toolbar stuff */
    wxAuiToolBar *file_toolbar_ {nullptr}, *simulation_toolbar_ {nullptr};
#endif

    wxWindowID ID_SIMULATION_CLEARPOLYGONS;
    wxWindowID ID_SIMULATION_START;
    wxWindowID ID_SIMULATION_PAUSE;

    wxWindowID ID_SCRIPT_SAVE;

    /* miscelleneous */
    wxString default_directory_ {""};
};

MainFrame::Impl::Impl(MainFrame* frm) : frm_{frm}
{
}

bool MainFrame::Impl::Create(wxWindow* parent, wxWindowID id)
{
    if (!frm_->wxFrame::Create(parent, id, _("MaSoSi")))
        return false;

    frm_->SetSize(1024, 640);
    frm_->CenterOnScreen();

    /* BEGIN Menubar Creation */ {

        wxMenuBar *menubar = new wxMenuBar;

        /* BEGIN File Menu */ {

            // wxWindowID ID_SAVECOPYAS      = menubar->NewControlId();
            wxWindowID ID_SAVEALL         = menubar->NewControlId();

            wxMenu *menu = new wxMenu;

            menu->Append(wxID_NEW,      _("&New\tCTRL+N"));
            menu->AppendSeparator();
            menu->Append(wxID_OPEN,     _("&Open\tCTRL+O"));
            // TODO some kind of recent files list?
            menu->AppendSeparator();
            menu->Append(wxID_SAVE,     _("&Save\tCTRL+S"));
            menu->Append(wxID_SAVEAS,   _("Save &As...\tCTRL+SHIFT+S"));
            // menu->Append(ID_SAVECOPYAS, _("Sa&ve Copy As..."));
            menu->Append(ID_SAVEALL,    _("Save A&ll\tCTRL+L"));
            menu->AppendSeparator();
            menu->Append(wxID_CLOSE,        _("&Close\tCTRL+W"));
            menu->Append(wxID_CLOSE_ALL,    _("Clos&e All"));
            menu->AppendSeparator();
            menu->Append(wxID_EXIT,     _("&Quit\tALT+F4"));

            menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoCreateNew();
            }, wxID_NEW);

            menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoOpen();
            }, wxID_OPEN);

            menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoSave();
            }, wxID_SAVE);

            menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoSaveAs();
            }, wxID_SAVEAS);

            /*
            menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                // TODO implement File > Save Copy As...
            }, ID_SAVECOPYAS);
            */

            menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoSaveAll();
            }, ID_SAVEALL);

            menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoClose();
            }, wxID_CLOSE);

            menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoCloseAll();
            }, wxID_CLOSE_ALL);

            menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoQuit();
            }, wxID_EXIT);

            menubar->Append(menu, _("&File"));
            
        } /* END */

        /* BEGIN Simulation Menu */ {

            ID_SIMULATION_CLEARPOLYGONS     = menubar->NewControlId();
            ID_SIMULATION_START             = menubar->NewControlId();
            ID_SIMULATION_PAUSE             = menubar->NewControlId();

            wxWindowID ID_CLEAROUTPUT       = menubar->NewControlId();
            wxWindowID ID_CLEAR             = menubar->NewControlId();
            wxWindowID ID_REFRESHPOLYGONS   = menubar->NewControlId();
            wxWindowID ID_REFRESHDRAWING    = menubar->NewControlId();
            wxWindowID ID_REFRESH           = menubar->NewControlId();
            wxWindowID ID_RESETLUA1         = menubar->NewControlId();
            wxWindowID ID_RESETLUA2         = menubar->NewControlId();
            wxWindowID ID_RESETLUAALL       = menubar->NewControlId();

            simulation_menu_ = new wxMenu;

            simulation_menu_->Append(ID_SIMULATION_START,   MENU_START_TEXT);
            simulation_menu_->Append(ID_SIMULATION_PAUSE,   MENU_PAUSE_TEXT);
            simulation_menu_->AppendSeparator();
            simulation_menu_->Append(ID_SIMULATION_CLEARPOLYGONS,      _("Clear Po&lygons\tCTRL+SHIFT+E"));
            simulation_menu_->Append(ID_CLEAROUTPUT,    _("Clear &Output\tCTRL+SHIFT+X"));
            simulation_menu_->Append(ID_CLEAR,          _("&Clear (all of the above)\tCTRL+F2"));
            simulation_menu_->AppendSeparator();
            simulation_menu_->Append(ID_REFRESHPOLYGONS,    _("Refresh Pol&ygons\tCTRL+F5"));
            simulation_menu_->Append(ID_REFRESHDRAWING,     _("Refresh Dra&wing\tCTRL+SHIFT+F5"));
            simulation_menu_->Append(ID_REFRESH,            _("&Refresh (all of the above)\tF5"));
            simulation_menu_->AppendSeparator();
            simulation_menu_->Append(ID_RESETLUA1,      _("Reset Lua of the mai&n thread\tCTRL+F3"));
            simulation_menu_->Append(ID_RESETLUA2,      _("Reset Lua of the s&econdary thread\tCTRL+F4"));
            simulation_menu_->Append(ID_RESETLUAALL,    _("R&eset Lua (all of the above)\tCTRL+SHIFT+F4"));

            simulation_menu_->Enable(ID_SIMULATION_PAUSE, false);

            simulation_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoSimulationStartStop();
            }, ID_SIMULATION_START);

            simulation_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoSimulationPauseResume();
            }, ID_SIMULATION_PAUSE);

            simulation_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoClearPolygons();
            }, ID_SIMULATION_CLEARPOLYGONS);

            simulation_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoClearOutput();
            }, ID_CLEAROUTPUT);

            simulation_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoClear();
            }, ID_CLEAR);

            simulation_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoRefreshPolygons();
            }, ID_REFRESHPOLYGONS);

            simulation_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoRefreshDrawing();
            }, ID_REFRESHDRAWING);

            simulation_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoRefresh();
            }, ID_REFRESH);

            simulation_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoResetLua1();
            }, ID_RESETLUA1);

            simulation_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoResetLua2();
            }, ID_RESETLUA2);

            simulation_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoResetLua();
            }, ID_RESETLUAALL);

            menubar->Append(simulation_menu_, _("&Simulation"));
            
        } /* END */

        /* BEGIN Script Menu */ {

            ID_SCRIPT_SAVE                      = menubar->NewControlId();

            wxWindowID ID_RESTORE               = menubar->NewControlId();
            wxWindowID ID_CLEAR                 = menubar->NewControlId();
            wxWindowID ID_SAVETOFILE            = menubar->NewControlId();
                /* windows fucks up w/ standard version */
            wxWindowID ID_LOADFROMFILE          = menubar->NewControlId();
            wxWindowID ID_SAVEALLSCRIPTS        = menubar->NewControlId();

            script_menu_ = new wxMenu;

            script_menu_->Append(ID_SCRIPT_SAVE,    _("&Save\tCTRL+ALT+S"));
            script_menu_->Append(ID_RESTORE,        _("&Restore\tCTRL+ALT+R"));
            script_menu_->Append(ID_CLEAR,          _("&Clear\tCTRL+ALT+C"));
            script_menu_->AppendSeparator();
            script_menu_->Append(ID_SAVETOFILE,     _("Save to &File\tCTRL+SHIFT+ALT+S"));
            script_menu_->Append(ID_LOADFROMFILE,   _("&Load from File\tCTRL+SHIFT+ALT+L"));
            script_menu_->AppendSeparator();
            script_menu_->Append(ID_SAVEALLSCRIPTS, _("S&ave All Scripts\tCTRL+ALT+A"));

            script_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                auto doc = GetActiveDocument();
                if (doc)
                    doc->compoundView->Actions()->SaveCurrentScript();
            }, ID_SCRIPT_SAVE);

            script_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                auto doc = GetActiveDocument();
                if (doc)
                    doc->compoundView->Actions()->RestoreCurrentScript();
            }, ID_RESTORE);

            script_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoClearCurrentScript();
            }, ID_CLEAR);

            script_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoSaveCurrentScriptToFile();
            }, ID_SAVETOFILE);

            script_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoLoadCurrentScriptFromFile();
            }, ID_LOADFROMFILE);

            script_menu_->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoSaveAllScripts();
            }, ID_SAVEALLSCRIPTS);

            menubar->Append(script_menu_, _("Scrip&t"));
        } /* END */

        /* BEGIN Help menu */ {

            wxWindowID ID_HELP      = wxID_HELP;
            wxWindowID ID_ABOUT     = wxID_ABOUT;

            wxMenu *menu = new wxMenu;

            menu->Append(ID_HELP,   _("&Topics\tF1"));
            menu->Append(ID_ABOUT,  _("&About\tCTRL+F1"));

            menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoShowTopics();
            }, ID_HELP);

            menu->Bind(wxEVT_MENU, [this](wxCommandEvent &) {
                DoShowAbout();
            }, ID_ABOUT);

            menubar->Append(menu, _("&Help"));

        } /* END */

        frm_->SetMenuBar(menubar);

    } /* END */

    /* BEGIN Statusbar Creation */ {
        frm_->CreateStatusBar(3);
        frm_->SetStatusWidths(3, (const int []){ 300, 100, -1 });
        frm_->SetStatusText("Status:", 1);
    } /* END */

    mgr_.SetManagedWindow(frm_);

    notebook_ = new wxAuiNotebook(frm_, wxID_ANY);
    notebook_->SetArtProvider(new wxAuiGenericTabArt);
    notebook_->Bind(wxEVT_AUINOTEBOOK_PAGE_CHANGED,
        [this](wxAuiNotebookEvent &evt) {
        auto win = notebook_->GetPage(evt.GetSelection());
        auto it = std::find_if(
            documents_.begin(),
            documents_.end(),
            [win](
                const std::pair<const long, MainFrame::Impl::Document>
                    &document)
            {
                return document.second.compoundView == win;
            });
        if (it != documents_.end())
        {
            active_document_ = it->second.ID;
            simulation_menu_->Enable(   // TODO Update here accordingly
                                        // when the page is changed
                ID_SIMULATION_PAUSE,
                it->second.compoundView->GetSimulator()->IsRunning());
            simulation_menu_->SetLabel(
                ID_SIMULATION_PAUSE,
                it->second.compoundView->GetSimulator()->IsPaused()
                    ? MENU_RESUME_TEXT : MENU_PAUSE_TEXT);
            simulation_menu_->SetLabel(
                ID_SIMULATION_START,
                it->second.compoundView->GetSimulator()->IsRunning()
                    ? MENU_STOP_TEXT : MENU_START_TEXT);
            SetStatusText(it->second.latestStatusText);
        }
        else
        {
            active_document_ = -1;
        }
    });
    notebook_->Bind(wxEVT_AUINOTEBOOK_PAGE_CLOSE,
        [this](wxAuiNotebookEvent &evt) {
        auto win = dynamic_cast<CompoundView *>(
            notebook_->GetPage(evt.GetSelection()));
        auto doc = GetDocumentFromWindow(win);
        win->Actions()->SaveAllScripts(CompoundView::AF_DEFAULT);
        if (doc->isModified)
        {
            auto r = wxMessageBox(
                _("Simulation is modified, do you want to save it?"),
                wxString::Format(_("Close Page [id: %ld]"), doc->ID),
                wxYES | wxNO | wxCANCEL | wxYES_DEFAULT | wxCENTRE,
                win
            );
            if (r == wxYES)
            {
                if (!DoSave(doc))
                {
                    evt.Veto();
                    return ;
                }
            }
            else if (r == wxCANCEL)
            {
                evt.Veto();
                return ;
            }
            else
            {
                // just discard it
            }
        }
        active_document_ = -1; // windows may fuck w/o this
        SetStatusText("");
        documents_.erase(doc->ID);
    });

    frm_->Bind(wxEVT_CLOSE_WINDOW, [&, this](wxCloseEvent &evt) {
        if (DoCloseAll() == false /* cancelled */ && evt.CanVeto())
        {
            evt.Veto();
            return ;
        }
        else
            evt.Skip();
    });

#ifdef I_DECIDE_USING_TOOLBAR // TODO decide someday

    // the toolbar part

    wxBitmap toolbar_bmp;
    toolbar_bmp.Create(16, 16);

    simulation_toolbar_ = new wxAuiToolBar(
        frm_,
        wxID_ANY,
        wxDefaultPosition,
        wxDefaultSize,
        wxAUI_TB_TEXT | wxAUI_TB_HORZ_TEXT);
    simulation_toolbar_->AddTool(
        ID_SIMULATION_START,
        "Start",
        toolbar_bmp,
        _("Starts the simulation."));

    simulation_toolbar_->AddTool(
        ID_SIMULATION_PAUSE,
        "Pause",
        toolbar_bmp,
        _("Pauses the simulation."));

    simulation_toolbar_->Bind(wxEVT_COMMAND_TOOL_CLICKED,
        [this](wxCommandEvent &) {
        DoSimulationStartStop();
    }, ID_SIMULATION_START);

    simulation_toolbar_->Bind(wxEVT_COMMAND_TOOL_CLICKED,
        [this](wxCommandEvent &) {
        DoSimulationPauseResume();
    }, ID_SIMULATION_PAUSE);

#endif

    mgr_.AddPane(notebook_, wxAuiPaneInfo()
        .Name("Documents")
        .CenterPane()
        .PaneBorder(false));

#ifdef I_DECIDE_USING_TOOLBAR

    mgr_.AddPane(simulation_toolbar_, wxAuiPaneInfo()
        .Name("Simulation Toolbar")
        .Caption(_("Simulation Toolbar"))
        .ToolbarPane()
        .Top()
        .CloseButton(false));

#endif

    return true;
}

MainFrame::Impl::Document *
MainFrame::Impl::GetDocumentFromID(long n)
{
    auto it = documents_.find(n);
    if (it == documents_.end()) return nullptr;
    return &(it->second);
}

MainFrame::Impl::Document *
MainFrame::Impl::GetDocumentFromWindow(wxWindow* win)
{
    auto it = std::find_if(
        documents_.begin(),
        documents_.end(),
        [win](const std::pair<const long, MainFrame::Impl::Document> &doc) {
            return doc.second.compoundView == win;
        }
    );
    if (it == documents_.end()) return nullptr;
    return &(it->second);
}

MainFrame::Impl::Document * MainFrame::Impl::GetActiveDocument()
{
    return GetDocumentFromID(active_document_);
}

long MainFrame::Impl::AcquireDocID() const
{
    std::size_t i;
    for (i = 0; i < documents_.size(); ++i)
    {
        auto it = documents_.find(i);
        if (it == documents_.end() /* unoccupied */)
            break;
    }
    return i;
}

void MainFrame::Impl::UpdateDocTitle(MainFrame::Impl::Document* doc)
{
    assert(doc != nullptr);
    if (doc->ID < 0) return ;
    auto index = notebook_->GetPageIndex(doc->compoundView);
    notebook_->SetPageText(index, doc->MakeTitle());
}

void MainFrame::Impl::DoCreateNew()
{
    auto n = AcquireDocID();
    Document doc;
    doc.ID = n;
    doc.compoundView = CreateNewCompoundView(doc.MakeTitle(), n);
    documents_[n] = doc;
    active_document_ = n; // as we make the active document the
    // the newly created one using CreateNewCompoundView
}

void MainFrame::Impl::DoOpen()
{
    wxFileDialog ofd{
        frm_,
        _("Open MaSoSi File"),
        default_directory_,
        _(""),
        _("MaSoSi Files (*.mss)|*.mss|All Files (*.*)|*.*"),
        wxFD_OPEN|wxFD_FILE_MUST_EXIST};
    ofd.CenterOnParent();
    if (ofd.ShowModal() == wxID_CANCEL)
        return ;
    auto file_path_str = toStdString(ofd.GetPath());
    boost::filesystem::path file_path{file_path_str};
    default_directory_ = toWxString(file_path.parent_path().string());
    DoOpen(file_path_str);
}

void MainFrame::Impl::DoOpen(const std::string& path)
{
    boost::filesystem::path file_path{path};
    MSSFile mssfile;
    try
    {
        mssfile = MSSFile::Load(path);
    }
    catch (std::exception &ex)
    {
        wxMessageBox(
            wxString::Format(
                _("Couldn't open file:\n%s"), toWxString(ex.what())),
            _("Open File Error!"),
            wxICON_ERROR | wxOK | wxCENTRE,
            frm_
        );
        return ;
    }
    auto n = AcquireDocID();
    Document doc;
    doc.name = file_path.filename().string();
    doc.path = path;
    doc.ID = n;
    doc.compoundView = CreateNewCompoundView(doc.MakeTitle(), n);
    documents_[n] = doc;
    mssfile.TransmitToSimulator(*doc.compoundView->GetSimulator());
    doc.compoundView->ReloadScripts();
    active_document_ = n;
}

bool MainFrame::Impl::DoSave()
{
    return DoSave(active_document_);
}

bool MainFrame::Impl::DoSave(long n)
{
    if (n < 0) return true;
    return DoSave(&(documents_[n]));
}

bool MainFrame::Impl::DoSave(MainFrame::Impl::Document* doc)
{
    assert(doc != nullptr);
    if (doc->compoundView->Actions()->SaveAllScripts(
        /* CompoundView::AF_DEFAULT */ CompoundView::AF_INVOKE_EVENT_HANDLERS)
        != CompoundView::AR_DONE)
        return false;
        // ^^^ cancelled| NOW, not possible. (I just do not give a shit)
    if (doc->path.empty())
    {
        // this is a new file
        wxFileDialog sfd {
            frm_,
            wxString::Format(_("Save MaSoSi File \"%s\" [id: %ld]"),
                             toWxString(doc->name), doc->ID),
            default_directory_,
            _(""),
            _("MaSoSi Files (*.mss)|*.mss|All Files (*.*)|*.*"),
            wxFD_SAVE};
        sfd.CenterOnParent();
        if (sfd.ShowModal() == wxID_CANCEL)
            return false;
        auto file_path_str = toStdString(sfd.GetPath());
        boost::filesystem::path file_path{file_path_str};
        default_directory_ = toWxString(file_path.parent_path().string());
        doc->path = file_path_str;
        doc->name = file_path.filename().string();
        UpdateDocTitle(doc);
        DoSave(doc);
    }
    else
    {
        MSSFile mssfile;
        mssfile.TransmitFromSimulator(*doc->compoundView->GetSimulator());
        try
        {
            mssfile.Save(doc->path);
        }
        catch (std::exception &ex)
        {
            wxMessageBox(
                wxString::Format(
                    _("Couldn't save file:\n%s"), toWxString(ex.what())),
                _("Save File Error!"),
                wxICON_ERROR | wxOK | wxCENTRE,
                frm_
            );
        }
        doc->isModified = false;
        UpdateDocTitle(doc);
    }
    return true;
}

void MainFrame::Impl::DoSaveAs()
{
    DoSaveAs(active_document_);
}

void MainFrame::Impl::DoSaveAs(long n)
{
    if (n < 0) return ;
    DoSaveAs(GetDocumentFromID(n));
}

void MainFrame::Impl::DoSaveAs(MainFrame::Impl::Document* doc)
{
    assert(doc != nullptr);
    if (doc->path.empty())
    {
        DoSave(doc); // this is a new file, act like Save
        return ;
    }
    Document new_doc = *doc;
    new_doc.path = "";
    new_doc.ID = -1;
        // ^^^ to prohibit the commitment for changing doc title
    new_doc.compoundView = doc->compoundView;
    DoSave(&new_doc);
}

void MainFrame::Impl::DoSaveAll()
{
    for (auto &o: documents_)
    {
        DoSave(&o.second);
    }
}

void MainFrame::Impl::DoClose()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    DoClose(doc);
}

void MainFrame::Impl::DoClose(Document *doc)
{
    assert(doc != nullptr);

    doc->compoundView->Actions()->SaveAllScripts(
        CompoundView::AF_INVOKE_EVENT_HANDLERS);
        // save the modified scripts before

    // reference to the wxAUI source code
    // void wxAuiNotebook::OnTabButton(wxAuiNotebookEvent& evt) function
    // in auibook.cpp

    wxAuiNotebookEvent e(wxEVT_AUINOTEBOOK_PAGE_CLOSE, notebook_->GetId());
    auto close_wnd = doc->compoundView;
    auto index = notebook_->GetPageIndex(close_wnd);
    e.SetSelection(index);
    e.SetOldSelection(index);
    e.SetEventObject(notebook_);
    notebook_->GetEventHandler()->ProcessEvent(e);
    if (!e.IsAllowed())
        return ;
    assert(index != wxNOT_FOUND);
    notebook_->DeletePage(index);
    wxAuiNotebookEvent e2(wxEVT_AUINOTEBOOK_PAGE_CLOSED, notebook_->GetId());
    e2.SetSelection(index);
    e2.SetEventObject(notebook_);
    notebook_->GetEventHandler()->ProcessEvent(e2);
}

bool MainFrame::Impl::DoCloseAll()
{
    // here, since the DoClose triggers the close event, which invalidates
    // the interators, I do not use DoClose for each document.
    // let's have a better approach.

    class DocumentX : public Document
    {
    public:
        bool save {true};
        // vvv i didn't like this but it works
        static DocumentX fromDocument(const Document &doc) {
            DocumentX result;
            static_cast<Document &>(result) = doc;
            return result;
        }
    };

    std::vector<DocumentX> docs;
    docs.reserve(documents_.size());

    for (auto &doc: documents_)
    {
        doc.second.compoundView->Actions()->SaveAllScripts(
            CompoundView::AF_INVOKE_EVENT_HANDLERS);    // saves the modified
                                                        // scripts
        if (doc.second.isModified) docs.push_back(
            DocumentX::fromDocument(doc.second));
    }

    if (docs.empty()) return true;

    std::sort(
        docs.begin(),
        docs.end(),
        [](const DocumentX &doc1, const DocumentX &doc2) {
            return doc1.ID < doc2.ID;
        });

    wxArrayString items;

    for (auto &doc: docs)
    {
        items.push_back(doc.MakeTitle(false /* no asterisk (*) */));
    }

    wxDialog dlg(
        frm_,
        wxID_ANY,
        _("Save Changed Simulations"),
        wxDefaultPosition, wxSize(400, 500),
        wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER);
    dlg.CenterOnParent();

    auto statTextDesc = new wxStaticText(&dlg, wxID_ANY,
        _("Check the files you want to save from the list below."));

    auto chkLstDocs = new wxCheckListBox(
        &dlg,
        wxID_ANY,
        wxDefaultPosition,
        wxDefaultSize,
        items);

    auto btnSaveSelected = new wxButton(&dlg, wxID_SAVE, _("&Save"));
    auto btnCancel = new wxButton(&dlg, wxID_CANCEL, _("&Cancel"));
    auto btnSelectAll = new wxButton(&dlg, wxID_ANY, _("Select &All"));
    auto btnSelectNone = new wxButton(&dlg, wxID_ANY, _("Select &None"));

    auto boxSzr0 = new wxBoxSizer(wxVERTICAL);
    auto boxSzr1 = new wxBoxSizer(wxHORIZONTAL);
    auto boxSzr2 = new wxBoxSizer(wxVERTICAL);

    boxSzr0->Add(statTextDesc, wxSizerFlags(0).Expand().
        Border(wxALL & ~wxBOTTOM));
    boxSzr0->Add(boxSzr1, wxSizerFlags(1).Expand().Border(0));

    boxSzr1->Add(chkLstDocs, wxSizerFlags(1).Expand().
        Border(wxLEFT|wxTOP|wxBOTTOM));
    boxSzr1->Add(boxSzr2, wxSizerFlags(0).Expand().Border(0));

    boxSzr2->Add(btnSaveSelected, wxSizerFlags(0).Expand().Border(wxALL));
    boxSzr2->Add(btnCancel, wxSizerFlags(0).Expand().
        Border(wxALL & ~wxTOP & ~wxBOTTOM));
    boxSzr2->AddStretchSpacer(1);
    boxSzr2->Add(btnSelectAll, wxSizerFlags(0).Expand().
        Border(wxALL & ~wxTOP));
    boxSzr2->Add(btnSelectNone, wxSizerFlags(0).Expand().
        Border(wxALL & ~wxTOP));
    boxSzr2->AddSpacer(50);

    dlg.SetSizer(boxSzr0);

    btnSaveSelected->Bind(wxEVT_BUTTON, [&](wxCommandEvent &) {
        for (std::size_t i = 0; i < docs.size(); ++i)
        {
            docs[i].save = chkLstDocs->IsChecked(i);
        }
        dlg.EndModal(wxID_SAVE);
    });

    btnCancel->Bind(wxEVT_BUTTON, [&](wxCommandEvent &) {
        dlg.EndModal(wxID_CANCEL);
    });

    btnSelectAll->Bind(wxEVT_BUTTON, [&](wxCommandEvent &) {
        for (std::size_t i = 0; i < docs.size(); ++i)
            chkLstDocs->Check(i, true);
    });

    btnSelectNone->Bind(wxEVT_BUTTON, [&](wxCommandEvent &) {
        for (std::size_t i = 0; i < docs.size(); ++i)
            chkLstDocs->Check(i, false);
    });

    // select all by default
    for (std::size_t i = 0; i < docs.size(); ++i)
        chkLstDocs->Check(i, true);

    auto r = dlg.ShowModal();
    if (r == wxID_SAVE)
    {
        for (auto &doc: docs)
        {
            if (doc.save) DoSave(&doc);
        }
        notebook_->DeleteAllPages();
        documents_.clear();
        return true;
    }
    else
    {
        // cancelled, do nothing!
        return false;
    }
}

void MainFrame::Impl::DoQuit()
{
    frm_->Close();
}

void MainFrame::Impl::DoClearPolygons()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    auto simulator = doc->compoundView->GetSimulator();
    auto simulation = simulator->GetSimulation();
    auto simpanel = doc->compoundView->GetSimulationPanel();
    if (simulator->IsRunning()) return;
    simulation->ClearPolygons();
    simpanel->RefreshAll();
}

void MainFrame::Impl::DoClearOutput()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    auto outpanel = doc->compoundView->GetOutputControl();
    outpanel->Clear();
}

void MainFrame::Impl::DoClear()
{
    DoClearPolygons();
    DoClearOutput();
}

void MainFrame::Impl::DoRefreshPolygons()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    auto simpanel = doc->compoundView->GetSimulationPanel();
    simpanel->RefreshPolygons();
}

void MainFrame::Impl::DoRefreshDrawing()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    auto simpanel = doc->compoundView->GetSimulationPanel();
    simpanel->Refresh();
}

void MainFrame::Impl::DoRefresh()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    auto simpanel = doc->compoundView->GetSimulationPanel();
    simpanel->RefreshAll();
}

void MainFrame::Impl::DoResetLua1()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    doc->compoundView->Actions()->ResetLua(
        Simulator::STATE_DRAWING,
        CompoundView::AF_INVOKE_EVENT_HANDLERS);
}

void MainFrame::Impl::DoResetLua2()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    doc->compoundView->Actions()->ResetLua(
        Simulator::STATE_SECOND_THREAD,
        CompoundView::AF_INVOKE_EVENT_HANDLERS);
}

void MainFrame::Impl::DoResetLua()
{
    DoResetLua1();
    DoResetLua2();
}

void MainFrame::Impl::DoSaveCurrentScript()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    doc->compoundView->Actions()->SaveCurrentScript(
        CompoundView::AF_INVOKE_EVENT_HANDLERS);
}

void MainFrame::Impl::DoRestoreCurrentScript()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    doc->compoundView->Actions()->RestoreCurrentScript(
        CompoundView::AF_INVOKE_EVENT_HANDLERS);
}

void MainFrame::Impl::DoClearCurrentScript()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    doc->compoundView->Actions()->ClearCurrentScript(
        CompoundView::AF_INVOKE_EVENT_HANDLERS);
}

void MainFrame::Impl::DoSaveCurrentScriptToFile()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    auto current = doc->compoundView->GetCurrentScript();
    std::string script = toStdString(
        doc->compoundView->GetLuaEditor(current)->GetText());
    wxFileDialog sfd {
        frm_,
        _("Save Script to File"),
        default_directory_,
        _(""),
        _("Lua Files (*.lua)|*.lua|All Files (*.*)|*.*"),
        wxFD_SAVE};
    sfd.CenterOnParent();
    if (sfd.ShowModal() == wxID_CANCEL)
        return ;
    auto file_path_str = toStdString(sfd.GetPath());
    boost::filesystem::path file_path{file_path_str};
    default_directory_ = toWxString(file_path.parent_path().string());
    boost::filesystem::ofstream ofs{
        file_path, boost::filesystem::ifstream::binary};
    if (ofs)
    {
        ofs << script;
        ofs.close();
    }
    else
    {
        wxMessageBox(
            _("Cannot save the script.\nCouldn't open stream!"),
            _("Error!"),
            wxICON_ERROR | wxOK | wxOK_DEFAULT | wxCENTRE, frm_);
    }
}

void MainFrame::Impl::DoLoadCurrentScriptFromFile()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    auto current = doc->compoundView->GetCurrentScript();
    wxFileDialog ofd{
        frm_,
        _("Open Script to File"),
        default_directory_,
        _(""),
        _("Lua Files (*.lua)|*.lua|All Files (*.*)|*.*"),
        wxFD_OPEN|wxFD_FILE_MUST_EXIST};
    ofd.CenterOnParent();
    if (ofd.ShowModal() == wxID_CANCEL)
        return ;
    auto file_path_str = toStdString(ofd.GetPath());
    boost::filesystem::path file_path{file_path_str};
    default_directory_ = toWxString(file_path.parent_path().string());
    boost::filesystem::ifstream ifs{
        file_path, boost::filesystem::ifstream::binary};
    if (ifs)
    {
        ifs.seekg(0 /* < important!, fuck implicit casts */,
                  ifs.end);
        std::size_t sz = ifs.tellg();
        ifs.seekg(0, ifs.beg);
        std::vector<char> data;
        data.resize(sz);
        ifs.read(&data[0], sz);
        ifs.close();
        auto lua_editor = doc->compoundView->GetLuaEditor(current);
        lua_editor->InsertText(
            lua_editor->GetCurrentPos(),
            wxString::FromUTF8(&data[0], sz)
        );
    }
    else
    {
        wxMessageBox(
            _("Cannot load the script.\nCouldn't open stream!"),
            _("Error!"),
            wxICON_ERROR | wxOK | wxOK_DEFAULT | wxCENTRE, frm_);
    }
}

void MainFrame::Impl::DoSaveAllScripts()
{
    auto doc = GetActiveDocument();
    if (!doc) return ;
    doc->compoundView->Actions()->SaveAllScripts(
        CompoundView::AF_INVOKE_EVENT_HANDLERS);
}

void MainFrame::Impl::DoShowTopics()
{
    // TODO Have Topics
    wxMessageBox(
        _("We haven't got any topics for now, unfortunately."),
        _("Unimplemented Feature!"),
        wxICON_WARNING | wxOK | wxOK_DEFAULT | wxCENTRE, frm_);
}

void MainFrame::Impl::DoShowAbout()
{
    // TODO Have more on About
    wxMessageBox(
        _("It's being developed by Canberk Soenmez, under GPL license.\n"),
        _("About"),
        wxICON_INFORMATION | wxOK | wxOK_DEFAULT | wxCENTRE, frm_);
}

void MainFrame::Impl::DoSimulationStartStop()
{
    auto doc = GetActiveDocument();
    if (doc)
    {
        if (doc->compoundView->GetSimulator()->IsRunning())
        {
            doc->compoundView->Actions()->Stop();
        }
        else
        {
            doc->compoundView->Actions()->Start(
                true, /* save scripts */
                CompoundView::ActionsHelper::
                AF_INVOKE_EVENT_HANDLERS // to save silently
            );
        }
    }
}

void MainFrame::Impl::DoSimulationPauseResume()
{
    auto doc = GetActiveDocument();
    if (doc && doc->compoundView->GetSimulator()->IsRunning())
    {
        if (doc->compoundView->GetSimulator()->IsPaused())
        {
            doc->compoundView->Actions()->Resume();
        }
        else
        {
            doc->compoundView->Actions()->Pause();
        }
    }
}

CompoundView::EventHandler *MainFrame::Impl::CreateEventHandler(long n)
{
    assert(n >= 0);
    return new MyEventHandler(this, n);
}

CompoundView * MainFrame::Impl::CreateNewCompoundView(
    const wxString &title,
    long n)
{
    auto compoundView = new CompoundView(frm_, wxID_ANY);
    CompoundViewPrefs prefs;
    prefs.showStartStopButton = false;
    prefs.showPauseResumeButton = false;
    prefs.showSaveButton = false;
    prefs.showRestoreButton = false;
    prefs.showStatusText = false;
    compoundView->SetPrefs(prefs);
    compoundView->AddEventHandler(CreateEventHandler(n));
    notebook_->AddPage(compoundView, title);
    notebook_->SetSelection(notebook_->GetPageIndex(compoundView));
    return compoundView;
}

void MainFrame::Impl::SetStatusText(const wxString& text)
{
    frm_->SetStatusText(text, 1);
}

MainFrame::Impl::~Impl()
{
    mgr_.UnInit();
}

//BEGIN MyEventHandler Impl

bool MainFrame::Impl::MyEventHandler::OnStopped()
{
    if (n_ == impl_->active_document_)
    {
        impl_->simulation_menu_->SetLabel(
            impl_->ID_SIMULATION_START,
            MENU_START_TEXT);
        impl_->simulation_menu_->Enable(impl_->ID_SIMULATION_PAUSE, false);
        // we handle status text stuff in the corresponding handler
    }
    return true;
}

bool MainFrame::Impl::MyEventHandler::OnStarted()
{
    if (n_ == impl_->active_document_)
    {
        impl_->simulation_menu_->SetLabel(
            impl_->ID_SIMULATION_START,
            MENU_STOP_TEXT);
        impl_->simulation_menu_->Enable(
            impl_->ID_SIMULATION_PAUSE,
            true);
        impl_->simulation_menu_->SetLabel(
            impl_->ID_SIMULATION_PAUSE,
            MENU_PAUSE_TEXT);
        // we handle status text stuff in the corresponding handler
    }
    return true;
}

bool MainFrame::Impl::MyEventHandler::OnPaused()
{
    if (n_ == impl_->active_document_)
    {
        impl_->simulation_menu_->SetLabel(
            impl_->ID_SIMULATION_PAUSE,
            MENU_RESUME_TEXT);
        // we handle status text stuff in the corresponding handler
    }
    return true;
}

bool MainFrame::Impl::MyEventHandler::OnResumed()
{
    if (n_ == impl_->active_document_)
    {
        impl_->simulation_menu_->SetLabel(
            impl_->ID_SIMULATION_PAUSE,
            MENU_PAUSE_TEXT);
        // we handle status text stuff in the corresponding handler
    }
    return true;
}

bool MainFrame::Impl::MyEventHandler::OnCurrentScriptChanged(
    Simulator::EScriptType UNUSED(old_script),
    Simulator::EScriptType UNUSED(new_script))
{
    // QUESTION should I do sth here?
    return true;
}

bool MainFrame::Impl::MyEventHandler::OnScriptModified(
    Simulator::EScriptType UNUSED(script))
{
    // QUESTION should I do sth here?
    return true;
}

bool MainFrame::Impl::MyEventHandler::OnScriptRestored(
    Simulator::EScriptType UNUSED(script))
{
    // QUESTION should I do sth here?
    return true;
}

bool MainFrame::Impl::MyEventHandler::OnScriptSaved(
    Simulator::EScriptType UNUSED(script))
{
    if (n_ == impl_->active_document_)
    {
        auto doc = impl_->GetDocumentFromID(n_);
        doc->isModified = true;
        impl_->UpdateDocTitle(doc);
    }
    return true;
}

bool MainFrame::Impl::MyEventHandler::OnStatusChanged(
    const std::string & status)
{
    auto doc = impl_->GetDocumentFromID(n_);
    doc->latestStatusText = status;
    if (n_ == impl_->active_document_)
    {
        impl_->SetStatusText(toWxString(status));
    }
    return true;
}

bool MainFrame::Impl::MyEventHandler::OnInputRequested(
    const std::string & UNUSED(ir_type),
    const std::list<std::string> & UNUSED(ir_strings))
{
    return true;
}

bool MainFrame::Impl::MyEventHandler::OnOutput(
    const std::string & output)
{
    return true;
}

//END


//BEGIN Pseudo-Implementation of MainFrame

MainFrame::MainFrame()
{
}

MainFrame::MainFrame(wxWindow* parent, wxWindowID id)
{
    MainFrame::Create(parent, id);
}

bool MainFrame::Create(wxWindow* parent, wxWindowID id)
{
    impl_ = std::make_unique<Impl>(this);
    return impl_->Create(parent, id);
}

MainFrame::~MainFrame()
{
}

//END

}
