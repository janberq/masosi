#ifndef _MSSFILE_HPP_INCLUDED
#define _MSSFILE_HPP_INCLUDED

#include <boost/filesystem/path.hpp>
#include <string>
#include <exception>

#include "Simulator.hpp"

namespace masosi {

class MSSFile
{
public:

    class Exception : public std::runtime_error
    {
    public:
        Exception(const std::string &msg)
            : std::runtime_error(msg)
        {  }
    };

    static MSSFile Load(const std::string &path);
    static MSSFile Load(const boost::filesystem::path &path);

    void Save(const std::string &path);
    void Save(const boost::filesystem::path &path);

    std::string GetScript(
        Simulator::EScriptType type) const;
    void SetScript(
        Simulator::EScriptType type,
        const std::string &script);

    void TransmitToSimulator(Simulator &simulator);
    void TransmitFromSimulator(Simulator &simulator);

private:

    std::string scripts_[Simulator::SCRIPT_COUNT];

};

}

#endif // _MSSFILE_HPP_INCLUDED
